<?php

namespace App\Actions\Fortify;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Laravel\Fortify\Contracts\UpdatesUserProfileInformation;

class UpdateUserProfileInformation implements UpdatesUserProfileInformation
{
    /**
     * Validate and update the given user's profile information.
     *
     * @param mixed $user
     * @param array $input
     * @return void
     * @throws ValidationException
     */
    public function update($user, array $input)
    {
        Validator::make($input, [
            'firstname' => ['required', 'string', 'max:255'],
            'lastname'  => ['required', 'string', 'max:255'],
            //            'photo'     => ['nullable', 'mimes:jpg,jpeg,png', 'max:1024'],
        ])->validateWithBag('userProfileInformation');

//        if (isset($input['photo'])) {
//            $user->updateProfilePhoto($input['photo']);
//        }

        if ($input['email'] !== $user->email &&
            $user instanceof MustVerifyEmail) {
            $this->updateVerifiedUser($user, $input);
        } else {
            $user->forceFill([
                'firstname' => $input['firstname'],
                'lastname'  => $input['lastname']
            ])->save();
        }
    }

    /**
     * Validate and update the given user's profile information.
     *
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     */
    public function updateEmail(Request $request)
    {
        $request->validateWithBag('userProfileInformationEmail', [
            'email' => ['required', 'email', 'max:255', Rule::unique('users')->ignore($request->user()->id)],
        ]);

        $request->user()->update(['email' => $request->get('email')]);

        return $request->wantsJson()
            ? new JsonResponse('', 200)
            : back()->with('status', 'profile-information-updated');
    }

    /**
     * Update the given verified user's profile information.
     *
     * @param mixed $user
     * @param array $input
     * @return void
     */
    protected function updateVerifiedUser($user, array $input)
    {
        $user->forceFill([
            'firstname'         => $input['firstname'],
            'lastname'          => $input['lastname'],
            'email'             => $input['email'],
            'email_verified_at' => null,
        ])->save();

        $user->sendEmailVerificationNotification();
    }

    public function delete(Request $request): RedirectResponse
    {
        $request->session()->flush();
        $user = \Auth::user();
        $user->deleteProfilePhoto();
        $user->tokens()->delete();
        $user->delete();
        return redirect()->route('home');
    }
}
