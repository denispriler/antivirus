<?php

namespace App\Providers;

use App\Services\Megaplan\MegaplanApiService;
use App\Services\PaymentService;
use App\Services\ProductKeyService;
use App\Services\UserService;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(UserService::class, function (Application $app) {
            return new UserService();
        });

        $this->app->singleton(ProductKeyService::class, function (Application $app) {
            return new ProductKeyService(app(UserService::class));
        });

        $this->app->singleton(PaymentService::class, function (Application $app) {
            return new PaymentService(app(UserService::class));
        });

        $this->app->singleton(MegaplanApiService::class, function (Application $app) {
            return new MegaplanApiService();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
