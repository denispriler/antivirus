<?php

namespace App\Services\Megaplan\Extensions;

use App\Exceptions\Megaplan\MagaplanMethodNotOverriddenException;
use App\Exceptions\Megaplan\MegaplanAuthenticateException;
use App\Exceptions\Megaplan\MegaplanRequestException;
use App\Services\Megaplan\MegaplanApiService;
use Carbon\Carbon;
use Illuminate\Http\Client\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

abstract class MegaplanExtension
{
    protected ?string $access_id;
    protected ?string $secret_key;
    private string $api_url;

    /**
     * @throws MegaplanAuthenticateException|MegaplanRequestException
     */
    public function __construct(string $access_id = null, string $secret_key = null)
    {
        if (!config('megaplan.host')) {
            throw new MegaplanAuthenticateException();
        }
        $this->api_url = "https://" . config('megaplan.host');

        if (!$access_id || !$secret_key) {
            $this->access_id = null;
            $this->secret_key = null;

            $this->authenticate();
        } else {
            $this->secret_key = $secret_key;
            $this->access_id = $access_id;
        }
    }

    protected function getKeys(): array
    {
        return ['access_id' => $this->access_id, 'secret_key' => $this->secret_key];
    }

    public function setKeys(array $keys): MegaplanExtension
    {
        $this->secret_key = $keys['secret_key'];
        $this->access_id = $keys['access_id'];
        return $this;
    }

    // <editor-fold desc="Request protocol methods.">

    /**
     * @throws MegaplanAuthenticateException
     */
    protected function authenticate()
    {
        $login = config('megaplan.auth.login');
        $password = config('megaplan.auth.password');

        if (!$login || !$password) {
            throw new MegaplanAuthenticateException();
        }

        $response = \Http::contentType('application/x-www-form-urlencoded')->asForm()->post($this->api_url . '/BumsCommonApiV01/User/authorize.api', [
            'Login'    => $login,
            'Password' => md5($password)
        ]);

        $this->access_id = $response->json('data.AccessId');
        $this->secret_key = $response->json('data.SecretKey');

        if (!$this->access_id || !$this->secret_key) {
            throw new MegaplanAuthenticateException(trans('megaplan.authenticate.token'));
        }
    }

    protected function signature(string $method, string $Date, string $Uri, array $data): string
    {
        $Method = strtoupper($method);
        $ContentType = 'application/x-www-form-urlencoded';
        $Host = config('megaplan.host');
        $ContentMD5 = '';

        $stringToSign = $Method . "\n" .
            $ContentMD5 . "\n" .
            $ContentType . "\n" .
            $Date . "\n" .
            $Host . $Uri;

        $encrypted = hash_hmac('sha1', $stringToSign, $this->secret_key, false);

        return base64_encode($encrypted);
    }

    /**
     * @throws MegaplanRequestException
     */
    protected function request(string $method, string $url, array $data = [])
    {
        $date = (new Carbon())->toRfc2822String();
        $request = \Http::withHeaders(
            $this->access_id && $this->secret_key ? [
                'X-Authorization' => "$this->access_id:" . $this->signature($method, $date, str_replace('https://', '', str_replace(config('megaplan.host'), '', $url)), $data),
                'X-Sdf-Date'      => $date
            ] : []
        )->contentType('application/x-www-form-urlencoded');

        /** @var Response $response */
        switch ($method) {
            case 'post':
                $response = $request->asForm()->post($url, $data);
                break;
            default:
                $response = $request->$method($url, $data);
                break;
        }

        // Update keys
        $this->access_id = $response->json('data.AccessId', $this->access_id);
        $this->secret_key = $response->json('data.SecretKey', $this->secret_key);
        app(MegaplanApiService::class)->updateKeys($this->getKeys());

        if (!$response->ok() || $response->json('status.code') === 'error') {
            throw new MegaplanRequestException($response->json('status.message', $response->status()));
        }

        return $response->json('data');
    }

    /**
     * @throws MegaplanRequestException
     */
    protected function post(string $uri, array $data = [])
    {
        return $this->request('post', $this->api_url . "/$uri", $data);
    }

    /**
     * @throws MegaplanRequestException
     */
    protected function get(string $uri, array $data = [])
    {
        return $this->request('get', $this->api_url . "/$uri", $data);
    }

    // </editor-fold>

    // <editor-fold desc="CRUD methods.">

    /**
     * @throws MagaplanMethodNotOverriddenException
     */
    public function CreateOrUpdate($model): array
    {
        if (!$model) {
            throw new NotFoundHttpException();
        }

        throw new MagaplanMethodNotOverriddenException(trans('megaplan.extension.override', ['method' => __METHOD__]));
    }

    /**
     * @throws MagaplanMethodNotOverriddenException
     */
    public function Delete($model): array
    {
        throw new MagaplanMethodNotOverriddenException(trans('megaplan.extension.override', ['method' => __METHOD__]));
    }

    /**
     * @throws MagaplanMethodNotOverriddenException
     */
    public function Read(string $search = null): array
    {
        throw new MagaplanMethodNotOverriddenException(trans('megaplan.extension.override', ['method' => __METHOD__]));
    }

    /**
     * @throws MagaplanMethodNotOverriddenException
     */
    public function List(): array
    {
        throw new MagaplanMethodNotOverriddenException(trans('megaplan.extension.override', ['method' => __METHOD__]));
    }

    // </editor-fold>
}
