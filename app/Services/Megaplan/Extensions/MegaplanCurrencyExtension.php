<?php

namespace App\Services\Megaplan\Extensions;

use App\Exceptions\Megaplan\MegaplanRequestException;

class MegaplanCurrencyExtension extends MegaplanExtension
{
    /**
     * @throws MegaplanRequestException
     */
    public function List(): array
    {
        return $this->get('BumsCommonApiV01/Currency/list.api')['currencies'];
    }
}
