<?php

namespace App\Services\Megaplan\Extensions;

use App\Exceptions\Megaplan\MegaplanRequestException;

class MegaplanClientTypeExtension extends MegaplanExtension
{
    /**
     * @throws MegaplanRequestException
     */
    public function List(): array
    {
        return $this->get('BumsCrmApiV01/ContractorType/list.api');
    }
}
