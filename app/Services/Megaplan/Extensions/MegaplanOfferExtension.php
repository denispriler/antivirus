<?php


namespace App\Services\Megaplan\Extensions;


use App\Exceptions\Megaplan\MegaplanRequestException;
use App\Models\Product;
use App\Services\Megaplan\MegaplanApiService;

class MegaplanOfferExtension extends MegaplanExtension
{
    /**
     * @param Product $model
     *
     * @return array
     *
     * @throws MegaplanRequestException
     */
    public function CreateOrUpdate($model): array
    {
        $megaplan = app(MegaplanApiService::class);

        /** @var array $usd */
        $usd = collect($megaplan->Currency->List())->firstWhere('Abbreviation', "USD");
        $megaplan->setKeys($megaplan->Currency->getKeys());

        /** @var array $unit */
        $unit = collect($megaplan->Unit->List())->firstWhere('Name', "шт.");
        $megaplan->setKeys($megaplan->Unit->getKeys());

        $this->setKeys($megaplan->Unit->getKeys());

        $data = [];

        if ($model->m_id) {
            $data['Id'] = $model->m_id;
        }

        $data['Model'] = [
            'Name'    => $model->name,
            'Count'   => 100,
            'Price'   => [
                'Value'    => $model->price,
                'Currency' => $usd['Id']
            ],
            'Unit'    => $unit['Id'],
            'Article' => $model->slug
        ];

        $offer = $this->post('BumsInvoiceApiV01/Offer/save.api', $data)['offer'];

        $model->update([
            'm_id' => $offer['Id']
        ]);

        return $offer;
    }
}
