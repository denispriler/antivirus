<?php


namespace App\Services\Megaplan\Extensions;


use App\Exceptions\Megaplan\MegaplanRequestException;

class MegaplanUnitExtension extends MegaplanExtension
{
    /**
     * @throws MegaplanRequestException
     */
    public function List(): array
    {
        return $this->get('BumsInvoiceApiV01/Unit/list.api')['units'];
    }
}
