<?php

namespace App\Services\Megaplan\Extensions\Deal;

use App\Exceptions\Megaplan\MegaplanRequestException;
use App\Services\Megaplan\Extensions\MegaplanExtension;

class MegaplanProgramExtension extends MegaplanExtension
{
    /**
     * @throws MegaplanRequestException
     */
    public function List(): array
    {
        return $this->get('BumsTradeApiV01/Program/list.api')['programs'];
    }
}
