<?php

namespace App\Services\Megaplan\Extensions\Deal;

use App\Exceptions\Megaplan\MegaplanRequestException;
use App\Models\ProductKey;
use App\Services\Megaplan\Extensions\MegaplanExtension;
use App\Services\Megaplan\MegaplanApiService;
use Megaplan\SimpleClient\Client;

class MegaplanDealExtension extends MegaplanExtension
{
    /**
     * @param ProductKey $model
     * @return array
     * @throws MegaplanRequestException
     */
    public function CreateOrUpdate($model): array
    {
        $megaplan = app(MegaplanApiService::class);

        /** @var array $usd */
        $usd = collect($megaplan->Currency->List())->firstWhere('Abbreviation', "USD");

        // TODO: deal program
        /** @var array $tradeProgram */
        $tradeProgram = collect($megaplan->Program->List())->firstWhere('Active', true);

        $data = [
            'ProgramId'   => $tradeProgram['Id'],
            // TODO: deal status
            'StatusId'    => collect($tradeProgram['Statuses'])->firstWhere('Name', "Закрыто")['Id'],
            'StrictLogic' => false,

            'Model' => [
                'Contractor'  => $model->user->m_id,
                'Contact'     => $model->user->m_id,
                'Description' => $model->product->name . ' product key ' . $model->key,

                'Paid' => [
                    'Value'    => $model->product->price,
                    'Currency' => $usd['Id']
                ],

//                'Cost' => [
//                    'Value'    => $model->product->price,
//                    'Currency' => $usd['Id']
//                ]
            ],

            'Positions' => [
                ['OfferId' => $model->product->m_id]
            ]
        ];

        if ($model->m_deal_id) {
            $data['Id'] = $model->m_deal_id;
        }

        $deal = $this->post('BumsTradeApiV01/Deal/save.api', $data)['deal'];
        $model->update([
            'm_deal_id' => $deal['Id']
        ]);

        return $deal;
    }
}
