<?php

namespace App\Services\Megaplan\Extensions;

use App\Exceptions\Megaplan\MegaplanRequestException;
use App\Models\User;
use App\Services\Megaplan\MegaplanApiService;

class MegaplanClientExtension extends MegaplanExtension
{
    public function __construct(string $access_id = null, string $secret_key = null)
    {
        parent::__construct($access_id, $secret_key);
    }

    /**
     * @param User|null $model
     *
     * @return array
     * @throws MegaplanRequestException
     */
    public function CreateOrUpdate($model): array
    {
        $megaplan = app(MegaplanApiService::class);
        $types = $megaplan->ClientType->list();

        $client = [
            'Model' => [
                'Type'       => collect($types)->where('MasterType', 'client')->first()['Id'],
                'FirstName'  => $model->name,
                'LastName'   => $model->lastname,
                'MiddleName' => '',
                'Email'      => $model->email,
                'Phones'     => [$model->phone],
                'TypePerson' => 'human',
            ]
        ];

        if ($model->m_id) {
            $client['Id'] = $model->m_id;
        }

        $response = $this->post('BumsCrmApiV01/Contractor/save.api', $client);

        $model->update([
            'm_id' => $response['contractor']['Id']
        ]);

        return $response;
    }
}
