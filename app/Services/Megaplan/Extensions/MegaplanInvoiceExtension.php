<?php

namespace App\Services\Megaplan\Extensions;

use App\Exceptions\Megaplan\MegaplanRequestException;
use App\Models\ProductKey;

class MegaplanInvoiceExtension extends MegaplanExtension
{
    /*
     * Статус счета (1 - Черновик, 2 - Выставлен, 3 - Оплачен, 4 - Отказ)
     */
    public static int $STATUS_TEMPLATE = 1;
    public static int $STATUS_CREATED = 2;
    public static int $STATUS_PAYED = 3;
    public static int $STATUS_DECLINED = 4;

    /**
     * @param ProductKey $model
     * @param int $status
     * @param int|null $dealId
     * @return array
     * @throws MegaplanRequestException
     *
     */
    public function CreateOrUpdateWithStatus(ProductKey $model, int $status = 2, int $dealId = null): array
    {
        $data = [];

        $data['Model'] = [
            'Recipient' => $model->user->m_id,
            'Payer'     => $model->user->m_id,
            'Consignee' => $model->user->m_id,
            'Comment'   => $model->product->name . " invoice",
            'Status'    => $status
        ];

        if ($model->m_invoice_id) {
            $data['Id'] = $model->m_invoice_id;
        }

        if ($dealId) {
            $data['Deal'] = $dealId;
        }

        $invoice = $this->post('BumsInvoiceApiV01/Invoice/save.api', $data)['Invoice'];

        $model->update([
            'm_invoice_id' => $invoice['Id']
        ]);

        return $invoice;
    }

    /**
     * @throws MegaplanRequestException
     */
    public function CreateOrUpdate($model): array
    {
        return $this->CreateOrUpdateWithStatus($model);
    }
}
