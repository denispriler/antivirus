<?php


namespace App\Services\Megaplan\Extensions;


use App\Exceptions\Megaplan\MegaplanRequestException;
use App\Models\ProductKey;

class MegaplanInvoiceRowExtension extends MegaplanExtension
{
    /**
     * @param ProductKey $model
     * @return array
     * @throws MegaplanRequestException
     */
    public function CreateOrUpdate($model): array
    {
        return $this->post('BumsInvoiceApiV01/InvoiceRow/save.api', [
            'Model' => [
                'Invoice'          => $model->m_invoice_id,
                'Name'        => $model->product->name,
                'Offer'       => $model->product->m_id,
                'Description' => $model->product->name . ' product key ' . $model->key
            ]
        ]);
    }
}
