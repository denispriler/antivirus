<?php


namespace App\Services\Megaplan;

use App\Exceptions\Megaplan\MegaplanAuthenticateException;
use App\Exceptions\Megaplan\MegaplanRequestException;
use App\Services\Megaplan\Extensions\Deal\MegaplanProgramExtension;
use App\Services\Megaplan\Extensions\MegaplanClientExtension;
use App\Services\Megaplan\Extensions\MegaplanClientTypeExtension;
use App\Services\Megaplan\Extensions\MegaplanCurrencyExtension;
use App\Services\Megaplan\Extensions\MegaplanExtension;
use App\Services\Megaplan\Extensions\MegaplanInvoiceExtension;
use App\Services\Megaplan\Extensions\MegaplanInvoiceRowExtension;
use App\Services\Megaplan\Extensions\MegaplanOfferExtension;
use App\Services\Megaplan\Extensions\MegaplanUnitExtension;
use App\Services\Megaplan\Extensions\Deal\MegaplanDealExtension;

class MegaplanApiService extends MegaplanExtension
{
    public array $services = [];

    public function __get($name)
    {
        if (!array_key_exists($name, $this->services)) {
            $this->services[$name] = $this->$name();
        }

        return $this->services[$name];
    }

    public function updateKeys(array $keys)
    {
        $this->setKeys($keys);
        foreach ($this->services as $service) {
            $service->setKeys($keys);
        }
    }

    /**
     * @throws MegaplanRequestException
     * @throws MegaplanAuthenticateException
     */
    private function ClientType(): MegaplanClientTypeExtension
    {
        return new MegaplanClientTypeExtension($this->access_id, $this->secret_key);
    }

    /**
     * @throws MegaplanRequestException
     * @throws MegaplanAuthenticateException
     */
    private function Client(): MegaplanClientExtension
    {
        return new MegaplanClientExtension($this->access_id, $this->secret_key);
    }

    /**
     * @throws MegaplanRequestException
     * @throws MegaplanAuthenticateException
     */
    private function Offer(): MegaplanOfferExtension
    {
        return new MegaplanOfferExtension($this->access_id, $this->secret_key);
    }

    /**
     * @throws MegaplanRequestException
     * @throws MegaplanAuthenticateException
     */
    private function Currency(): MegaplanCurrencyExtension
    {
        return new MegaplanCurrencyExtension($this->access_id, $this->secret_key);
    }

    /**
     * @throws MegaplanRequestException
     * @throws MegaplanAuthenticateException
     */
    private function Unit(): MegaplanUnitExtension
    {
        return new MegaplanUnitExtension($this->access_id, $this->secret_key);
    }

    /**
     * @throws MegaplanRequestException
     * @throws MegaplanAuthenticateException
     */
    private function Program(): MegaplanProgramExtension
    {
        return new MegaplanProgramExtension($this->access_id, $this->secret_key);
    }

    /**
     * @throws MegaplanRequestException
     * @throws MegaplanAuthenticateException
     */
    private function Deal(): MegaplanDealExtension
    {
        return new MegaplanDealExtension($this->access_id, $this->secret_key);
    }

    /**
     * @throws MegaplanRequestException
     * @throws MegaplanAuthenticateException
     */
    private function Invoice(): MegaplanInvoiceExtension
    {
        return new MegaplanInvoiceExtension($this->access_id, $this->secret_key);
    }

    /**
     * @throws MegaplanRequestException
     * @throws MegaplanAuthenticateException
     */
    private function InvoiceRow(): MegaplanInvoiceRowExtension
    {
        return new MegaplanInvoiceRowExtension($this->access_id, $this->secret_key);
    }
}
