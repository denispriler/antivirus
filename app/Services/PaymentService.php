<?php


namespace App\Services;


class PaymentService
{
    private UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * User's payment card verification.
     *
     * @param string $number
     * @param string $date
     * @param string $cvc
     * @return bool
     */
    public function authenticate(string $number, string $date, string $cvc): bool
    {
        return true;
    }

    public function invoice(string $number, string $date, string $cvc, float $amount): bool
    {
        return true;
    }
}
