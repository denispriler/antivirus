<?php


namespace App\Services;


use App\Models\Product;
use App\Models\ProductKey;
use App\Models\User;
use Carbon\Carbon;

class ProductKeyService
{
    private UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function createProductKey(Product $product, User $user = null): ProductKey
    {
        if ($user) {
            $this->userService->setUser($user);
        }
        $datetime = now();
        return ProductKey::create([
            'user_id'    => $this->userService->getUser()->id,
            'product_id' => $product->id,
            'key'        => $this->generateProductKeyString($this->userService->getUser(), $product, $datetime),
            'created_at' => $datetime,
            'updated_at' => $datetime
        ]);
    }

    private function generateProductKeyString(User $user, Product $product, Carbon $date): string
    {
        $dateSeries = substr(preg_replace('/[^A-Za-z0-9.!?[:space:]]/', '', \Hash::make($date->timestamp)), 0, 4);
        $userSeries = substr(preg_replace('/[^A-Za-z0-9.!?[:space:]]/', '', \Hash::make($user->uuid)), 4, 4);
        $productSeries = substr(preg_replace('/[^A-Za-z0-9.!?[:space:]]/', '', \Hash::make($product->slug)), 8, 4);
        $hash = substr(preg_replace('/[^A-Za-z0-9.!?[:space:]]/', '', \Hash::make(config('product.salt'))), -4);
        return strtoupper(implode("-", [$dateSeries, $userSeries, $productSeries, $hash]));
    }
}
