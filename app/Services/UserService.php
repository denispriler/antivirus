<?php

namespace App\Services;

use App\Models\User;

class UserService
{
    private ?User $user;
    private ?string $ip;

    public function __construct()
    {
        $this->user = request()->user() instanceof User ? request()->user() : null;
        $this->ip = request()->getClientIp();
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getClientIp(): ?string
    {
        return $this->ip;
    }
}
