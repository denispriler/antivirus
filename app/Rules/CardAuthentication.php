<?php

namespace App\Rules;

use App\Services\PaymentService;
use Illuminate\Contracts\Validation\Rule;

class CardAuthentication implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        $card = explode(",", $value);
        return app(PaymentService::class)->authenticate($card[0] ?? '', $card[1] ?? '', $card[2] ?? '');
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return trans('validation.payment.card.authenticate');
    }
}
