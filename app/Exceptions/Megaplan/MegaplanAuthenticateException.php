<?php


namespace App\Exceptions\Megaplan;


use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Throwable;

class MegaplanAuthenticateException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct('megaplan.authenticate.credentials', $code, $previous);
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function render(Request $request)
    {
        if ($request->expectsJson()) {
            return response()->json([
                'message' => trans('megaplan.authenticate.credentials')
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        } else {
            return response()->view('errors.500', ['exception' => $this], 500);
        }
    }
}
