<?php


namespace App\Exceptions\Megaplan;


use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Throwable;

class MagaplanMethodNotOverriddenException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct("Megaplan: " . $message, $code, $previous);
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function render(Request $request)
    {
        if ($request->expectsJson()) {
            return response()->json([
                'message' => $this->message
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        } else {
            return response()->view('errors.500', ['exception' => $this], 500);
        }
    }
}
