<?php

namespace App\Http\Resources\Product;

use App\Models\ProductKey;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductKeyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var ProductKey|JsonResource $this */
        return [
            'key' => $this->key
        ];
    }
}
