<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\User;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Laravel\Fortify\Contracts\TwoFactorLoginResponse;
use Laravel\Socialite\Facades\Socialite;

class HomeController extends Controller
{
    private StatefulGuard $guard;

    /**
     * Create a new controller instance.
     *
     * @param StatefulGuard $guard
     * @return void
     */
    public function __construct(StatefulGuard $guard)
    {
        $this->guard = $guard;
    }

    public function index()
    {
        return view('home', [
            'free_antivirus'    => Product::getFreeProduct()->slug,
            'premium_antivirus' => Product::getPremiumProduct()->slug,
        ]);
    }

    public function oauth(Request $request)
    {
        $serviceUser = Socialite::driver($request->route('provider'))->user();
        $dbUser = User::whereEmail($serviceUser->getEmail())->firstOr(function () use ($serviceUser) {
            $user = User::create([
                'email'       => $serviceUser->getEmail(),
                'first_name'  => $serviceUser->getName(),
                'second_name' => '',
                'password'    => bcrypt(Str::random())
            ]);
            $user->markEmailAsVerified();
            return $user;
        });

        $this->guard->login($dbUser, true);

        $request->session()->regenerate();

        return app(TwoFactorLoginResponse::class);
    }
}
