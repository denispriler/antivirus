<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Product\ProductInvoiceRequest;
use App\Http\Resources\Product\ProductKeyResource;
use App\Models\Product;
use App\Models\User;
use App\Services\Megaplan\Extensions\MegaplanInvoiceExtension;
use App\Services\Megaplan\MegaplanApiService;
use App\Services\PaymentService;
use App\Services\ProductKeyService;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class ProductController extends Controller
{
    /**
     * Product's invoice request.
     *
     * @param ProductInvoiceRequest $request
     * @param ProductKeyService $productKeyService
     * @param PaymentService $paymentService
     * @param MegaplanApiService $megaplanApiService
     *
     * @return ProductKeyResource
     */
    public function store(ProductInvoiceRequest $request,
                          ProductKeyService $productKeyService,
                          PaymentService $paymentService,
                          MegaplanApiService $megaplanApiService): ProductKeyResource
    {
        /** @var User $user */
        $user = User::firstOrCreate([
            'email' => $request->client()['email']
        ], array_merge($request->client(), ['password' => \Str::random(), 'uuid' => Uuid::uuid4()]));

        $megaplanApiService->Client->CreateOrUpdate($user);

        /** @var Product $product */
        $product = Product::whereSlug($request->product())->firstOrFail();

        /* Trying to invoice product amount from user's card. Card authorization made with request's rule. */
        if (!$paymentService->invoice($request->card()['number'], $request->card()['date'], $request->card()['cvc'], $product->price)) {
            throw new UnprocessableEntityHttpException(trans('validation.payment.card.invoice'));
        }

        // Create product key
        $productKey = $productKeyService->createProductKey($product, $user);
        // Create invoice
//        $megaplanApiService->Invoice->CreateOrUpdateWithStatus($productKey, MegaplanInvoiceExtension::$STATUS_TEMPLATE);
//        // Add product to invoice
//        $megaplanApiService->InvoiceRow->CreateOrUpdate($productKey);
//
//        // Create deal
//        $deal = $megaplanApiService->Deal->CreateOrUpdate($productKey);
//        // Update invoice with status and deal
//        $megaplanApiService->Invoice->CreateOrUpdateWithStatus($productKey, MegaplanInvoiceExtension::$STATUS_CREATED, $deal['Id']);
//        $megaplanApiService->Invoice->CreateOrUpdateWithStatus($productKey, MegaplanInvoiceExtension::$STATUS_PAYED, $deal['Id']);

        return ProductKeyResource::make($productKey);
    }
}
