<?php

namespace App\Http\Requests\Product;

use App\Models\Product;
use App\Rules\CardAuthentication;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;

class ProductInvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'card' => array_merge($this->get('card'), [
                'date'   => Carbon::createFromFormat("m/y", $this->card()['date'] ?? "12/12")->toDateString(),
                'number' => str_replace(" ", "", optional($this->get('card'))['number'])
            ])
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'product.slug' => 'required|string|in:' . implode(',', [Product::$FREE_SLUG, Product::$ULTIMATE_SLUG, Product::$PREMIUM_SLUG]),

            'user.name'     => 'required|string',
            'user.lastname' => 'required|string',
            'user.email'    => 'required|email',
            'user.phone'    => 'required|string',

            'card.number' => ['required', 'numeric', new CardAuthentication],
            'card.date'   => 'required|date|after:now',
            'card.cvc'    => 'required|integer|digits:3',
        ];
    }

    public function client(): array
    {
        return $this->get('user');
    }

    public function card(): array
    {
        return $this->get('card');
    }

    public function product(): string
    {
        return $this->get('product')['slug'];
    }
}
