<?php

namespace App\Http\View\Composers;

use App\Models\Category;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\View;

class CategoryComposer
{
    /**
     * @var Category[]|Collection
     */
    private $categories;

    /**
     * Create a new category composer.
     */
    public function __construct()
    {
        $this->categories = Category::with('products.platforms')->get();
    }

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('categories', $this->categories);
    }
}
