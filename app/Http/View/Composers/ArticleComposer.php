<?php

namespace App\Http\View\Composers;

use App\Models\Article;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\View;

class ArticleComposer
{
    /**
     * @var Article[]|Collection
     */
    private $articles;

    /**
     * Create a new category composer.
     */
    public function __construct()
    {
        $this->articles = Article::all();
    }

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('articles', $this->articles);
    }
}
