<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'm_id',
        'category_id',
        'name',
        'slug',
        'details',
        'logo',
        'price',
        'is_best',
        'tpl'
    ];

    static public $FREE_SLUG = 'free-antivirus';
    static public $PREMIUM_SLUG = 'premium-security';
    static public $ULTIMATE_SLUG = 'ultimate';

    public function platforms(): BelongsToMany
    {
        return $this->belongsToMany(
            Platform::class,
            'product_has_platform',
            'product_id',
            'platform_id'
        );
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public static function getFreeProduct()
    {
        return self::query()->where('slug', self::$FREE_SLUG)->first();
    }

    public static function getPremiumProduct()
    {
        return self::query()->where('slug', self::$PREMIUM_SLUG)->first();
    }

    public static function getUltimateProduct()
    {
        return self::query()->where('slug', self::$ULTIMATE_SLUG)->first();
    }

    public function productKeys(): HasMany
    {
        return $this->hasMany(ProductKey::class);
    }
}
