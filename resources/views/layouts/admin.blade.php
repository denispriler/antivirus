<!DOCTYPE html>
<html lang="en">

<head>

    <!-- <script src="./scripts/OtAutoBlock.js"></script> -->
    <script src="{{asset('js/static/10001792/web/j/vendor/one-trust.js')}}"></script>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="icon" type="image/png" href="{{asset('favicon.ico')}}" sizes="32x32" />
    <link rel="icon" type="image/png" href="{{asset('favicon.ico')}}" sizes="16x16" />

    <meta name="application-name" content="VimGuard Antivirus" />
    <meta name="msapplication-TileColor" content="#2d364c" />
    <meta name="msapplication-square150x150logo" content="{{asset('images/big-logo.png')}}">
    <meta name="msapplication-square310x310logo" content="{{asset('images/big-logo.png')}}" />

    <meta content="{{asset('images/big-logo.png')}}" property="og:image" />

    <link href="{{asset('styles/styles.css')}}" rel="stylesheet" media="all">

    <title>VimGuard | Download Free Antivirus</title>
    <meta
        content="Join 180 million others and get award-winning free antivirus for PC, Mac & Android. Surf safely & privately with our VPN. Download VimGuard today!"
        name="description">
    <meta property="og:title" content="VimGuard | Download Free Antivirus &amp; VPN | 100% Free &amp; Easy">
    <meta property="og:description"
          content="Join 180 million others and get award-winning free antivirus for PC, Mac & Android. Surf safely & privately with our VPN. Download VimGuard today!">

    <script src="{{asset('js/polyfill.min.js')}}"></script>
    <script>
        document.documentElement.className = document.documentElement.className.replace("no-js", "js");
    </script>
</head>
<!-- Использовать тег "logged", когда пользователь успешно вошел в свой кабинет -->

<body class="tmpl-www mod-en-us logged">

@include('layouts.admin-header')
<main class="main_admin" id="main-content">
    @yield('content')
</main>
@include('footer')
</body>

</html>
