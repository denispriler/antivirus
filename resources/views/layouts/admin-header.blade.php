<header class="header-admin">
    <div class="header-line">
        <a href="{{route('home')}}"><img src="{{asset('images/logo.png')}}" alt="VimGuard Logo"></a>
        <div class="header-line_user">
            <img src="{{Auth::user()->profile_photo_url}}" alt
                 class="header-line_user_img">
            <div class="dropdown">
                <button id="admin-header-dropdown" type="button" data-toggle="dropdown">
                    <span class="header-line_user_login">{{Auth::user()->email}}</span>
                </button>
                <div class="dropdown-menu" aria-labelledby="admin-header-dropdown">
                    <a href="{{route('dashboard')}}" class="dropdown-item">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-command">
                            <path
                                d="M18 3a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3 3 3 0 0 0 3-3 3 3 0 0 0-3-3H6a3 3 0 0 0-3 3 3 3 0 0 0 3 3 3 3 0 0 0 3-3V6a3 3 0 0 0-3-3 3 3 0 0 0-3 3 3 3 0 0 0 3 3h12a3 3 0 0 0 3-3 3 3 0 0 0-3-3z"></path>
                        </svg>
                        <span class="ml-2">Dashboard</span>
                    </a>
                    <form action="{{route('logout')}}" method="POST">
                        @csrf
                        <button type="submit"
                                class="dropdown-item d-flex justify-content-start align-items-center flex-row">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none"
                                 stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                 class="feather feather-log-in">
                                <path d="M15 3h4a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2h-4"></path>
                                <polyline points="10 17 15 12 10 7"></polyline>
                                <line x1="15" y1="12" x2="3" y2="12"></line>
                            </svg>
                            <span class="ml-2">Logout</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</header>
