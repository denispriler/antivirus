<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    <script src="{{asset('js/static/10001792/web/j/vendor/one-trust.js')}}"></script>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="icon" type="image/png" href="{{asset('favicon.ico')}}" sizes="32x32"/>
    <link rel="icon" type="image/png" href="{{asset('favicon.ico')}}" sizes="16x16"/>

    <meta name="application-name" content="VimGuard Antivirus"/>
    <meta name="msapplication-TileColor" content="#2d364c"/>
    <meta name="msapplication-square150x150logo" content="/images/big-logo.png">
    <meta name="msapplication-square310x310logo" content="/images/big-logo.png"/>

    <meta content="{{asset('images/big-logo.png')}}" property="og:image"/>

    <title>VimGuard | Download Free Antivirus</title>
    <meta
        content="Join 180 million others and get award-winning free antivirus for PC, Mac & Android. Surf safely & privately with our VPN. Download VimGuard today!"
        name="description">
    <meta property="og:title" content="VimGuard | Download Free Antivirus &amp; VPN | 100% Free &amp; Easy">
    <meta property="og:description"
          content="Join 180 million others and get award-winning free antivirus for PC, Mac & Android. Surf safely & privately with our VPN. Download VimGuard today!">

    <link rel="stylesheet" href="{{asset('styles/styles.css')}}">

    <script src="{{asset('js/polyfill.min.js')}}"></script>
    <script>
        document.documentElement.className = document.documentElement.className.replace("no-js", "js");
    </script>
    <script>!function (a) {
            var e = "https://s.go-mpulse.net/boomerang/", t = "addEventListener";
            if ("False" == "True") a.BOOMR_config = a.BOOMR_config || {}, a.BOOMR_config.PageParams = a.BOOMR_config.PageParams || {}, a.BOOMR_config.PageParams.pci = !0, e = "https://s2.go-mpulse.net/boomerang/";
            if (window.BOOMR_API_key = "9K3EU-JY7U6-TD3RC-2KCEC-AH4A8", function () {
                function n(e) {
                    a.BOOMR_onload = e && e.timeStamp || (new Date).getTime()
                }

                if (!a.BOOMR || !a.BOOMR.version && !a.BOOMR.snippetExecuted) {
                    a.BOOMR = a.BOOMR || {}, a.BOOMR.snippetExecuted = !0;
                    var i, _, o, r = document.createElement("iframe");
                    if (a[t]) a[t]("load", n, !1); else if (a.attachEvent) a.attachEvent("onload", n);
                    r.src = "javascript:void(0)", r.title = "", r.role = "presentation", (r.frameElement || r).style.cssText = "width:0;height:0;border:0;display:none;", o = document.getElementsByTagName("script")[0], o.parentNode.insertBefore(r, o);
                    try {
                        _ = r.contentWindow.document
                    } catch (O) {
                        i = document.domain, r.src = "javascript:var d=document.open();d.domain='" + i + "';void(0);", _ = r.contentWindow.document
                    }
                    _.open()._l = function () {
                        var a = this.createElement("script");
                        if (i) this.domain = i;
                        a.id = "boomr-if-as", a.src = e + "9K3EU-JY7U6-TD3RC-2KCEC-AH4A8", BOOMR_lstart = (new Date).getTime(), this.body.appendChild(a)
                    }, _.write("<bo" + 'dy onload="document._l();">'), _.close()
                }
            }(), "".length > 0) if (a && "performance" in a && a.performance && "function" == typeof a.performance.setResourceTimingBufferSize) a.performance.setResourceTimingBufferSize();
            !function () {
                if (BOOMR = a.BOOMR || {}, BOOMR.plugins = BOOMR.plugins || {}, !BOOMR.plugins.AK) {
                    var e = "" == "true" ? 1 : 0, t = "",
                        n = "avurmb3imccf6yfz5uha-f-a483a0b26-clientnsv4-s.akamaihd.net", i = {
                            "ak.v": "31",
                            "ak.cp": "175726",
                            "ak.ai": parseInt("208113", 10),
                            "ak.ol": "0",
                            "ak.cr": 62,
                            "ak.ipv": 4,
                            "ak.proto": "http/1.1",
                            "ak.rid": "858bab5f",
                            "ak.r": 25187,
                            "ak.a2": e,
                            "ak.m": "dsca",
                            "ak.n": "essl",
                            "ak.bpcip": "5.105.22.0",
                            "ak.cport": 54099,
                            "ak.gh": "104.96.91.101",
                            "ak.quicv": "",
                            "ak.tlsv": "tls1.3",
                            "ak.0rtt": "",
                            "ak.csrc": "-",
                            "ak.acc": "reno",
                            "ak.t": "1622797582",
                            "ak.ak": "hOBiQwZUYzCg5VSAfCLimQ==AaapL55wWE3YEyhIp6mVlWibP5rX/zq3vM30ZcrWPBnTcsnB3d7A/yqloTTWHY82F7Go9xjU6XrhjlB7/GaDEqt7IHcRhez9RsV5eresoXJKTlRwe5rJKO/xsshqugZlkqXuyYeruUhZH1LNN8MOvIW5MhQoiVzENu6Y14sboe9B69JhR0PPBtySEc4NUfcEuhn/8vWP3TRPnQJlNmsMkAoc4jci05kI/E+q0zUdI0lqhc4Z7WG2T4vXnrd8JhpSmK0hIuRIrRzATc72yYIey68bT/Tmy0s7qX5qorTgs3TmqItNWJpxtMu2yPtcT5FBDrMk9aXE26Lg2mFLxVpHDAOOWSyOyT8/e1q9NPdW1U/TBACQE/Rr8E9gCErJfHOpJiAMmlEP1R/4SWI9mCAlV5KGHzWi3FHuiQeSbmW9TLg=",
                            "ak.pv": "449",
                            "ak.dpoabenc": ""
                        };
                    if ("" !== t) i["ak.ruds"] = t;
                    var _ = {
                        i: !1, av: function (e) {
                            var t = "http.initiator";
                            if (e && (!e[t] || "spa_hard" === e[t])) i["ak.feo"] = void 0 !== a.aFeoApplied ? 1 : 0, BOOMR.addVar(i)
                        }, rv: function () {
                            var a = ["ak.bpcip", "ak.cport", "ak.cr", "ak.csrc", "ak.gh", "ak.ipv", "ak.m", "ak.n", "ak.ol", "ak.proto", "ak.quicv", "ak.tlsv", "ak.0rtt", "ak.r", "ak.acc", "ak.t"];
                            BOOMR.removeVar(a)
                        }
                    };
                    BOOMR.plugins.AK = {
                        akVars: i, akDNSPreFetchDomain: n, init: function () {
                            if (!_.i) {
                                var a = BOOMR.subscribe;
                                a("before_beacon", _.av, null, null), a("onbeacon", _.rv, null, null), _.i = !0
                            }
                            return this
                        }, is_complete: function () {
                            return !0
                        }
                    }
                }
            }()
        }(window);</script>
</head>
<body class="tmpl-www mod-en-us">
<div class="navigation-mobile-overlay"></div>
@include('header')
<div class="content-holder" id="main-content" tabindex="-1">
    @yield('content')
</div>
@include('footer')

</body>
</html>
