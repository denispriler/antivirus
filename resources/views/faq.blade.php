@extends('layouts.admin')
@section('content')
    <div class="main_admin_questions_container">
        <div class="main_admin_questions_chapter">
            <div class="main_admin_questions_chapter_header">
                <p class="main_admin_questions_chapter_header_text">
                    Installation and Activation
                </p>
            </div>
            <div class="main_admin_questions_items">
                <div class="main_admin_questions_item">
                    <h5 class="main_admin_questions_question">
                        <span>Download and install you Vim product</span>
                    </h5>
                    <p class="main_admin_questions_answer">
                        <span>To download and install your Avast product:</span>
                        <span>1. Click the button below to open our download and installation help page:</span>
                        <button type="button" class="main_admin_questions_answer_btn">GET Vimguard</button>
                        <span>2. Use the Choose your product drop-down menu to select your purchased product.</span>
                        <span>3. Use the Choose your platform drop-down menu to select your platform.</span>
                        <span>4. Click See Installation Instructions, then follow the on-screen steps.</span>
                        <span>Your Vim product is now installed on your device. If you have not yet activated your subscription, refer the the next sections in this article for information about locating your activation code and activating your Vim product.</span>
                    </p>
                </div>
                <div class="main_admin_questions_item">
                    <h5 class="main_admin_questions_question">
                        <span>Locate your activation code</span>
                    </h5>
                    <p class="main_admin_questions_answer">
                        <span>You can find your activation code in one of the following locations:</span>
                        <span class="marker">The <a href="#">Vim Account</a> that is linked to the email address you provided during the subscription purchase.</span>
                        <span class="marker">The <b>order confirmation email&nbsp;</b> that you received after purchase (usually from refund@vimguard.com).</span>
                        <span class="marker">The <b>activation card&nbsp;</b> that was included with your purchase in an Vim box or plastic case.</span>
                        <span class="marker"><b>Another Vim product&nbsp;</b> that you have already activated using the same activation code.</span>
                        <span>For detailed instructions on locating your activation code, refer to the following article:</span>
                        <span class="marker"><a href="#">Locating your Vim activation code</a></span>
                        <span>For detailed instructions on retrieving an activation code from your Vim Account, refer to the following article:</span>
                        <span class="marker">Retrieving an activation code from your Vim Account</span>
                    </p>
                </div>
                <div class="main_admin_questions_item">
                    <h5 class="main_admin_questions_question">
                        <span>Activate your Vim product</span>
                    </h5>
                    <p class="main_admin_questions_answer">
                        <span>You can find your activation code in one of the following locations:</span>
                        <span>1. Locate your activation code. This is included in your order confirmation email. For detailed instructions, refer to the following article:</span>
                        <span class="marker"><a href="#">Locating your Vim activation code</a></span>
                        <span>2. Double-click the Vim Internet Security icon on your Windows desktop to open the application.</span>
                        <span>For alternative options to open Vim Internet Security, refer to the following article:</span>
                        <span class="marker"><a href="#">Opening Vim applications</a></span>
                        <span>3. Select <b>☰ Menu ▸ Enter activation code.</b></span>
                        <span>4. Type or paste your <b>activation code</b> (including hyphens) into the text box, then click <b>Enter</b>.</span>
                        <span>5. If your activation code is valid for <b>multiple products</b>, untick the box next to any products that you <b>do not</b> want to install and activate.</span>
                        <span>Click <b>Activate & install</b> to confirm activation and installation of all selected products.</span>
                        <span>Your Vim Internet Security subscription is now active. If activation is unsuccessful, refer to the Still need help? section in this article.</span>
                    </p>
                </div>
                <div class="main_admin_questions_item">
                    <h5 class="main_admin_questions_question">
                        <span>Transfer your subscription to another device</span>
                    </h5>
                    <p class="main_admin_questions_answer">
                        <span>To learn how to transfer an Vim subscription from one device to another, refer to the following article:</span>
                        <span class="marker"><a href="#">Transferring an Vim subscription to another device</a></span>
                    </p>
                </div>
            </div>
        </div>
        <div class="main_admin_questions_chapter">
            <div class="main_admin_questions_chapter_header">
                <p class="main_admin_questions_chapter_header_text">
                    Troubleshooting
                </p>
            </div>
            <div class="main_admin_questions_items">
                <div class="main_admin_questions_item">
                    <h5 class="main_admin_questions_question">
                        <span>Which Vim product(s) did I purchase?</span>
                    </h5>
                    <p class="main_admin_questions_answer">
                        <span>Some Avast subscriptions are valid for multiple products, and some are valid for just one product. You can verify what is included in your Avast subscription using one of the methods below:</span>
                        <span class="marker"><b>Vim Account</b>: Sign in to the Vim Account that is linked to the email address you provided at checkout, then click the Subscriptions tile. The valid platforms and products for each subscription are listed under Install on your devices.</span>
                        <span class="marker"><b>Order confirmation email</b>: Locate the order confirmation email that you received after purchase (usually from refund@vimguard.com). The valid platforms and products are listed under <b>Your products</b>.
                            </span>
                    </p>
                </div>
                <div class="main_admin_questions_item">
                    <h5 class="main_admin_questions_question">
                        <span>What if installation fails?</span>
                    </h5>
                    <p class="main_admin_questions_answer">
                        <span>Before following the installation steps in this article, ensure that your PC is properly prepared for installation. For instructions, refer to the following article:</span>
                        <span class="marker"><a href="#">Preparing your PC for installation of Vim software</a></span>
                        <span>If this does not resolve the issue, try installing your Vim product using one of our detailed installation articles.</span>
                        <span>If installation continues to fail, you can click the link below to open the Vim <b>Technical Support</b> form. After you submit the request, an Vim Support agent will contact you to help resolve the issue.</span>
                        <span class="marker"><a href="#">Contact Vim Support</a></span>
                    </p>
                </div>
                <div class="main_admin_questions_item">
                    <h5 class="main_admin_questions_question">
                        <span>What if activation is unsuccessful?</span>
                    </h5>
                    <p class="main_admin_questions_answer">
                        <span>Review the information below, which explains how to resolve some of the most common activation issues.</span>
                        <span>Your activation method:</span>
                        <span class="method"><a href="#">VIM ACCOUNT</a><a href="#">ACTIVATION CODE</a></span>
                        <span>If your Vim product displays an <b>expiration message</b>, refer to the following section of this article to ensure your product is properly activated:</span>
                        <span class="marker">Activate your Vim product</span>
                        <span>Ensure you are correctly entering your activation code (including hyphens). Refer to the following section of this article for more information about locating your activation code:</span>
                        <span class="marker"><a href="#">Locate your activation code</a></span>
                        <span>If you see an error message during activation, refer to the following article for advice:</span>
                        <span class="marker"><a href="#">Troubleshooting common activation error messages</a></span>
                        <span>If you are still unable to activate your subscription, you can click the link below to open the Vim Technical Support form. After you submit the request, an Vim Support agent will contact you to help resolve the issue.</span>
                        <span class="marker"><a href="#">Contact Vim Support</a></span>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection
