@extends('layouts.admin')
@section('content')
    <div class="main_page_first_line">
        <h2 class="main_page_first_line_title">
            Account settings
        </h2>
        <p class="main_page_first_line_text">
            Adjust your account details, passwords, email, and more.
        </p>
    </div>
    <div class="main_admin_settings_container">
        @if (Laravel\Fortify\Features::canUpdateProfileInformation())
            <div class="main_admin_settings_item">
                <img src="{{asset('images/adminpanel/ac3.jpg')}}" alt="Personal information">
                <div class="main_admin_settings_item_info">
                    <p class="main_admin_settings_item_info_title">Personal
                        information {{$errors->has('userProfileInformation')}}</p>
                    <div
                        class="main_admin_settings_item_info_content name @if($errors->hasBag('userProfileInformation')) d-none @endif"
                        id="main_admin_settings_item_info_content-userProfileInformation-info">
                        <p class="main_admin_settings_item_info_text">First Name: <b
                                id="fn">{{$user->firstname ?? 'Not Set'}}</b></p>
                        <p class="main_admin_settings_item_info_text">Last Name: <b
                                id="sn">{{$user->lastname ?? 'Not Set'}}</b></p>
                        <button type="button" class="main_admin_settings_item_info_button"
                                id="main_admin_settings_item_info_button-userProfileInformation">Edit
                        </button>
                    </div>
                    <form method="POST" action="{{route('user-profile-information.update')}}"
                          class="main_admin_settings_item_info_content name @if(!$errors->hasBag('userProfileInformation')) d-none @endif"
                          id="main_admin_settings_item_info_content-userProfileInformation-form">
                        @method('PUT')
                        @csrf
                        <input
                            class="main_admin_settings_item_info_content_fn" placeholder="First Name" type="text"
                            name="firstname" value="{{old('firstname', $user->firstname)}}">
                        @if($errors->hasBag('userProfileInformation'))
                            <ul class="main_signup_error_list m-0">
                                @foreach($errors->userProfileInformation->get('firstname') as $error)
                                    <li class="main_signup_error_item text-danger">{{$error}}</li>
                                @endforeach
                            </ul>
                        @endif
                        <input
                            class="main_admin_settings_item_info_content_sn" placeholder="Last Name" type="text"
                            name="lastname" value="{{old('lastname', $user->firstname)}}">
                        @if($errors->hasBag('userProfileInformation'))
                            <ul class="main_signup_error_list m-0">
                                @foreach($errors->userProfileInformation->get('lastname') as $error)
                                    <li class="main_signup_error_item text-danger">{{$error}}</li>
                                @endforeach
                            </ul>
                        @endif
                        <input type="hidden" name="email" value="{{$user->email}}">
                        <button type="submit" class="main_admin_settings_item_info_content_sn_save">save</button>
                        <button type="button" class="main_admin_settings_item_info_content_sn_cancel"
                                id="main_admin_settings_item_info_content_sn_cancel-userProfileInformation">cancel
                        </button>
                    </form>
                </div>
            </div>
        @endif
        @if (Laravel\Fortify\Features::canUpdateProfileInformation())
            <div class="main_admin_settings_item">
                <img src="{{asset('images/adminpanel/ac4.jpg')}}" alt="Email management">
                <div class="main_admin_settings_item_info">
                    <p class="main_admin_settings_item_info_title">Email management</p>
                    <p class="main_admin_settings_item_info_text">
                        Please, provide us you Email for future subscriptions and proposals!
                    </p>
                    <div
                        class="main_admin_settings_item_info_content email @if($errors->hasBag('userProfileInformationEmail')) d-none @endif"
                        id="main_admin_settings_item_info_content-userProfileInformationEmail-info">
                        <p class="main_admin_settings_item_info_text">Current email: <b id="email">{{$user->email}}</b>
                        </p>
                        <button type="button" class="main_admin_settings_item_info_button"
                                id="main_admin_settings_item_info_button-userProfileInformationEmail">Change email
                        </button>
                    </div>
                    <form method="POST" action="{{route('user-profile-information-email.update')}}"
                          class="main_admin_settings_item_info_content email @if(!$errors->hasBag('userProfileInformationEmail')) d-none @endif"
                          id="main_admin_settings_item_info_content-userProfileInformationEmail-form">
                        @csrf
                        <input
                            class="main_admin_settings_item_info_content_fn" placeholder="Email" type="email"
                            name="email" value="{{old('email', $user->email)}}">
                        @if($errors->hasBag('userProfileInformationEmail'))
                            <ul class="main_signup_error_list m-0">
                                @foreach($errors->userProfileInformationEmail->get('email') as $error)
                                    <li class="main_signup_error_item text-danger">{{$error}}</li>
                                @endforeach
                            </ul>
                        @endif
                        <button type="submit" class="main_admin_settings_item_info_content_sn_save">save</button>
                        <button type="button" class="main_admin_settings_item_info_content_sn_cancel"
                                id="main_admin_settings_item_info_content_sn_cancel-userProfileInformationEmail">cancel
                        </button>
                    </form>
                </div>
            </div>
        @endif
        @if (Laravel\Fortify\Features::enabled(Laravel\Fortify\Features::updatePasswords()))
            <div class="main_admin_settings_item">
                <img src="{{asset('images/adminpanel/ac5.jpg')}}" alt="Edit password">
                <div class="main_admin_settings_item_info">
                    <p class="main_admin_settings_item_info_title">Edit password</p>
                    <p class="main_admin_settings_item_info_text">
                        Your password must be at least 8 characters long. (Try a random sentence like
                        "why_are_3_eyed_cats_such_good_doctors?" to make it both memorable and harder to guess.)
                    </p>
                    <div
                        class="main_admin_settings_item_info_content password @if($errors->hasBag('updatePassword')) d-none @endif"
                        id="main_admin_settings_item_info_content-updatePassword-info">
                        <button type="button" class="main_admin_settings_item_info_button"
                                id="main_admin_settings_item_info_button-updatePassword">Change password
                        </button>
                    </div>
                    <form
                        method="POST" action="{{route('user-password.update')}}"
                        class="main_admin_settings_item_info_content password @if(!$errors->hasBag('updatePassword')) d-none @endif"
                        id="main_admin_settings_item_info_content-updatePassword-form">
                        @method('PUT')
                        @csrf
                        <div class="main_login_container_input_password_container">
                            <input type="password" placeholder="Current Password" class="input_password"
                                   name="current_password">
                            <button type="button" class="password_show_btn"></button>
                        </div>
                        @if($errors->hasBag('updatePassword'))
                            <ul class="main_signup_error_list m-0">
                                @foreach($errors->updatePassword->get('current_password') as $error)
                                    <li class="main_signup_error_item text-danger">{{$error}}</li>
                                @endforeach
                            </ul>
                        @endif
                        <div class="main_login_container_input_password_container">
                            <input type="password" placeholder="New Password" class="input_password" name="password">
                            <button type="button" class="password_show_btn"></button>
                        </div>
                        @if($errors->hasBag('updatePassword'))
                            <ul class="main_signup_error_list m-0">
                                @foreach($errors->updatePassword->get('password') as $error)
                                    <li class="main_signup_error_item text-danger">{{$error}}</li>
                                @endforeach
                            </ul>
                        @endif
                        <div class="main_login_container_input_password_container">
                            <input type="password" placeholder="New Password Confirmation" class="input_password"
                                   name="password_confirmation">
                            <button type="button" class="password_show_btn"></button>
                        </div>
                        <button type="submit" class="main_admin_settings_item_info_content_sn_save">Confirm
                        </button>
                        <button type="button" class="main_admin_settings_item_info_content_sn_cancel"
                                id="main_admin_settings_item_info_content_sn_cancel-updatePassword">cancel
                        </button>
                    </form>
                </div>
            </div>
        @endif
        @if (Laravel\Jetstream\Jetstream::hasAccountDeletionFeatures())
            <div class="main_admin_settings_item">
                <img src="{{asset('images/adminpanel/ac6.jpg')}}" alt="Delete account">
                <div class="main_admin_settings_item_info">
                    <p class="main_admin_settings_item_info_title">Delete account</p>
                    <p class="main_admin_settings_item_info_text">
                        If you delete your account, you won’t be able to manage all your subscriptions from one place
                        anymore. But you can still use your Avast products as long as you have a valid activation code.
                    </p>
                    <div class="main_admin_settings_item_info_content delete">
                        <button type="button" data-toggle="modal" data-target="#delete-modal"
                                class="main_admin_settings_item_delete_account">Delete account
                        </button>
                    </div>
                </div>
            </div>
            <div class="modal fade admin_modal" id="delete-modal" tabindex="-1" role="dialog"
                 aria-labelledby="deleteModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="deleteModalLabel">
                                Are you sure you want to delete your Vim Account?
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Your Vim Account will be permanently deleted, and you’ll lose your ability to manage all
                            your subscriptions from one place. But your Vim subscriptions will still be active (make
                            sure you keep your activation codes!).
                            <br>
                            <br>
                            Are you really sure you want to delete your Vim Account?
                        </div>
                        <form action="{{route('user-profile-information.delete')}}"
                              method="POST"
                              class="modal-footer">
                            @csrf
                            <button type="submit" class="delete_account">Delete account</button>
                        </form>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
