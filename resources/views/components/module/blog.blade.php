<div class="product_slider_privacy">
    <section data-cmp-name="cmp-articles-carousel" class="carousel-slider slider large overflow-visible">
        <div class="container">
            <div class="tiny-slider">
                <div class="tns-outer" id="tns1-ow">
                    <div class="tns-liveregion tns-visually-hidden" aria-live="polite" aria-atomic="true">slide <span
                            class="current">1 to 2</span> of 9
                    </div>
                    <div id="tns1-mw" class="tns-ovh">
                        <div class="tns-inner" id="tns1-iw">
                            <div
                                class="article-items slider-articlesDefault  tns-slider tns-carousel tns-subpixel tns-calc tns-horizontal"
                                id="tns1" style="transition-duration: 0s; transform: translate3d(0px, 0px, 0px);">
                                @foreach($articles as $article)
                                    <a href="{{ route('article.show', ['article' => $article->slug]) }}"
                                       data-cmp-name="cmp-card-notice"
                                       class="card card-small card-secondary notice flat hover tns-item tns-slide-active"
                                       id="tns1-item0">
                                        <div class="card-img-top" style="background-image: url('{{ $article->image }}'); "
                                             aria-label="" role="img"></div>
                                        <div class="card-body">
                                            <div class="card-title h6">{{ $article->title }}</div>
                                            <p class="card-text text-small">{!! $article->short_text !!}</p>
                                        </div>
                                        <div class="card-footer">
                                            <div data-cmp-name="cmp-button" class="btn-wrapper ">
                            <span class="btn btn-link btn-sm btn-icon-right">
                                <span>Learn more</span>
                                <img src="/images/local/arrow-s-right-purple.svg" alt="" class="btn-icon">
                            </span>
                                            </div>
                                        </div>
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="tns-nav" aria-label="Carousel Pagination">
                        <button data-nav="0" aria-controls="tns1" style="" aria-label="Carousel Page 1 (Current Slide)"
                                class="tns-nav-active"></button>
                        <button data-nav="1" tabindex="-1" aria-controls="tns1" style=""
                                aria-label="Carousel Page 2"></button>
                        <button data-nav="2" tabindex="-1" aria-controls="tns1" style=""
                                aria-label="Carousel Page 3"></button>
                        <button data-nav="3" tabindex="-1" aria-controls="tns1" style=""
                                aria-label="Carousel Page 4"></button>
                        <button data-nav="4" tabindex="-1" aria-controls="tns1" style=""
                                aria-label="Carousel Page 5"></button>
                        <button data-nav="5" tabindex="-1" aria-controls="tns1" style=""
                                aria-label="Carousel Page 6"></button>
                        <button data-nav="6" tabindex="-1" aria-controls="tns1" style=""
                                aria-label="Carousel Page 7"></button>
                        <button data-nav="7" tabindex="-1" aria-controls="tns1" style=""
                                aria-label="Carousel Page 8"></button>
                        <button data-nav="8" tabindex="-1" aria-controls="tns1" style="display:none"
                                aria-label="Carousel Page 9"></button>
                    </div>
                </div>
                <div class="slider-controls slider-articlesDefault-controls" aria-label="Carousel Navigation"
                     tabindex="0" style="display: none;">
                    <button class="prev button-circle button-circle-secondary-outline alt-opacity" aria-controls="tns1"
                            tabindex="-1" data-controls="prev" disabled="">
                        <img class="tiny-slider-prev-button btn-icon-normal" src="/images/local/back-s-purple.svg"
                             alt="Previous item">
                        <img class="tiny-slider-prev-button btn-icon-hover" src="/images/local/back-s-white.svg"
                             alt="Previous item">
                    </button>
                    <button class="next button-circle button-circle-primary alt-opacity" aria-controls="tns1"
                            tabindex="-1" data-controls="next">
                        <img class="tiny-slider-next-button" src="/images/local/next-s-white.svg" alt="Next item">
                    </button>
                </div>
            </div>
        </div>
    </section>
</div>
