@extends('layouts.default')
@section('content')
    <section id="top" class="hero hero-left-amoeba">
        <div class="container js-platform-switch">
            <div class="row align-items-center">
                <div class="col-12 col-lg-7 hero-content justify-content-center">
                    <h1 class="hero-headline">Antivirus is your first step to complete online protection</h1>
                    <p class="hero-subheadline text-x-large">Award-winning <a href="/product/{{ $free_antivirus }}">free
                            antivirus</a> that’s light and powerful. <br>Join over 180 million people who trust
                        VimGuard to keep them
                        safe.</p>
                    <div class="hero-btn-group">
                        <div class="js-pc">
                            <div data-browser="chrome">
                                <div data-cmp-name="cmp-button" class="btn-wrapper ">
                                    <a
                                        href="{{ route('product.index') }}"
                                        class="btn btn-lg btn-primary btn-icon-left bi-download-link">
                                        <img src="images/local/win-white.svg" alt=""
                                             class="btn-icon">
                                        <span>BUY NOW</span>
                                    </a>
                                </div>
                            </div>
                            <div data-browser="ie">
                                <div data-cmp-name="cmp-button" class="btn-wrapper ">
                                    <a href="/product/{{ $free_antivirus }}"
                                       data-bi-download-name="FAV-ONLINE"
                                       data-cms-component="button--btn-lg btn-primary"
                                       data-role="download-link" data-download-name="FAV-ONLINE"
                                       data-bi-download-name="FAV-ONLINE"
                                       class="btn btn-lg btn-primary btn-icon-left bi-download-link">
                                        <img src="images/local/win-white.svg" alt=""
                                             class="btn-icon">
                                        <span>BUY NOW</span>
                                    </a>
                                </div>
                            </div>
                            <div data-browser="firefox">
                                <div data-cmp-name="cmp-button" class="btn-wrapper ">
                                    <a
                                        href="/product/{{ $free_antivirus }}"
                                        data-bi-download-name="FAV-ONLINE-DIRECT-1"
                                        data-cms-component="button--btn-lg btn-primary"
                                        data-role="download-link" data-behavior="downloadPopup"
                                        data-download-name="FAV-ONLINE-DIRECT-1"
                                        data-bi-download-name="FAV-ONLINE-DIRECT-1"
                                        class="btn btn-lg btn-primary btn-icon-left bi-download-link">
                                        <img src="images/local/win-white.svg" alt=""
                                             class="btn-icon">
                                        <span>BUY NOW</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="js-mac">
                            <div data-cmp-name="cmp-button" class="btn-wrapper ">
                                <a href="/product/{{ $free_antivirus }}"
                                   data-bi-download-name="MAC-FREE-ONLINE"
                                   data-cms-component="button--btn-lg btn-primary"
                                   data-role="download-link" data-download-name="MAC-FREE-ONLINE"
                                   data-bi-download-name="MAC-FREE-ONLINE"
                                   class="btn btn-lg btn-primary btn-icon-left bi-download-link">
                                    <img src="images/local/mac-white.svg" alt=""
                                         class="btn-icon">
                                    <span>BUY NOW</span>
                                </a>
                            </div>
                        </div>
                        <div class="js-android">
                            <div data-cmp-name="cmp-button" class="btn-wrapper ">
                                <a href="/product/{{ $free_antivirus }}" data-bi-download-name="AMS"
                                   data-cms-component="button--btn-lg btn-primary" data-role="download-link"
                                   data-download-name="AMS"
                                   data-bi-download-name="AMS"
                                   class="btn btn-lg btn-primary btn-icon-left bi-download-link">
                                    <img src="images/local/android-white.svg" alt=""
                                         class="btn-icon">
                                    <span>BUY NOW</span>
                                </a>
                            </div>
                        </div>
                        <div class="js-android-market">
                            <div data-cmp-name="cmp-button" class="btn-wrapper ">
                                <a href="/product/{{ $free_antivirus }}"
                                   data-bi-download-name="AMS-MARKET" data-cms-component="button--btn-lg btn-primary"
                                   data-role="download-link" data-download-name="AMS-MARKET"
                                   data-bi-download-name="AMS-MARKET"
                                   class="btn btn-lg btn-primary btn-icon-left bi-download-link">
                                    <img src="images/local/android-white.svg" alt=""
                                         class="btn-icon">
                                    <span>BUY NOW</span>
                                </a>
                            </div>
                        </div>
                        <div class="js-ios">
                            <div data-cmp-name="cmp-button" class="btn-wrapper ">
                                <a href="/product/{{ $free_antivirus }}" data-bi-download-name="IMS"
                                   data-cms-component="button--btn-lg btn-primary" data-role="download-link"
                                   data-download-name="IMS"
                                   data-bi-download-name="IMS"
                                   class="btn btn-lg btn-primary btn-icon-left bi-download-link">
                                    <img src="images/local/ios-white.svg" alt=""
                                         class="btn-icon">
                                    <span>BUY NOW</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="hero-hint-wrap">
                        <div class="hero-hint">
                            {{--<div class="links-blok js-pc">--}}
                            {{--    <a href="/en-us/for-windows" target="_self" data-role="promo-link" data-campaign="internal"--}}
                            {{--       data-cta="pc-products"><span data-hash="comparison">All PC products</span></a>--}}
                            {{--</div>--}}
                            {{--<div class="links-blok last js-pc">--}}
                            {{--    <a href="/en-us/compare-antivirus" target="_self" data-role="promo-link" data-campaign="internal"--}}
                            {{--       data-cta="compare"><span data-hash="comparison">Compare products</span></a>--}}
                            {{--</div>--}}
                            {{--<div class="links-blok last js-mac">Also available for: <a href="/en-us/free-ios-security">iOS</a>, <a--}}
                            {{--        href="/en-us/free-mobile-security">Android</a>, <a href="/en-us/free-antivirus-download">PC</a>--}}
                            {{--</div>--}}
                            {{--<div class="links-blok last js-android">Also available for: <a--}}
                            {{--        href="/en-us/free-antivirus-download">PC</a>, <a href="/en-us/free-mac-security">Mac</a>, <a--}}
                            {{--        href="/en-us/free-ios-security">iOS</a></div>--}}
                            {{--<div class="links-blok last js-android-market">Also available for: <a--}}
                            {{--        href="/en-us/free-antivirus-download">PC</a>, <a href="/en-us/free-mac-security">Mac</a>, <a--}}
                            {{--        href="/en-us/free-ios-security">iOS</a></div>--}}
                            {{--<div class="links-blok last js-ios">Also available for: <a href="/en-us/free-mac-security">Mac</a>, <a--}}
                            {{--        href="/en-us/free-antivirus-download">PC</a>, <a href="/en-us/free-mobile-security">Android</a>--}}
                            {{--</div>--}}
                            {{--<div class="clearfix"></div>--}}
                            <div class="awards">
                                <div class="awards__item award-1 en">
                                    <div class="awards__item-logo">
                                        <img src="images/topa.png"
                                             alt="Editors' Choice">
                                    </div>
                                    <span class="awards__item-text">2019 <b>Top Rated <br>best</b></span>
                                </div>
                                <div class="awards__item award-2">
                                    <div class="awards__item-logo">
                                        <img src="images/topb.png"
                                             alt="AV comparatives logo">
                                    </div>
                                    <span class="awards__item-text">2019 <b>Top Rated <br>product</b></span>
                                </div>
                                <div class="awards__item award-3">
                                    <div class="awards__item-logo">
                                        <img src="images/topc.png"
                                             alt="AV Test logo">
                                    </div>
                                    <span class="awards__item-text">2019 <b>Top Rated <br>product</b></span>
                                </div>
                                <div class="awards__item award-4">
                                    <div class="awards__item-logo">
                                        <img src="images/topd.png"
                                             alt="SE Labs logo">
                                    </div>
                                    <span class="awards__item-text">2019 <b>Top Rated <br>Survey panels</b></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hero-amoeba">
                    <img
                        src="images/clakj.png"
                        alt="">
                </div>
            </div>
        </div>
    </section>
    <section id="cards" class="section primary">
        <div class="container">
            <h2 class="text-center">Ensure your privacy, security, and performance with VimGuard complete online
                protection
            </h2>
            <div class="row justify-content-center">
                <div class="col-12 col-lg-7">
                    <p class="text-large text-center">Live your best connected life with our comprehensive range of
                        products.
                        Safeguard your online privacy, secure all your devices against threats, and keep them running at
                        peak
                        performance.</p>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-sm-10 col-lg-4 pb-3 p-lg-0">
                    <a href="/product/{{ $premium_antivirus }}" data-role="promo-link" data-cta="pillar-privacy"
                       data-campaign="internal"
                       data-aos="fade-up" data-aos-delay="300" data-cmp-name="cmp-card-notice"
                       class="card card-large card-primary notice flat hover">
                        <div class="card-img-top"
                             style="background-image: url('./images/carta.png'); "
                             aria-label="" role="img"></div>
                        <div class="card-body">
                            <div class="badge badge-primary-alt badge-sm badge-position-middle">Privacy</div>
                            <div class="card-title h4">Keep your online activity completely private</div>
                            <p class="card-text ">Protect your privacy by hiding what you do online and staying safe on
                                unsecured
                                public Wi-Fi. Plus, get access to the content you love without restrictions. It’s easy
                                with our
                                Virtual Private Network (VPN).</p>
                        </div>
                        <div class="card-footer">
                            <div data-cmp-name="cmp-button" class="btn-wrapper ">
                  <span class="btn btn-outline-secondary btn-icon-right">
                    <span>GET PREMIUM SECURITY</span>
                    <img src="images/local/arrow-m-right-purple.svg" alt=""
                         class="btn-icon btn-icon-normal">
                    <img src="images/local/arrow-m-right-white.svg"
                         class="btn-icon btn-icon-hover" alt="">
                  </span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-sm-10 col-lg-4 pb-3 p-lg-0">
                    <a href="/product/{{ $premium_antivirus }}" data-role="promo-link" data-cta="pillar-security"
                       data-campaign="internal"
                       data-aos="fade-up" data-aos-delay="400" data-cmp-name="cmp-card-notice"
                       class="card card-large card-primary notice flat hover">
                        <div class="card-img-top"
                             style="background-image: url('./images/cartb.png'); "
                             aria-label="" role="img"></div>
                        <div class="card-body">
                            <div class="badge badge-primary-alt badge-sm badge-position-middle">Security</div>
                            <div class="card-title h4">Go beyond the essentials with advanced security</div>
                            <p class="card-text ">Stay safe from
                                <object><a class="link-primary-dotted"
                                           href="#" title="Computer viruses">viruses</a></object>
                                ,
                                <object><a class="link-primary-dotted" href="#"
                                           title="Ransomware">ransomware</a></object>
                                , phishing, and hackers with our award-winning
                                antivirus. Plus, get additional advanced security features so you can safely bank and
                                shop online.
                            </p>
                        </div>
                        <div class="card-footer">
                            <div data-cmp-name="cmp-button" class="btn-wrapper ">
                  <span class="btn btn-outline-secondary btn-icon-right">
                    <span>Get premium security</span>
                    <img src="images/local/arrow-m-right-purple.svg" alt=""
                         class="btn-icon btn-icon-normal">
                    <img src="images/local/arrow-m-right-white.svg"
                         class="btn-icon btn-icon-hover" alt="">
                  </span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-sm-10 col-lg-4 pb-3 p-lg-0">
                    <a href="/product/{{ $premium_antivirus }}" data-role="promo-link" data-cta="pillar-performance"
                       data-campaign="internal"
                       data-aos="fade-up" data-aos-delay="500" data-cmp-name="cmp-card-notice"
                       class="card card-large card-primary notice flat hover">
                        <div class="card-img-top"
                             style="background-image: url('./images/cartc.png'); "
                             aria-label="" role="img"></div>
                        <div class="card-body">
                            <div class="badge badge-primary-alt badge-sm badge-position-middle">Performance</div>
                            <div class="card-title h4">Keep your devices junk-free and running like new</div>
                            <p class="card-text ">Clean up junk to free up gigabytes of storage space, boost your
                                device’s speed,
                                and keep it running smoothly. Cleanup Premium makes sure your device performs at its
                                best so you can
                                get the most out of it.</p>
                        </div>
                        <div class="card-footer">
                            <div data-cmp-name="cmp-button" class="btn-wrapper ">
                  <span class="btn btn-outline-secondary btn-icon-right">
                    <span>GET PREMIUM SECURITY</span>
                    <img src="images/local/arrow-m-right-purple.svg" alt=""
                         class="btn-icon btn-icon-normal">
                    <img src="images/local/arrow-m-right-white.svg"
                         class="btn-icon btn-icon-hover" alt="">
                  </span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section data-cmp-name="cmp-media-object" class="ultimate-offer">
        <div class="media-container container inverse">
            <div class="row media media-image-left">
                <div class="col-lg-6 d-none d-lg-inline media-image justify-content-center">
                    <picture>
                        <source media="(min-width: 992px)"
                                srcset="images/comp.png">
                        <img class="media-image-img-desktop"
                             src="images/comp.png" alt=""
                             loading="lazy">
                    </picture>
                </div>
                <div class="col-12 col-lg-5 media-body">
                    <a class="tagline-medium title-tagline" href="#" data-role="promo-link" data-campaign="internal"
                       data-cta="ultimate">
                        <div data-cmp-name="cmp-product-icon" class="product-icon plain small">
                            <img alt=""
                                 src="images/local/product-icon-ultimate.svg">
                        </div>
                        VimGuard Ultimate
                    </a>
                    <h2 class="media-title">One package — everything you need</h2>
                    <div class="media-body-image d-lg-none">
                        <picture>
                            <source media="(max-width: 991px)"
                                    srcset="images/local/vim-ultimate.webp">
                            <img class="media-image-img-mobile"
                                 src="images/local/vim-ultimate.webp" alt=""
                                 width="100%" loading="lazy">
                        </picture>
                    </div>
                    <div class="media-text">
                        <p class="d-lg-block text-security">We've conveniently bundled our core security, privacy, and
                            performance
                            products all into one.</p>
                        <div data-cmp-name="cmp-button" class="btn-wrapper ">
                            <a href="{{ route('product.show', 'ultimate') }}" data-cms-component="button--btn-primary"
                               data-role="promo-link"
                               data-cta="ultimate" data-campaign="internal" class="btn btn-primary btn-icon-right">
                                <span>Get VimGuard Ultimate</span>
                                <img src="images/local/arrow-m-right-white.svg" alt=""
                                     class="btn-icon">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="business-offer media-blade-right">
        <div class="media-container container">
            <div class="row media media-image-right media-amoeba-outlined">
                <div class="col-lg-6 d-none d-lg-inline media-image justify-content-center">
                    <picture>
                        <source media="(min-width: 992px)"
                                srcset="images/secur.png">
                        <img class="media-image-img-desktop"
                             src="images/secur.png" alt=""
                             loading="lazy">
                    </picture>
                    <div class="btn-wrapper business-btn-video">
                        <a data-target="#modal-BusinessVideo" href="#modals-BusinessVideo"
                           data-cms-component="button--btn-outline-secondary" data-role="cta-link"
                           class="btn btn-outline-secondary btn-icon-left">
                            <img src="images/local/play-icon.svg" alt=""
                                 class="btn-icon">
                            <span>About vimguard</span>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-lg-5 media-body">
                    <h2 class="media-title">Did you know more about VimGuard?</h2>
                    <div class="media-body-image d-lg-none">
                        <picture>
                            <source media="(max-width: 991px)"
                                    srcset="images/local/vim-business.webp">
                            <img class="media-image-img-mobile"
                                 src="images/local/vim-business.webp" alt=""
                                 width="100%" loading="lazy">
                        </picture>
                        <div data-cmp-name="cmp-button" class="btn-wrapper business-btn-video">
                            <a
                                data-cms-component="button--btn-outline-secondary" data-role="cta-link"
                                class="btn btn-outline-secondary btn-icon-left">
                                <img src="images/local/play-icon.svg" alt=""
                                     class="btn-icon">
                                <span>About vimguard</span>
                            </a>
                        </div>
                    </div>
                    <div class="media-text">
                        <div data-cmp-name="cmp-button" class="btn-wrapper ">
                            <a href="" data-cms-component="button--btn-primary" data-role="promo-link"
                               data-cta="business" data-campaign="internal" class="btn btn-primary btn-icon-right">
                                <span>About us</span>
                                <img src="images/local/arrow-m-right-white.svg" alt=""
                                     class="btn-icon">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{--<section id="latest-version" class="protection-banner mt-5">--}}
    {{--    <div class="container">--}}
    {{--        <div class="row media media-image-left security">--}}
    {{--            <div class="col-lg-6 d-none d-lg-inline media-image image">--}}
    {{--                <img src="images/big-logo.png"--}}
    {{--                     class="image-mobile-security" alt="">--}}
    {{--            </div>--}}
    {{--            <div class="col-12 col-lg-5 media-body">--}}
    {{--                <a class="label" href="/en-us/3d-protection" data-role="promo-link" data-campaign="internal"--}}
    {{--                   data-cta="premium"></a>--}}
    {{--                <h2 class="media-title inverse">VimGuard Complete Online Protection</h2>--}}
    {{--                <h4 class="inverse text-security">Privacy | Security | Performance</h4>--}}
    {{--                <p class="inverse text-large">Safeguard your privacy, protect your devices, and keep them running smoothly--}}
    {{--                    with our range of privacy, security, and performance products.</p>--}}
    {{--                <div data-cmp-name="cmp-list-icons"--}}
    {{--                     class="list-icons list-icons--vertical list-icons--medium inverse mt-3 mb-5">--}}
    {{--                    <div class="list-icons__item">--}}
    {{--                        <img src="images/local/check-oval.svg" alt=""><span--}}
    {{--                            class="list-icons__text">Easily safeguard your online privacy and protect your personal--}}
    {{--              information</span>--}}
    {{--                    </div>--}}
    {{--                    <div class="list-icons__item">--}}
    {{--                        <img src="images/local/check-oval.svg" alt=""><span--}}
    {{--                            class="list-icons__text">Secure your devices against viruses and other malware, hackers, phishing, and--}}
    {{--              more</span>--}}
    {{--                    </div>--}}
    {{--                    <div class="list-icons__item">--}}
    {{--                        <img src="images/local/check-oval.svg" alt=""><span--}}
    {{--                            class="list-icons__text">Keep your computer, phones, and tablets clutter-free and running like--}}
    {{--              new</span>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div data-cmp-name="cmp-button" class="btn-wrapper ">--}}
    {{--                    <a href="/en-us/3d-protection" data-cms-component="button--btn-primary" data-role="promo-link"--}}
    {{--                       data-cta="Brand-Awareness" data-campaign="HP-Swimlane"--}}
    {{--                       data-internal-campaign="Brand Awareness - Swimlane" class="btn btn-primary btn-icon-right">--}}
    {{--                        <span>LEARN MORE</span>--}}
    {{--                        <img src="images/local/arrow-m-right-white.svg" alt=""--}}
    {{--                             class="btn-icon">--}}
    {{--                    </a>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    {{--</section>--}}
    <div id="modal-BusinessVideo" tabindex="-1" role="dialog" aria-labelledby="modal-BusinessVideo"
         aria-hidden="true" data-cmp-name="cmp-modal" class="modal fade modal--large">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button data-dismiss="modal" aria-label="Close"
                            class="button-circle button-circle-primary-outline modal-close">
                        <img src="images/local/cross-m-purple.svg" alt="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <iframe id="aboutBusinessVideo" width="100%" height="450" srcdoc="
					<style>*{padding:0;margin:0;overflow:hidden}html,body{background:#000;height:100%}img{position:absolute;width:100%;top:0;bottom:0;margin:auto}</style>
					<a href='https://www.youtube-nocookie.com/embed/ogPGX0NHkkQ?enablejsapi=1'>
						<img src='./images/local/vim-business.webp'>
						<img src='./images/local/youtube-play-button.svg' style='width: 66px; position: absolute; left: 41.5%;'>
					</a>" frameborder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture *"
                                allowfullscreen loading="lazy"
                                link="https://www.youtube-nocookie.com/embed/ogPGX0NHkkQ"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<section data-cmp-name="cmp-articles-header" class="articles articles-header">--}}
    {{--    <div class="container">--}}
    {{--        <div class="row align-items-end">--}}
    {{--            <div class="articles-title col-12 col-md-7 col-lg-6">--}}
    {{--                <h2>Get privacy and performance tips, straight from the experts</h2>--}}
    {{--            </div>--}}
    {{--            <div class="articles-text col-12 col-md-5 col-lg-6">--}}
    {{--                <a class='link-primary' href='#'>Read more at VimGuar Academy</a>--}}
    {{--                <img class="article-text-link-icon"--}}
    {{--                     src="images/local/arrow-m-right-purple.svg" alt="">--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    {{--</section>--}}
    {{--<section data-cmp-name="cmp-articles-carousel" class="carousel-slider slider large overflow-visible">--}}
    {{--    <div class="container">--}}
    {{--        <div class="tiny-slider">--}}
    {{--            <div class="article-items slider-articlesDefault">--}}
    {{--                <a href="#" data-cmp-name="cmp-card-notice"--}}
    {{--                   class="card card-small card-secondary notice flat hover">--}}
    {{--                    <div class="card-img-top"--}}
    {{--                         style="background-image: url('./images/local/Computer_Virus-Thumb.webp'); "--}}
    {{--                         aria-label="" role="img"></div>--}}
    {{--                    <div class="card-body">--}}
    {{--                        <div class="card-title h6">The Essential Guide to Computer Viruses</div>--}}
    {{--                        <p class="card-text text-small">A computer virus is a type of malware that can spread quickly between--}}
    {{--                            computers and other devices. Learn how viruses work and how to protect against them.</p>--}}
    {{--                    </div>--}}
    {{--                    <div class="card-footer">--}}
    {{--                        <div data-cmp-name="cmp-button" class="btn-wrapper ">--}}
    {{--              <span class="btn btn-link btn-sm btn-icon-right">--}}
    {{--                <span>Learn more</span>--}}
    {{--                <img src="images/local/arrow-s-right-purple.svg" alt=""--}}
    {{--                     class="btn-icon">--}}
    {{--              </span>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </a>--}}
    {{--                <a href="#" data-cmp-name="cmp-card-notice"--}}
    {{--                   class="card card-small card-secondary notice flat hover">--}}
    {{--                    <div class="card-img-top"--}}
    {{--                         style="background-image: url('./images/local/What_is_a_VPN_and_how_does_it_work-Your_Essential_Guide-Thumb.webp'); "--}}
    {{--                         aria-label="" role="img"></div>--}}
    {{--                    <div class="card-body">--}}
    {{--                        <div class="card-title h6">What is a VPN &amp; How Does it Work?</div>--}}
    {{--                        <p class="card-text text-small">A VPN is a secure, encrypted connection that protects your privacy--}}
    {{--                            online. Learn what VPNs are, how they work, and why they&#039;re essential to your security.</p>--}}
    {{--                    </div>--}}
    {{--                    <div class="card-footer">--}}
    {{--                        <div data-cmp-name="cmp-button" class="btn-wrapper ">--}}
    {{--              <span class="btn btn-link btn-sm btn-icon-right">--}}
    {{--                <span>Learn more</span>--}}
    {{--                <img src="images/local/arrow-s-right-purple.svg" alt=""--}}
    {{--                     class="btn-icon">--}}
    {{--              </span>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </a>--}}
    {{--                <a href="#" data-cmp-name="cmp-card-notice"--}}
    {{--                   class="card card-small card-secondary notice flat hover">--}}
    {{--                    <div class="card-img-top"--}}
    {{--                         style="background-image: url('./images/local/How_to_Increase_Your_Internet_Speed-Thumb-1.jpg'); "--}}
    {{--                         aria-label="" role="img"></div>--}}
    {{--                    <div class="card-body">--}}
    {{--                        <div class="card-title h6">How to Increase Your Internet Speed... Right Now!</div>--}}
    {{--                        <p class="card-text text-small">Wondering why your internet is so slow? Read our full guide to find out--}}
    {{--                            how to improve your connection right now, whether you&#039;re on Wi-Fi or Ethernet.</p>--}}
    {{--                    </div>--}}
    {{--                    <div class="card-footer">--}}
    {{--                        <div data-cmp-name="cmp-button" class="btn-wrapper ">--}}
    {{--              <span class="btn btn-link btn-sm btn-icon-right">--}}
    {{--                <span>Learn more</span>--}}
    {{--                <img src="images/local/arrow-s-right-purple.svg" alt=""--}}
    {{--                     class="btn-icon">--}}
    {{--              </span>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </a>--}}
    {{--                <a href="#" data-cmp-name="cmp-card-notice"--}}
    {{--                   class="card card-small card-secondary notice flat hover">--}}
    {{--                    <div class="card-img-top"--}}
    {{--                         style="background-image: url('./images/local/Hacker-Thumb.webp'); "--}}
    {{--                         aria-label="" role="img"></div>--}}
    {{--                    <div class="card-body">--}}
    {{--                        <div class="card-title h6">What Is Hacking?</div>--}}
    {{--                        <p class="card-text text-small">We all have some concept of hacking - but do you really know what it is?--}}
    {{--                            Read our full hacking definition here.</p>--}}
    {{--                    </div>--}}
    {{--                    <div class="card-footer">--}}
    {{--                        <div data-cmp-name="cmp-button" class="btn-wrapper ">--}}
    {{--              <span class="btn btn-link btn-sm btn-icon-right">--}}
    {{--                <span>Learn more</span>--}}
    {{--                <img src="images/local/arrow-s-right-purple.svg" alt=""--}}
    {{--                     class="btn-icon">--}}
    {{--              </span>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </a>--}}
    {{--                <a href="#" data-cmp-name="cmp-card-notice"--}}
    {{--                   class="card card-small card-secondary notice flat hover">--}}
    {{--                    <div class="card-img-top"--}}
    {{--                         style="background-image: url('./images/local/Academy-Is-Snapchat-safe-for-kids-Thumb.webp'); "--}}
    {{--                         aria-label="" role="img"></div>--}}
    {{--                    <div class="card-body">--}}
    {{--                        <div class="card-title h6">Is Snapchat Safe for Kids? A Parents’ Guide to Snapchat</div>--}}
    {{--                        <p class="card-text text-small">Is Snapchat safe for kids? What is it and how does it work? Here&#039;s--}}
    {{--                            everything that curious parents need to know about Snapchat and its most common dangers.</p>--}}
    {{--                    </div>--}}
    {{--                    <div class="card-footer">--}}
    {{--                        <div data-cmp-name="cmp-button" class="btn-wrapper ">--}}
    {{--              <span class="btn btn-link btn-sm btn-icon-right">--}}
    {{--                <span>Learn more</span>--}}
    {{--                <img src="images/local/arrow-s-right-purple.svg" alt=""--}}
    {{--                     class="btn-icon">--}}
    {{--              </span>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </a>--}}
    {{--                <a href="#" data-cmp-name="cmp-card-notice"--}}
    {{--                   class="card card-small card-secondary notice flat hover">--}}

    {{--                    <div class="card-img-top"--}}
    {{--                         style="background-image: url('./images/local/Academy-How-to-check-if-your-VPN-is-working-Thumb.jpg'); "--}}
    {{--                         aria-label="" role="img"></div>--}}
    {{--                    <div class="card-body">--}}
    {{--                        <div class="card-title h6">How to Check If Your VPN Is Working</div>--}}
    {{--                        <p class="card-text text-small">Learn how to check whether your VPN is working correctly. Avoid common--}}
    {{--                            security flaws like DNS leaks and keep your IP, location, and browsing habits private.</p>--}}
    {{--                    </div>--}}
    {{--                    <div class="card-footer">--}}
    {{--                        <div data-cmp-name="cmp-button" class="btn-wrapper ">--}}
    {{--              <span class="btn btn-link btn-sm btn-icon-right">--}}
    {{--                <span>Learn more</span>--}}
    {{--                <img src="images/local/arrow-s-right-purple.svg" alt=""--}}
    {{--                     class="btn-icon">--}}
    {{--              </span>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </a>--}}
    {{--                <a href="#" data-cmp-name="cmp-card-notice"--}}
    {{--                   class="card card-small card-secondary notice flat hover">--}}
    {{--                    <div class="card-img-top"--}}
    {{--                         style="background-image: url('./images/local/Academy-How-to-free-up-disk-space-Thumb.jpg'); "--}}
    {{--                         aria-label="" role="img"></div>--}}
    {{--                    <div class="card-body">--}}
    {{--                        <div class="card-title h6">How to Free Up Disk Space</div>--}}
    {{--                        <p class="card-text text-small">Is your computer slow or struggling to install the latest updates? Learn--}}
    {{--                            how to clear disk space and clean up hard drives on a Mac or Windows PC.</p>--}}
    {{--                    </div>--}}
    {{--                    <div class="card-footer">--}}
    {{--                        <div data-cmp-name="cmp-button" class="btn-wrapper ">--}}
    {{--              <span class="btn btn-link btn-sm btn-icon-right">--}}
    {{--                <span>Learn more</span>--}}
    {{--                <img src="images/local/arrow-s-right-purple.svg" alt=""--}}
    {{--                     class="btn-icon">--}}
    {{--              </span>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </a>--}}
    {{--                <a href="#" data-cmp-name="cmp-card-notice"--}}
    {{--                   class="card card-small card-secondary notice flat hover">--}}
    {{--                    <div class="card-img-top"--}}
    {{--                         style="background-image: url('./images/local/What-is-a-dark-web-scan-Thumb.webp'); "--}}
    {{--                         aria-label="" role="img"></div>--}}
    {{--                    <div class="card-body">--}}
    {{--                        <div class="card-title h6">Everything You Need to Know About Dark Web Scanning</div>--}}
    {{--                        <p class="card-text text-small">Worried your information has landed on the dark web? Learn about dark--}}
    {{--                            web scanners and what you can do if your email is found on the dark web.</p>--}}
    {{--                    </div>--}}
    {{--                    <div class="card-footer">--}}
    {{--                        <div data-cmp-name="cmp-button" class="btn-wrapper ">--}}
    {{--              <span class="btn btn-link btn-sm btn-icon-right">--}}
    {{--                <span>Learn more</span>--}}
    {{--                <img src="images/local/arrow-s-right-purple.svg" alt=""--}}
    {{--                     class="btn-icon">--}}
    {{--              </span>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </a>--}}
    {{--                <a href="#" data-cmp-name="cmp-card-notice"--}}
    {{--                   class="card card-small card-secondary notice flat hover">--}}
    {{--                    <div class="card-img-top"--}}
    {{--                         style="background-image: url('./images/local/Academy-How-to-defrag-your-Mac-hard-drive-Thumb.jpg'); "--}}
    {{--                         aria-label="" role="img"></div>--}}
    {{--                    <div class="card-body">--}}
    {{--                        <div class="card-title h6">How to Defrag Your Mac Hard Drive</div>--}}
    {{--                        <p class="card-text text-small">Find out how to defrag a Mac and whether it&#039;s necessary, or--}}
    {{--                            discover alternatives to defragmentation. Keep your Mac&#039;s drive and disk in order.</p>--}}
    {{--                    </div>--}}
    {{--                    <div class="card-footer">--}}
    {{--                        <div data-cmp-name="cmp-button" class="btn-wrapper ">--}}
    {{--              <span class="btn btn-link btn-sm btn-icon-right">--}}
    {{--                <span>Learn more</span>--}}
    {{--                <img src="images/local/arrow-s-right-purple.svg" alt=""--}}
    {{--                     class="btn-icon">--}}
    {{--              </span>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </a>--}}
    {{--            </div>--}}
    {{--            <div class="slider-controls slider-articlesDefault-controls">--}}
    {{--                <button class="prev button-circle button-circle-secondary-outline alt-opacity">--}}
    {{--                    <img class="tiny-slider-prev-button btn-icon-normal"--}}
    {{--                         src="images/local/back-s-purple.svg" alt="Previous item">--}}
    {{--                    <img class="tiny-slider-prev-button btn-icon-hover"--}}
    {{--                         src="images/local/back-s-white.svg" alt="Previous item">--}}
    {{--                </button>--}}
    {{--                <button class="next button-circle button-circle-primary alt-opacity">--}}
    {{--                    <img class="tiny-slider-next-button"--}}
    {{--                         src="images/local/next-s-white.svg" alt="Next item">--}}
    {{--                </button>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    {{--</section>--}}
    <section class="world-security js-world-security inverse bg-plum mt-5">
        <div class="container-transition">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="orbits-container animated-svg">
                            <div class="world-security-top">
                                <h1 class="text-center">Keeping people safe & secure around the world</h1>
                                <p class="subh1 text-x-large text-center">Using real-time intelligence from over 180
                                    million VimGuard
                                    users, we prevent more than 66 million threats every day.</p>
                            </div>
                            <div class="globe-container">
                                <div class="globe-earth">
                                    <div class="globe-earth-round"></div>
                                    <img src="images/planet.png" alt="">
                                </div>
                            </div>
                            <div class="world-security-middle">
                                <div class="info-data tagline-medium left">
                                    <div class="info-num" data-animation="fadein">
                                        <div class="info-num" data-animation="fadein"><span class="info-count"><span
                                                    class="js-info"
                                                    data-count="180">0</span><span>M</span></span><span
                                                data-animation="fadein"
                                                class="info-text">people trust VimGuard</span></div>
                                    </div>
                                </div>
                                <div class="info-center">
                                    <div class="js-platform-switch platform-switch">
                                        <div class="js-pc">
                                            <div data-browser="chrome">
                                                <div data-cmp-name="cmp-button" class="btn-wrapper ">
                                                    <a
                                                        href="/product/{{ $free_antivirus }}"
                                                        data-bi-download-name="FAV-ONLINE"
                                                        data-cms-component="button--btn-lg btn-primary"
                                                        data-role="download-link" data-download-name="FAV-ONLINE"
                                                        data-bi-download-name="FAV-ONLINE"
                                                        class="btn btn-lg btn-primary btn-icon-left bi-download-link">
                                                        <img src="images/local/win-white.svg" alt=""
                                                             class="btn-icon">
                                                        <span>DOWNLOAD FREE PROTECTION</span>
                                                    </a>
                                                </div>
                                            </div>
                                            <div data-browser="ie">
                                                <div data-cmp-name="cmp-button" class="btn-wrapper ">
                                                    <a
                                                        href="/product/{{ $free_antivirus }}"
                                                        data-bi-download-name="FAV-ONLINE"
                                                        data-cms-component="button--btn-lg btn-primary"
                                                        data-role="download-link" data-download-name="FAV-ONLINE"
                                                        data-bi-download-name="FAV-ONLINE"
                                                        class="btn btn-lg btn-primary btn-icon-left bi-download-link">
                                                        <img src="images/local/win-white.svg" alt=""
                                                             class="btn-icon">
                                                        <span>DOWNLOAD FREE PROTECTION</span>
                                                    </a>
                                                </div>
                                            </div>
                                            <div data-browser="firefox">
                                                <div data-cmp-name="cmp-button" class="btn-wrapper ">
                                                    <a
                                                        href="/product/{{ $free_antivirus }}"
                                                        data-bi-download-name="FAV-ONLINE-DIRECT-1"
                                                        data-cms-component="button--btn-lg btn-primary"
                                                        data-role="download-link"
                                                        data-behavior="downloadPopup"
                                                        data-download-name="FAV-ONLINE-DIRECT-1"
                                                        data-bi-download-name="FAV-ONLINE-DIRECT-1"
                                                        class="btn btn-lg btn-primary btn-icon-left bi-download-link">
                                                        <img src="images/local/win-white.svg" alt=""
                                                             class="btn-icon">

                                                        <span>DOWNLOAD FREE PROTECTION</span>
                                                    </a>
                                                </div>
                                            </div>
                                            <p class="button-hint text-center">Also available for: <a
                                                    href="/product/{{ $free_antivirus }}">Android</a>, <a
                                                    href="/product/{{ $free_antivirus }}">iOS</a>,
                                                <a href="/product/{{ $free_antivirus }}">Mac</a></p>
                                        </div>
                                        <div class="js-mac">
                                            <div data-cmp-name="cmp-button" class="btn-wrapper ">
                                                <a href="/product/{{ $free_antivirus }}"
                                                   data-bi-download-name="MAC-FREE-ONLINE"
                                                   data-cms-component="button--btn-lg btn-primary"
                                                   data-role="download-link" data-download-name="MAC-FREE-ONLINE"
                                                   data-bi-download-name="MAC-FREE-ONLINE"
                                                   class="btn btn-lg btn-primary btn-icon-left bi-download-link">
                                                    <img src="images/local/mac-white.svg" alt=""
                                                         class="btn-icon">
                                                    <span>START WITH FREE PROTECTION</span>
                                                </a>
                                            </div>
                                            <p class="button-hint text-center">Also available for: <a
                                                    href="/product/{{ $free_antivirus }}">iOS</a>, <a
                                                    href="/product/{{ $free_antivirus }}">Android</a>,
                                                <a href="/product/{{ $free_antivirus }}">PC</a></p>
                                        </div>
                                        <div class="js-android">
                                            <div data-cmp-name="cmp-button" class="btn-wrapper ">
                                                <a href="/product/{{ $free_antivirus }}" data-bi-download-name="AMS"
                                                   data-cms-component="button--btn-lg btn-primary"
                                                   data-role="download-link"
                                                   data-download-name="AMS" data-bi-download-name="AMS"
                                                   class="btn btn-lg btn-primary btn-icon-left bi-download-link">
                                                    <img src="images/local/android-white.svg" alt=""
                                                         class="btn-icon">
                                                    <span>START WITH FREE PROTECTION</span>
                                                </a>
                                            </div>
                                            <p class="button-hint text-center">Also available for: <a
                                                    href="/product/{{ $free_antivirus }}">PC</a>, <a
                                                    href="/product/{{ $free_antivirus }}">Mac</a>, <a
                                                    href="/product/{{ $free_antivirus }}">iOS</a></p>
                                        </div>
                                        <div class="js-android-market">
                                            <div data-cmp-name="cmp-button" class="btn-wrapper ">
                                                <a href="/product/{{ $free_antivirus }}"
                                                   data-bi-download-name="AMS-MARKET"
                                                   data-cms-component="button--btn-lg btn-primary"
                                                   data-role="download-link" data-download-name="AMS-MARKET"
                                                   data-bi-download-name="AMS-MARKET"
                                                   class="btn btn-lg btn-primary btn-icon-left bi-download-link">
                                                    <img src="images/local/android-white.svg" alt=""
                                                         class="btn-icon">
                                                    <span>START WITH FREE PROTECTION</span>
                                                </a>
                                            </div>
                                            <p class="button-hint text-center">Also available for: <a
                                                    href="/product/{{ $free_antivirus }}">PC</a>, <a
                                                    href="/product/{{ $free_antivirus }}">Mac</a>, <a
                                                    href="/product/{{ $free_antivirus }}">iOS</a></p>
                                        </div>
                                        <div class="js-ios">
                                            <div data-cmp-name="cmp-button" class="btn-wrapper ">
                                                <a href="/product/{{ $free_antivirus }}" data-bi-download-name="IMS"
                                                   data-cms-component="button--btn-lg btn-primary"
                                                   data-role="download-link"
                                                   data-download-name="IMS" data-bi-download-name="IMS"
                                                   class="btn btn-lg btn-primary btn-icon-left bi-download-link">
                                                    <img src="images/local/ios-white.svg" alt=""
                                                         class="btn-icon">
                                                    <span>START WITH FREE PROTECTION</span>
                                                </a>
                                            </div>
                                            <p class="button-hint text-center">Also available for: <a
                                                    href="/product/{{ $free_antivirus }}">Mac</a>, <a
                                                    href="/product/{{ $free_antivirus }}">PC</a>, <a
                                                    href="/product/{{ $free_antivirus }}">Android</a></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="info-data tagline-medium right">
                                    <div class="info-num" data-animation="fadein">
                                        <div class="info-num" data-animation="fadein"><span class="info-count"><span
                                                    class="js-info"
                                                    data-count="66">0</span><span>M</span></span><span
                                                data-animation="fadein"
                                                class="info-text">threats blocked every day</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{--<div id="download-popup-ds" tabindex="-1" role="dialog" aria-labelledby="download-popup-ds" aria-hidden="true"--}}
    {{--     data-cmp-name="cmp-modal" class="modal fade modal--full modal--inverse">--}}
    {{--    <div class="modal-dialog" role="document">--}}
    {{--        <div class="modal-content inverse">--}}
    {{--            <div class="modal-header">--}}
    {{--                <button data-dismiss="modal" aria-label="Close"--}}
    {{--                        class="button-circle button-circle-primary-outline modal-close">--}}
    {{--                    <img src="images/local/cross-m-purple.svg" alt="Close">--}}
    {{--                </button>--}}
    {{--            </div>--}}
    {{--            <div class="modal-body">--}}
    {{--                <div class="dtp-arrow-flex">--}}

    {{--                    <div class="dtp-arrow-ds">--}}
    {{--                        <div class="dtp-arrow-header">--}}
    {{--                            <img--}}
    {{--                                src="images/big-logo.png"--}}
    {{--                                alt="VimGuard logo">--}}
    {{--                        </div>--}}
    {{--                        <div class="dtp-arrow-body">--}}
    {{--                            <p>Click this file<br>to start<br>installing<br>VimGuard.</p>--}}
    {{--                        </div>--}}
    {{--                        <div class="dtp-arrow-footer">--}}
    {{--                            <div class="dtp-arrow-footer-img">--}}
    {{--                                <img--}}
    {{--                                    src="images/local/dtp-icon-arrow-down.svg"--}}
    {{--                                    alt="Download">--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="text-center">--}}
    {{--                    <h2>Follow these steps to complete your VimGuard installation:</h2>--}}
    {{--                    <p class="text-large dtp-text">--}}
    {{--                        <strong>Note</strong>: If your download did not start automatically, please--}}
    {{--                        <a href="/en-us/download-thank-you.php?product=FAV-ONLINE-GLOWEB-1072-B-DIRECT&locale=en-us"--}}
    {{--                           data-bi-download-name="FAV-ONLINE-GLOWEB-1072-B-DIRECT"--}}
    {{--                           class="link-secondary text-large bi-download-link dtp-button bi-download-link"--}}
    {{--                           data-role="download-link" data-download-name="FAV-ONLINE-GLOWEB-1072-B-DIRECT"--}}
    {{--                           data-behavior="downloadPage">click here</a>--}}
    {{--                    </p>--}}
    {{--                    <div class="row justify-content-center">--}}
    {{--                        <div class="col-12 col-lg-10 col-xl-9">--}}
    {{--                            <div class="row justify-content-center">--}}
    {{--                                <div class="col-12 col-lg-3">--}}
    {{--                                    <div data-cmp-name="cmp-card-feature-item"--}}
    {{--                                         class="card card-medium card-primary feature-item  card-centered flat">--}}
    {{--                                        <div class="feature-item-left">--}}
    {{--                                            <div class="feature-item__img">--}}
    {{--                                                <img--}}
    {{--                                                    src="images/local/dtp-icon-download.svg"--}}
    {{--                                                    alt="">--}}
    {{--                                            </div>--}}
    {{--                                        </div>--}}
    {{--                                        <div class="feature-item-right">--}}
    {{--                                            <h6 class="feature-item__title">--}}
    {{--                                                1. Download--}}
    {{--                                            </h6>--}}
    {{--                                            <div class="feature-item__desc">--}}
    {{--                                                Download the VimGuard installation file.--}}
    {{--                                            </div>--}}
    {{--                                        </div>--}}
    {{--                                    </div>--}}
    {{--                                </div>--}}
    {{--                                <div class="col-12 col-lg-1 d-none d-lg-block">--}}
    {{--                                    <div class="dtp-next-step">--}}
    {{--                                        <img--}}
    {{--                                            src="images/local/dtp-icon-stroke.svg"--}}
    {{--                                            alt="next">--}}
    {{--                                    </div>--}}
    {{--                                </div>--}}
    {{--                                <div class="col-12 col-lg-3">--}}
    {{--                                    <div data-cmp-name="cmp-card-feature-item"--}}
    {{--                                         class="card card-medium card-primary feature-item  card-centered flat">--}}
    {{--                                        <div class="feature-item-left">--}}
    {{--                                            <div class="feature-item__img">--}}
    {{--                                                <img--}}
    {{--                                                    src="images/local/dtp-icon-file.svg"--}}
    {{--                                                    alt="">--}}
    {{--                                            </div>--}}
    {{--                                        </div>--}}
    {{--                                        <div class="feature-item-right">--}}
    {{--                                            <h6 class="feature-item__title">--}}
    {{--                                                2. Open the file--}}
    {{--                                            </h6>--}}
    {{--                                            <div class="feature-item__desc">--}}
    {{--                                                Run the installation by clicking on downloaded file.--}}
    {{--                                            </div>--}}
    {{--                                        </div>--}}
    {{--                                    </div>--}}
    {{--                                </div>--}}
    {{--                                <div class="col-12 col-lg-1 ds-none d-none d-lg-block">--}}
    {{--                                    <div class="dtp-next-step">--}}
    {{--                                        <img--}}
    {{--                                            src="images/local/dtp-icon-stroke.svg"--}}
    {{--                                            alt="next">--}}
    {{--                                    </div>--}}
    {{--                                </div>--}}
    {{--                                <div class="col-12 col-lg-3">--}}
    {{--                                    <div data-cmp-name="cmp-card-feature-item"--}}
    {{--                                         class="card card-medium card-primary feature-item  card-centered flat">--}}
    {{--                                        <div class="feature-item-left">--}}
    {{--                                            <div class="feature-item__img">--}}
    {{--                                                <img--}}
    {{--                                                    src="images/local/dtp-icon-instructions.svg"--}}
    {{--                                                    alt="">--}}
    {{--                                            </div>--}}
    {{--                                        </div>--}}
    {{--                                        <div class="feature-item-right">--}}
    {{--                                            <h6 class="feature-item__title">--}}
    {{--                                                3. Follow instructions--}}
    {{--                                            </h6>--}}
    {{--                                            <div class="feature-item__desc">--}}
    {{--                                                Follow the on-screen instructions.--}}
    {{--                                            </div>--}}
    {{--                                        </div>--}}
    {{--                                    </div>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    {{--</div>--}}
    {{--<div class="js-popup-dtp popup-dtp" id="downloadPopup" data-behavior="downloadPopup-gloweb-1072" data-role="popup"--}}
    {{--     style="display: none">--}}
    {{--    <div data-browser="chrome">--}}
    {{--        <div class="dtp-arrow">--}}
    {{--            <div class="dtp-arrow-header"></div>--}}
    {{--            <div class="dtp-arrow-body">--}}
    {{--                <p>Click this file to start installing VimGuard</p>--}}
    {{--            </div>--}}
    {{--            <div class="dtp-arrow-footer"></div>--}}
    {{--        </div>--}}
    {{--        <div class="js-dtp-close dtp-close">--}}
    {{--            <span class="text">Close</span>--}}
    {{--            <span class="cross"></span>--}}
    {{--        </div>--}}
    {{--        <div class="content">--}}
    {{--            <div id="dwnTy-download-popup" class="dwnTy">--}}
    {{--                <div class="AVsizer">--}}
    {{--                    <div class="center">--}}
    {{--                        <h1>Follow these steps to complete your VimGuard installation:</h1>--}}
    {{--                        <p class="noteProgress" style="display: none;"><span></span></p>--}}
    {{--                        <div data-abtest="GLOWEB-1072-CONTROL">--}}
    {{--                            <p class="note noteHidden" style="display: inline-block;"><strong>Note</strong>: If your download did--}}
    {{--                                not start automatically, please <a--}}
    {{--                                    href="/en-us/download-thank-you.php?product=FAV-ONLINE&locale=en-us"--}}
    {{--                                    data-bi-download-name="FAV-ONLINE" class="bi-download-link" data-role="download-link"--}}
    {{--                                    data-download-name="FAV-ONLINE">click here</a>.</p>--}}
    {{--                        </div>--}}
    {{--                        <div data-abtest="GLOWEB-1072-A">--}}
    {{--                            <p class="note noteHidden" style="display: inline-block;"><strong>Note</strong>: If your download did--}}
    {{--                                not start automatically, please <a--}}
    {{--                                    href="/en-us/download-thank-you.php?product=FAV-ONLINE-GLOWEB-1072-A&locale=en-us"--}}
    {{--                                    data-bi-download-name="FAV-ONLINE-GLOWEB-1072-A" class="bi-download-link"--}}
    {{--                                    data-role="download-link" data-download-name="FAV-ONLINE-GLOWEB-1072-A">click here</a>.</p>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                    <div class="boxes">--}}
    {{--                        <div class="row">--}}
    {{--                            <div class="span4">--}}
    {{--                                <div class="box">--}}
    {{--                                    <span class="step"><span>1</span></span>--}}
    {{--                                    <div class="box-in">--}}
    {{--                                        <div class="same-height" style="height: 135px;">--}}
    {{--                                            <h2>Run the VimGuard installer</h2>--}}
    {{--                                            <p>Click the downloaded file at the bottom left corner of your screen.</p>--}}
    {{--                                        </div>--}}
    {{--                                        <!-- exe file image-->--}}
    {{--                                        <img src="" alt="">--}}
    {{--                                    </div>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                            <div class="span4">--}}
    {{--                                <div class="box">--}}
    {{--                                    <span class="step"><span>2</span></span>--}}
    {{--                                    <div class="box-in">--}}
    {{--                                        <div class="same-height" style="height: 135px;">--}}
    {{--                                            <h2>Confirm the installation</h2>--}}
    {{--                                            <p>Click "Yes" on the system dialog window to approve the start of your VimGuard installation.--}}
    {{--                                            </p>--}}
    {{--                                        </div>--}}
    {{--                                        <img src="images/local/ch-step2.png" alt="">--}}
    {{--                                    </div>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                            <div class="span4">--}}
    {{--                                <div class="box">--}}
    {{--                                    <span class="step"><span>3</span></span>--}}
    {{--                                    <div class="box-in">--}}
    {{--                                        <div class="same-height" style="height: 135px;">--}}
    {{--                                            <h2>Follow setup instructions</h2>--}}
    {{--                                            <p>Click the button in the installer window to begin installation.</p>--}}
    {{--                                        </div>--}}
    {{--                                        <img src="images/local/express-install.png" alt="">--}}
    {{--                                    </div>--}}

    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <div class="clear"> </div>--}}
    {{--                    </div>--}}
    {{--                    <p class="offlinep" style="display:none">--}}
    {{--                        If you need to install VimGuard on a PC without an internet connection,<br> you can download the offline--}}
    {{--                        installer <a href="/en-us/download-thank-you.php?product=FAV-ONLINE-DIRECT&locale=en-us"--}}
    {{--                                     data-bi-download-name="FAV-ONLINE-DIRECT" class="bi-download-link" data-role="download-link"--}}
    {{--                                     data-download-name="FAV-ONLINE">here</a>.--}}
    {{--                    </p>--}}
    {{--                    <div class="techSupport">--}}
    {{--                        <img src="images/local/icon-phone.svg" alt="">--}}
    {{--                        <p><b>Need help?</b> Please call <span>855-745-3255</span></p>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    {{--    <div data-browser="firefox" id="popup-dtp-firefox" style="display: none">--}}
    {{--        <div class="dtp-arrow-title">--}}
    {{--            <p>Click this file to start <br>installing VimGuard</p>--}}
    {{--        </div>--}}
    {{--        <div class="dtp-arrow firefox">--}}
    {{--            <div class="dtp-arrow-body">--}}
    {{--                <div id="animation_container" style="background-color:rgba(36, 44, 65, 1.00); width:56px; height:56px">--}}
    {{--                    <canvas id="canvas" width="56" height="56"></canvas>--}}
    {{--                    <div id="dom_overlay_container"--}}
    {{--                         style="pointer-events:none; overflow:hidden; width:56px; height:56px; position: absolute; left: 0px; top: 0px; display: block;">--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <img class="arrow-top"--}}
    {{--                     src="images/local/dtp-ff-arrow-website-698.svg" alt="">--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--        <div class="js-dtp-close dtp-close">--}}
    {{--            <span class="text">CLOSE</span>--}}
    {{--            <span class="cross"></span>--}}
    {{--        </div>--}}
    {{--        <div class="content">--}}
    {{--            <div id="dwnTy-download-popup-firefox" class="dwnTy">--}}
    {{--                <div class="AVsizer">--}}
    {{--                    <div class="center">--}}
    {{--                        <h1>Almost done!</h1>--}}
    {{--                        <p class="info">Follow these 3 easy steps to complete your VimGuard installation</p>--}}
    {{--                        <p class="noteProgress" style="display: none;"><span></span></p>--}}
    {{--                        <p class="note noteHidden firefox" style="display: inline-block;"><strong>Note</strong>: If your--}}
    {{--                            download did not start automatically, please <a--}}
    {{--                                href="/en-us/download-thank-you.php?product=FAV-ONLINE&locale=en-us"--}}
    {{--                                data-bi-download-name="FAV-ONLINE" class="bi-download-link" data-role="download-link"--}}
    {{--                                data-download-name="FAV-ONLINE">click here</a>.</p>--}}
    {{--                    </div>--}}
    {{--                    <div class="boxes">--}}
    {{--                        <div class="row">--}}
    {{--                            <div class="span4">--}}
    {{--                                <div class="box firefox">--}}
    {{--                                    <span class="step"><span>Step 1</span></span>--}}
    {{--                                    <div class="box-in">--}}
    {{--                                        <div class="same-height" style="height: 135px;">--}}
    {{--                                            <h2>Run the VimGuard installer</h2>--}}
    {{--                                            <p>Click the downloaded file at the <br />top right corner of your screen.</p>--}}
    {{--                                        </div>--}}
    {{--                                    </div>--}}
    {{--                                    <!-- need exe image -->--}}
    {{--                                    <img src=""--}}
    {{--                                         alt="">--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                            <div class="span4">--}}
    {{--                                <div class="box firefox">--}}
    {{--                                    <span class="step"><span>Step 2</span></span>--}}
    {{--                                    <div class="box-in">--}}
    {{--                                        <div class="same-height" style="height: 135px;">--}}
    {{--                                            <h2>Confirm the installation</h2>--}}
    {{--                                            <p>Click "Yes" on the system dialog window to approve the start of your VimGuard installation.--}}
    {{--                                            </p>--}}
    {{--                                        </div>--}}
    {{--                                    </div>--}}
    {{--                                    <img src="images/local/dtp-ff-step2-website-698.png"--}}
    {{--                                         alt="">--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                            <div class="span4">--}}
    {{--                                <div class="box firefox">--}}
    {{--                                    <span class="step"><span>Step 3</span></span>--}}
    {{--                                    <div class="box-in">--}}
    {{--                                        <div class="same-height" style="height: 135px;">--}}
    {{--                                            <h2>Follow setup instructions</h2>--}}
    {{--                                            <p>Click the button in the installer <br />window to begin installation.</p>--}}
    {{--                                        </div>--}}
    {{--                                    </div>--}}
    {{--                                    <img src="images/local/dtp-ff-step3-website-698.png"--}}
    {{--                                         alt="">--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <div class="clear"> </div>--}}
    {{--                    </div>--}}
    {{--                    <p class="offlinep" style="display:none">--}}
    {{--                        Follow setup instructions--}}
    {{--                    </p>--}}
    {{--                    <div class="techSupport">--}}
    {{--                        <img src="images/local/icon-phone.svg" alt="">--}}
    {{--                        <p><b>Need help?</b> Please call <span>855-745-3255</span></p>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    {{--</div>--}}
    {{--<div class="chrome-banner inverse" style="display:none" id="offer-chrome" data-cmp-name="cmp-chrome-banner">--}}
    {{--    <div class="container">--}}
    {{--        <div class="row justify-content-center">--}}
    {{--            <div class="col-12 col-xl-10">--}}
    {{--                <div class="chrome-banner__content">--}}
    {{--                    <div class="chrome-banner__info">--}}
    {{--                        <div class="chrome-banner__ico">--}}
    {{--                            <img src="images/local/chrome.svg" alt="">--}}
    {{--                        </div>--}}
    {{--                        <h4 class="chrome-banner__title">--}}
    {{--                            VimGuard recommends using <br> the <b>FREE</b> Chrome&trade; internet browser.--}}
    {{--                        </h4>--}}
    {{--                    </div>--}}
    {{--                    <div class="chrome-banner__cta">--}}
    {{--                        <div class="d-none d-md-block">--}}
    {{--                            <div data-cmp-name="cmp-button" class="btn-wrapper ">--}}
    {{--                                <a href="https://www.google.com/chrome/" data-cms-component="button--btn-lg btn-outline-secondary"--}}
    {{--                                   data-role="cta-link" class="btn btn-lg btn-outline-secondary">--}}
    {{--                                    <span>Download Chrome</span>--}}
    {{--                                </a>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <a href="https://play.google.com/store/apps/details/Google_Chrome_Fast_Secure?id=com.android.chrome"--}}
    {{--                           class="d-block d-md-none chrome-banner__play" target="_blank" rel="noopener noreferrer"--}}
    {{--                           title="Download Chrome">--}}
    {{--                            <img--}}
    {{--                                src="images/local/google-play.svg"--}}
    {{--                                alt="" data-cmp-name="cmp-store-badge-img" class="storebadge-google ">--}}
    {{--                        </a>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    {{--</div>--}}
@endsection
