@extends('layouts.default')
@section('content')
    <div class="main_signup_container">
        <div class="main_signup_container_left">
            <img style="height: 100%;" src="{{asset('images/adminpanel/looginimage221.jpg')}}" alt="Two people logining in vimguard">
        </div>
        <auth-form wrapper-class="main_signup_container_right" method="POST" action="{{route('register')}}"
                   redirect="{{route('dashboard')}}">

            <template v-slot:email-step>
                <div class="main_signup_container_first_line">
                    <span class="main_signup_container_title">Create an account</span>
                    <a href="{{URL::previous()}}" class="main_signup_container_signup">Back</a>
                </div>
                <p class="main_signup_container_info">
                    if you already shopped with us, please use email address you used at checkout
                </p>

                <!-- Использовать класс "error" когда поле заполнено не правильно -->

                <div class="main_signup_container_input_container">
                    <input type="email" name="email" id="user-email" class="main_signup_container_input_email"
                           placeholder="E-mail" required>
                </div>
                <p class="main_signup_error_info">
                    This account already exists. <a href="#">Sign in instead</a> or create an account using a different
                    email.
                </p>
                <p class="main_signup_container_agree">
                    By clicking “Continue”, you agree witch our <a href="#">License Agreement</a> and <a href="#">Privacy
                        Policy</a>.
                </p>

                <!-- Класс "disabled" используется, когда поле заполнено не правильно -->

                <button type="submit" class="main_login_continue_btn">continue</button>
            </template>
            <template v-slot:password-step>
                <div class="main_signup_container_first_line">
                    <span class="main_signup_container_title">Crate your password</span>
                    <a href="{{URL::previous()}}" class="main_signup_container_signup">Back</a>
                </div>
                <p class="main_signup_container_info">
                    Last step... Create your VimGuard Account password.
                    Make it a good one!
                </p>

                <!-- Отображение ошибки ввода пароля - класс "error_passoword" -->

                <div class="main_login_container_input_password_container form-inputs-container">
                    <input type="password" name="password" id="user-password"
                           class="main_login_container_input_password input_password"
                           placeholder="Password">
                    <button type="button" class="password_show_btn" data-input-id="user-password">
                    </button>
                </div>
                <div class="main_login_container_input_password_container mt-2" id="form-inputs-container">
                    <input type="password" name="password_confirmation" id="user-password"
                           class="main_login_container_input_password input_password"
                           placeholder="Password confirmation">
                    <button type="button" class="password_show_btn" data-input-id="user-password">
                    </button>
                </div>

                <!-- Блок появляется когда у соседнего присутствует класс "error_passoword" -->
                <div class="main_signup_error_password">
                    <ul class="main_signup_error_list" id="form-errors-list">
                        <li class="main_signup_error_item">Must be at least 8 characters</li>
                        <li class="main_signup_error_item">Cannot be a common password</li>
                    </ul>
                </div>

                <!-- Класс "disabled" используется, когда поле заполнено не правильно -->

                <button type="submit" class="main_login_continue_btn">continue</button>
            </template>
        </auth-form>
    </div>
@endsection
