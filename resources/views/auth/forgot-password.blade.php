@extends('layouts.default')
@section('content')
    <main class="main_admin">
        <div class="main_signup_container">
            <div class="main_signup_container_left">
                <img src="{{asset('images/adminpanel/looginimage221.jpg')}}" alt="Two people logining in vimguard">
            </div>
            <form class="main_signup_container_right" method="POST" action="{{route('password.email')}}">
                @csrf
                <div class="main_signup_container_first_line">
                    <span class="main_signup_container_title">Need a new password?</span>
                    <a href="#" class="main_signup_container_signup">Back</a>
                </div>
                <p class="main_signup_container_info @if (session('status')) alert-success p-2 @endif">
                    @if (session('status'))
                        {{ session('status') }}
                    @else
                        We’ll email you to confirm you’re really you.
                        Then you’ll create a new password.
                    @endif
                </p>

                <!-- Использовать класс "error" когда поле заполнено не правильно -->

                <div class="main_signup_container_input_container @error('email') error @enderror">
                    <input type="email" name="email" value="{{old('email')}}" class="main_signup_container_input_email"
                           placeholder="E-mail">
                </div>
                @error('email')
                <p class="main_signup_error_info">
                    {{ $message }}
                </p>
            @enderror

            <!-- Класс "disabled" используется, когда поле заполнено не правильно -->
                <button type="submit" class="main_login_continue_btn">continue</button>
            </form>
        </div>
    </main>
@endsection
