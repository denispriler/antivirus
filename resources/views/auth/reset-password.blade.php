@extends('layouts.default')
@section('content')
    <main class="main_admin">
        <div class="main_signup_container">
            <div class="main_signup_container_left">
                <img src="{{asset('images/adminpanel/looginimage221.jpg')}}" alt="Two people logining in vimguard">
            </div>
            <form class="main_signup_container_right" method="POST" action="{{ route('password.update') }}">
                @csrf

                <input type="hidden" name="token" value="{{ $request->route('token') }}">
                <input type="hidden" name="email" value="{{ $request->query('email') }}">
                <div class="main_signup_container_first_line">
                    <span class="main_signup_container_title">Crate your password</span>
                    <a href="#" class="main_signup_container_signup">Back</a>
                </div>
                <p class="main_signup_container_info @if (session('status')) alert-success p-2 @endif">
                    @if (session('status'))
                        {{ session('status') }}
                    @else
                        Last step... Create your VimGuard Account password.
                        Make it a good one!
                    @endif
                </p>

                <!-- Отображение ошибки ввода пароля - класс "error_passoword" -->

                <div class="main_login_container_input_password_container @error('password') error_password @enderror">
                    <input type="password" class="main_login_container_input_password input_password" id="password"
                           name="password"
                           placeholder="Password">
                    <button type="button" class="password_show_btn">
                    </button>
                </div>
                <div class="main_login_container_input_password_container mt-4 @error('password') error_password @enderror">
                    <input type="password" class="main_login_container_input_password input_password" id="password_confirmation"
                           name="password_confirmation"
                           placeholder="Password confirmation">
                    <button type="button" class="password_show_btn">
                    </button>
                </div>
                <!-- Блок появляется когда у соседнего присутствует класс "error_passoword" -->
                @error('password')
                <div class="main_signup_error_password">
                    <ul class="main_signup_error_list">
                        <li class="main_signup_error_item">{{$message}}</li>
                    </ul>
                </div>
            @enderror

            <!-- Класс "disabled" используется, когда поле заполнено не правильно -->

                <button type="submit" class="main_login_continue_btn">continue</button>
            </form>
        </div>
    </main>
@endsection
