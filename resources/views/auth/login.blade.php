@extends('layouts.default')
@section('content')
    <div class="main_login_container">
        <div class="main_login_container_left">
            <img style="height: 100%;" src="{{asset('images/adminpanel/looginimage221.jpg')}}"
                 alt="Two people logining in vimguard">
        </div>
        <auth-form wrapper-class="main_login_container_center" method="POST" action="{{route('login')}}"
                   redirect="{{route('dashboard')}}">
            <div class="main_login_container_first_line">
                <span class="main_login_container_title">Log in</span>
                <a href="{{route('register')}}" class="main_login_container_signup">Sign up</a>
            </div>
            <!-- Отображение ошибки ввода пароля - класс "error_email" -->
            <div class="main_login_container_input_email_container form-inputs-container">
                <input type="email" name="email" class="main_login_container_input_email" placeholder="E-mail">
            </div>
            <!-- Отображение ошибки ввода пароля - класс "error_passoword" -->
            <div class="main_login_container_input_password_container form-inputs-container">
                <input type="password" name="password" class="main_login_container_input_password"
                       id="login_password"
                       placeholder="Password">
                <button type="button" class="password_show_btn">
                </button>
            </div>
            <!-- Блок появляется когда у соседнего присутствует класс "error_passoword" -->
            <div class="main_signup_error_password">
                <ul class="main_signup_error_list" id="form-errors-list">
                </ul>
            </div>
            <div class="main_login_container_last_line">
                <div class="main_login_container_checkbox_container">
                </div>
                <a href="{{route('password.request')}}" class="main_login_forgot_password">Forgot password?</a>
            </div>
            <button type="submit" class="main_login_continue_btn mb-2">continue</button>
        </auth-form>
        <div class="main_login_container_right">
            <a href="{{route('oauth.redirect', ['provider' => 'google'])}}" class="main_login_google">
                Enter with GOOGLE
            </a>
            <a href="{{route('oauth.redirect', ['provider' => 'facebook'])}}" class="main_login_facebook">
                Enter with Facebook
            </a>
        </div>
    </div>
@endsection
