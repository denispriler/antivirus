@extends('layouts.default')
@section('content')
    <main class="main">
        <div class="product_banner">
            <div class="product_banner_left">
                <img src="/images/laptop.png" alt="Laptop">
            </div>
            <div class="product_banner_right">
                <div class="product_banner_title">
                    {{ $product->name }}
                </div>
                <p class="product_banner_text">
                    {!! $product->description !!}
                </p>
                <div class="product_banner_trust">
                    <img src="/images/checkvim.png" alt="Check">
                    <p class="product_banner_trust_text">
                        No viruses and adware
                    </p>
                </div>
                <a href="{{ route('product.index') }}" class="product_banner_button">
                    Buy product
                </a>
                <p class="product_banner_also">
                    Also available for: <span>Android</span>, <span>iOS</span>, <span>Mac</span>
                </p>
            </div>
        </div>
        <div class="product_secvices">
            <div class="product_secvices_item">
                <div class="product_secvices_item_image">
                    <img src="/images/serv/adblock.png" alt="Ad blocking">
                </div>
                <div class="product_secvices_item_about">
                    <h4 class="product_secvices_item_about_title">Ad blocking</h4>
                    <p class="product_secvices_item_about_text">
                        VimGuard ad filter blocks all kinds of ads.
                        Pop-ups, video ads, banners and such — they will all go away.
                        Due to unnoticeable background filtering and cosmetic processing,
                        all you will see is clean pages with the content you came for.
                    </p>
                </div>
            </div>
            <div class="product_secvices_item">
                <div class="product_secvices_item_image">
                    <img src="/images/serv/browser.png" alt="Safe web surfing">
                </div>
                <div class="product_secvices_item_about">
                    <h4 class="product_secvices_item_about_title">Safe web surfing</h4>
                    <p class="product_secvices_item_about_text">
                        Protection from phishing and hazardous websites
                        and malvertising (malicious ads). VimGuard
                        checks every page against our database for any malicious
                        content and blocks requests from potentially dangerous ones.
                    </p>
                </div>
            </div>
            <div class="product_secvices_item">
                <div class="product_secvices_item_image">
                    <img src="/images/serv/privacyprot.png" alt="Privacy protection">
                </div>
                <div class="product_secvices_item_about">
                    <h4 class="product_secvices_item_about_title">Privacy protection</h4>
                    <p class="product_secvices_item_about_text">
                        VimGuard fights against all trackers and analytical systems
                        that spy on you. The program blocks third-party cookies,
                        can hide your IP address, and provides an abundance of other
                        features to protect your personal data.
                    </p>
                </div>
            </div>
            <div class="product_secvices_item">
                <div class="product_secvices_item_image">
                    <img src="/images/serv/paternalcontrol.png" alt="Parental control">
                </div>
                <div class="product_secvices_item_about">
                    <h4 class="product_secvices_item_about_title">Parental control</h4>
                    <p class="product_secvices_item_about_text">
                        VimGuard protects your children online. It blocks access
                        to inappropriate websites, removes obscene materials from
                        search results, and provides parents with a customizable
                        blacklist to tailor the safest web experience for their kids.
                    </p>
                </div>
            </div>
            <div class="product_secvices_item">
                <div class="product_secvices_item_image">
                    <img src="/images/serv/group.png" alt="Protect your data">
                </div>
                <div class="product_secvices_item_about">
                    <h4 class="product_secvices_item_about_title">Protect your data</h4>
                    <p class="product_secvices_item_about_text">
                        Everything on the web nowadays tries to steal your data.
                        AdGuard has a dedicated module to prevent that from happening.
                    </p>
                </div>
            </div>
            <div class="product_secvices_item">
                <div class="product_secvices_item_image">
                    <img src="/images/serv/hacker.png" alt="Disguise yourself online">
                </div>
                <div class="product_secvices_item_about">
                    <h4 class="product_secvices_item_about_title">Disguise yourself online</h4>
                    <p class="product_secvices_item_about_text">
                        Instead of simply hiding your online profile,
                        you can change it to appear as someone else and
                        browse anonymously.
                    </p>
                </div>
            </div>
        </div>
        <div class="laureats">
            <div class="laureats_left">
                <img src="/images/laureatsimg.png" alt="Laureats">
            </div>
            <div class="laureats_right">
                <div class="laureats_item">
                    <div class="laureats_item_left">
                        <img src="/images/toprated.png" alt="Top Rated best">
                    </div>
                    <div class="laureats_item_righ">
                        <p class="laureats_item_year">
                            2021
                        </p>
                        <h5 class="laureats_item_title">
                            Top Rated best
                        </h5>
                    </div>
                </div>
                <div class="laureats_item">
                    <div class="laureats_item_left">
                        <img src="/images/homeadvisor.png" alt="Top Rated product">
                    </div>
                    <div class="laureats_item_righ">
                        <p class="laureats_item_year">
                            2020
                        </p>
                        <h5 class="laureats_item_title">
                            Top Rated product
                        </h5>
                    </div>
                </div>
                <div class="laureats_item">
                    <div class="laureats_item_left">
                        <img src="/images/greatnonprof.png" alt="Top Rated product">
                    </div>
                    <div class="laureats_item_righ">
                        <p class="laureats_item_year">
                            2020
                        </p>
                        <h5 class="laureats_item_title">
                            Top Rated product
                        </h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="product_comeon">
            <p class="product_comeon_text">
                Install VimGuard for Windows and see the Internet as it was supposed to be — clean and safe.
            </p>
        </div>
        {{--<div class="products_rewiew">--}}
        {{--    <h3 class="products_rewiew_title">--}}
        {{--        Reviews on VimGuard--}}
        {{--    </h3>--}}
        {{--    <div class="products_rewiews_line owl-carousel owl-loaded owl-drag">--}}








        {{--        <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-446px, 0px, 0px); transition: all 0s ease 0s; width: 1787px;"><div class="owl-item cloned" style="width: 111.667px;"><div class="products_rewiews_line_item">--}}
        {{--                        <h5 class="products_rewiews_line_item_title">--}}
        {{--                            Great app--}}
        {{--                        </h5>--}}
        {{--                        <div class="products_rewiews_line_item_rating">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/halfstar.png" alt="halfstar">--}}
        {{--                            <img src="/images/emptystar.png" alt="emptystar">--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_line_item_tex">--}}
        {{--                            Many thanks to the developers for the application.--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_person">--}}
        {{--                            <img src="/images/rewiers/a.png" alt="Person`s photo">--}}
        {{--                            <span>David De Gea</span>--}}
        {{--                        </div>--}}
        {{--                    </div></div><div class="owl-item cloned" style="width: 111.667px;"><div class="products_rewiews_line_item">--}}
        {{--                        <h5 class="products_rewiews_line_item_title">--}}
        {{--                            Great app--}}
        {{--                        </h5>--}}
        {{--                        <div class="products_rewiews_line_item_rating">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/halfstar.png" alt="halfstar">--}}
        {{--                            <img src="/images/emptystar.png" alt="emptystar">--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_line_item_tex">--}}
        {{--                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Luctus et faucibus neque sed.                    </div>--}}
        {{--                        <div class="products_rewiews_person">--}}
        {{--                            <img src="/images/rewiers/b.png" alt="Person`s photo">--}}
        {{--                            <span>Ralph Edwards</span>--}}
        {{--                        </div>--}}
        {{--                    </div></div><div class="owl-item cloned" style="width: 111.667px;"><div class="products_rewiews_line_item">--}}
        {{--                        <h5 class="products_rewiews_line_item_title">--}}
        {{--                            Great app--}}
        {{--                        </h5>--}}
        {{--                        <div class="products_rewiews_line_item_rating">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/halfstar.png" alt="halfstar">--}}
        {{--                            <img src="/images/emptystar.png" alt="emptystar">--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_line_item_tex">--}}
        {{--                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Urna tristique duis augue nunc penatibus varius ultrices fringilla eu. At ultricies quam egestas porta praesent. Vulputate consequat lacinia justo, pellentesque.--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_person">--}}
        {{--                            <img src="/images/rewiers/c.png" alt="Person`s photo">--}}
        {{--                            <span>Jacob Jones</span>--}}
        {{--                        </div>--}}
        {{--                    </div></div><div class="owl-item cloned" style="width: 111.667px;"><div class="products_rewiews_line_item">--}}
        {{--                        <h5 class="products_rewiews_line_item_title">--}}
        {{--                            Great app--}}
        {{--                        </h5>--}}
        {{--                        <div class="products_rewiews_line_item_rating">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/halfstar.png" alt="halfstar">--}}
        {{--                            <img src="/images/emptystar.png" alt="emptystar">--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_line_item_tex">--}}
        {{--                            Many thanks to the developers for the application.--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_person">--}}
        {{--                            <img src="/images/rewiers/d.png" alt="Person`s photo">--}}
        {{--                            <span>Annette Black</span>--}}
        {{--                        </div>--}}
        {{--                    </div></div><div class="owl-item active" style="width: 111.667px;"><div class="products_rewiews_line_item">--}}
        {{--                        <h5 class="products_rewiews_line_item_title">--}}
        {{--                            Great app--}}
        {{--                        </h5>--}}
        {{--                        <div class="products_rewiews_line_item_rating">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/halfstar.png" alt="halfstar">--}}
        {{--                            <img src="/images/emptystar.png" alt="emptystar">--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_line_item_tex">--}}
        {{--                            Many thanks to the developers for the application.--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_person">--}}
        {{--                            <img src="/images/rewiers/a.png" alt="Person`s photo">--}}
        {{--                            <span>David De Gea</span>--}}
        {{--                        </div>--}}
        {{--                    </div></div><div class="owl-item active" style="width: 111.667px;"><div class="products_rewiews_line_item">--}}
        {{--                        <h5 class="products_rewiews_line_item_title">--}}
        {{--                            Great app--}}
        {{--                        </h5>--}}
        {{--                        <div class="products_rewiews_line_item_rating">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/halfstar.png" alt="halfstar">--}}
        {{--                            <img src="/images/emptystar.png" alt="emptystar">--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_line_item_tex">--}}
        {{--                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Luctus et faucibus neque sed.                    </div>--}}
        {{--                        <div class="products_rewiews_person">--}}
        {{--                            <img src="/images/rewiers/b.png" alt="Person`s photo">--}}
        {{--                            <span>Ralph Edwards</span>--}}
        {{--                        </div>--}}
        {{--                    </div></div><div class="owl-item active" style="width: 111.667px;"><div class="products_rewiews_line_item">--}}
        {{--                        <h5 class="products_rewiews_line_item_title">--}}
        {{--                            Great app--}}
        {{--                        </h5>--}}
        {{--                        <div class="products_rewiews_line_item_rating">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/halfstar.png" alt="halfstar">--}}
        {{--                            <img src="/images/emptystar.png" alt="emptystar">--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_line_item_tex">--}}
        {{--                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Urna tristique duis augue nunc penatibus varius ultrices fringilla eu. At ultricies quam egestas porta praesent. Vulputate consequat lacinia justo, pellentesque.--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_person">--}}
        {{--                            <img src="/images/rewiers/c.png" alt="Person`s photo">--}}
        {{--                            <span>Jacob Jones</span>--}}
        {{--                        </div>--}}
        {{--                    </div></div><div class="owl-item" style="width: 111.667px;"><div class="products_rewiews_line_item">--}}
        {{--                        <h5 class="products_rewiews_line_item_title">--}}
        {{--                            Great app--}}
        {{--                        </h5>--}}
        {{--                        <div class="products_rewiews_line_item_rating">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/halfstar.png" alt="halfstar">--}}
        {{--                            <img src="/images/emptystar.png" alt="emptystar">--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_line_item_tex">--}}
        {{--                            Many thanks to the developers for the application.--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_person">--}}
        {{--                            <img src="/images/rewiers/d.png" alt="Person`s photo">--}}
        {{--                            <span>Annette Black</span>--}}
        {{--                        </div>--}}
        {{--                    </div></div><div class="owl-item" style="width: 111.667px;"><div class="products_rewiews_line_item">--}}
        {{--                        <h5 class="products_rewiews_line_item_title">--}}
        {{--                            Great app--}}
        {{--                        </h5>--}}
        {{--                        <div class="products_rewiews_line_item_rating">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/halfstar.png" alt="halfstar">--}}
        {{--                            <img src="/images/emptystar.png" alt="emptystar">--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_line_item_tex">--}}
        {{--                            Many thanks to the developers for the application.--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_person">--}}
        {{--                            <img src="/images/rewiers/a.png" alt="Person`s photo">--}}
        {{--                            <span>David De Gea</span>--}}
        {{--                        </div>--}}
        {{--                    </div></div><div class="owl-item" style="width: 111.667px;"><div class="products_rewiews_line_item">--}}
        {{--                        <h5 class="products_rewiews_line_item_title">--}}
        {{--                            Great app--}}
        {{--                        </h5>--}}
        {{--                        <div class="products_rewiews_line_item_rating">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/halfstar.png" alt="halfstar">--}}
        {{--                            <img src="/images/emptystar.png" alt="emptystar">--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_line_item_tex">--}}
        {{--                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Luctus et faucibus neque sed.                    </div>--}}
        {{--                        <div class="products_rewiews_person">--}}
        {{--                            <img src="/images/rewiers/b.png" alt="Person`s photo">--}}
        {{--                            <span>Ralph Edwards</span>--}}
        {{--                        </div>--}}
        {{--                    </div></div><div class="owl-item" style="width: 111.667px;"><div class="products_rewiews_line_item">--}}
        {{--                        <h5 class="products_rewiews_line_item_title">--}}
        {{--                            Great app--}}
        {{--                        </h5>--}}
        {{--                        <div class="products_rewiews_line_item_rating">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/halfstar.png" alt="halfstar">--}}
        {{--                            <img src="/images/emptystar.png" alt="emptystar">--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_line_item_tex">--}}
        {{--                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Urna tristique duis augue nunc penatibus varius ultrices fringilla eu. At ultricies quam egestas porta praesent. Vulputate consequat lacinia justo, pellentesque.--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_person">--}}
        {{--                            <img src="/images/rewiers/c.png" alt="Person`s photo">--}}
        {{--                            <span>Jacob Jones</span>--}}
        {{--                        </div>--}}
        {{--                    </div></div><div class="owl-item" style="width: 111.667px;"><div class="products_rewiews_line_item">--}}
        {{--                        <h5 class="products_rewiews_line_item_title">--}}
        {{--                            Great app--}}
        {{--                        </h5>--}}
        {{--                        <div class="products_rewiews_line_item_rating">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/halfstar.png" alt="halfstar">--}}
        {{--                            <img src="/images/emptystar.png" alt="emptystar">--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_line_item_tex">--}}
        {{--                            Many thanks to the developers for the application.--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_person">--}}
        {{--                            <img src="/images/rewiers/d.png" alt="Person`s photo">--}}
        {{--                            <span>Annette Black</span>--}}
        {{--                        </div>--}}
        {{--                    </div></div><div class="owl-item cloned" style="width: 111.667px;"><div class="products_rewiews_line_item">--}}
        {{--                        <h5 class="products_rewiews_line_item_title">--}}
        {{--                            Great app--}}
        {{--                        </h5>--}}
        {{--                        <div class="products_rewiews_line_item_rating">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/halfstar.png" alt="halfstar">--}}
        {{--                            <img src="/images/emptystar.png" alt="emptystar">--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_line_item_tex">--}}
        {{--                            Many thanks to the developers for the application.--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_person">--}}
        {{--                            <img src="/images/rewiers/a.png" alt="Person`s photo">--}}
        {{--                            <span>David De Gea</span>--}}
        {{--                        </div>--}}
        {{--                    </div></div><div class="owl-item cloned" style="width: 111.667px;"><div class="products_rewiews_line_item">--}}
        {{--                        <h5 class="products_rewiews_line_item_title">--}}
        {{--                            Great app--}}
        {{--                        </h5>--}}
        {{--                        <div class="products_rewiews_line_item_rating">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/halfstar.png" alt="halfstar">--}}
        {{--                            <img src="/images/emptystar.png" alt="emptystar">--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_line_item_tex">--}}
        {{--                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Luctus et faucibus neque sed.                    </div>--}}
        {{--                        <div class="products_rewiews_person">--}}
        {{--                            <img src="/images/rewiers/b.png" alt="Person`s photo">--}}
        {{--                            <span>Ralph Edwards</span>--}}
        {{--                        </div>--}}
        {{--                    </div></div><div class="owl-item cloned" style="width: 111.667px;"><div class="products_rewiews_line_item">--}}
        {{--                        <h5 class="products_rewiews_line_item_title">--}}
        {{--                            Great app--}}
        {{--                        </h5>--}}
        {{--                        <div class="products_rewiews_line_item_rating">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/halfstar.png" alt="halfstar">--}}
        {{--                            <img src="/images/emptystar.png" alt="emptystar">--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_line_item_tex">--}}
        {{--                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Urna tristique duis augue nunc penatibus varius ultrices fringilla eu. At ultricies quam egestas porta praesent. Vulputate consequat lacinia justo, pellentesque.--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_person">--}}
        {{--                            <img src="/images/rewiers/c.png" alt="Person`s photo">--}}
        {{--                            <span>Jacob Jones</span>--}}
        {{--                        </div>--}}
        {{--                    </div></div><div class="owl-item cloned" style="width: 111.667px;"><div class="products_rewiews_line_item">--}}
        {{--                        <h5 class="products_rewiews_line_item_title">--}}
        {{--                            Great app--}}
        {{--                        </h5>--}}
        {{--                        <div class="products_rewiews_line_item_rating">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/fullstar.png" alt="fullstar">--}}
        {{--                            <img src="/images/halfstar.png" alt="halfstar">--}}
        {{--                            <img src="/images/emptystar.png" alt="emptystar">--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_line_item_tex">--}}
        {{--                            Many thanks to the developers for the application.--}}
        {{--                        </div>--}}
        {{--                        <div class="products_rewiews_person">--}}
        {{--                            <img src="/images/rewiers/d.png" alt="Person`s photo">--}}
        {{--                            <span>Annette Black</span>--}}
        {{--                        </div>--}}
        {{--                    </div></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev"><span aria-label="Previous">‹</span></button><button type="button" role="presentation" class="owl-next"><span aria-label="Next">›</span></button></div><div class="owl-dots"><button role="button" class="owl-dot active"><span></span></button><button role="button" class="owl-dot"><span></span></button><button role="button" class="owl-dot"><span></span></button></div></div>--}}
        {{--    <div class="products_rewiew_button">--}}
        {{--        <button type="button">--}}
        {{--            Write a review--}}
        {{--        </button>--}}
        {{--    </div>--}}
        {{--</div>--}}
        <div class="product_faq" style="margin-top: 3%;">
            <h3 class="product_faq_title">
                FAQ
            </h3>
            <div class="product_faq_container">
                <div class="product_faq_item">
                    <h5 class="product_faq_question">
                        <span>How do I install VimGuard?</span>
                    </h5>
                    <p class="product_faq_answer">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Earum, ratione cupiditate eveniet similique quibusdam doloribus
                        odio quam corporis molestiae ex! Iure, sapiente aliquam!
                        Accusantium, exercitationem soluta hic tempora pariatur eligendi.
                    </p>
                </div>
                <div class="product_faq_item">
                    <h5 class="product_faq_question">
                        <span>Why is VimGuard better than other apps?</span>
                    </h5>
                    <p class="product_faq_answer">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Earum, ratione cupiditate eveniet similique quibusdam doloribus
                        odio quam corporis molestiae ex! Iure, sapiente aliquam!
                        Accusantium, exercitationem soluta hic tempora pariatur eligendi.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Earum, ratione cupiditate eveniet similique quibusdam doloribus
                        odio quam corporis molestiae ex! Iure, sapiente aliquam!
                        Accusantium, exercitationem soluta hic tempora pariatur eligendi.
                    </p>
                </div>
                <div class="product_faq_item">
                    <h5 class="product_faq_question">
                        <span>What are the main features of VimGuard for Windows?</span>
                    </h5>
                    <p class="product_faq_answer">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Earum, ratione cupiditate eveniet similique quibusdam doloribus
                        odio quam corporis molestiae ex! Iure, sapiente aliquam!
                        Accusantium, exercitationem soluta hic tempora pariatur eligendi.
                    </p>
                </div>
                <div class="product_faq_item">
                    <h5 class="product_faq_question">
                        <span>What if I have questions? Do you have a support service?</span>
                    </h5>
                    <p class="product_faq_answer">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Earum, ratione cupiditate eveniet similique quibusdam doloribus
                        odio quam corporis molestiae ex! Iure, sapiente aliquam!
                        Accusantium, exercitationem soluta hic tempora pariatur eligendi.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Earum, ratione cupiditate eveniet similique quibusdam doloribus
                        odio quam corporis molestiae ex! Iure, sapiente aliquam!
                        Accusantium, exercitationem soluta hic tempora pariatur eligendi.
                    </p>
                </div>
            </div>
        </div>
        @include('components.module.blog')
    </main>
@endsection
