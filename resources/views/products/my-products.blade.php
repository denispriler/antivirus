@extends('layouts.admin')
@section('content')
    <main class="main_admin">
        <div class="main_page_first_line">
            <h2 class="main_page_first_line_title">
                My products
            </h2>
            <p class="main_page_first_line_text">
                Only paid subscriptions will appear here. To see mobile subscriptions, just log into your mobile app.
            </p>
        </div>
        @if($productKeys->count() === 0)
            <div class="main_page_product_empty">
                <img src="{{asset('images/adminpanel/computer4110.png')}}" alt="Computer and server"
                     class="main_page_product_empty_img">
                <p class="main_page_product_empty_text">No products here, yet.</p>
                <a href="{{route('product.index')}}" type="button" class="main_page_product_empty_btn">get now</a>
            </div>
        @else
            <div class="main_page_product_list">
                <div class="main_page_product_list_header">
                    <div class="main_page_product_list_header_item product">Product</div>
                    <div class="main_page_product_list_header_item devices">Devices</div>
                    <div class="main_page_product_list_header_item validity">Validity</div>
                    <div class="main_page_product_list_header_item price">Price</div>
                </div>
                <div class="main_page_product_list_main">
                    @foreach($productKeys as $productKey)
                        <div class="main_page_product_list_container">
                            <div class="main_page_product_list_container_item product">
                                <input type="checkbox" class="product_list_item_checkbox"
                                       id="product-{{$productKey->id}}">
                                <a
                                    data-toggle="collapse" href="#key-collapse-{{$productKey->id}}" role="button"
                                    aria-expanded="false" aria-controls="collapseExample"
                                    class="product_list_item_name d-flex flex-row">
                                    <span>{{$productKey->product->name}}</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                         fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                         stroke-linejoin="round" class="feather feather-chevron-down mt-2">
                                        <polyline points="6 9 12 15 18 9"></polyline>
                                    </svg>
                                </a>
                            </div>
                            <div class="main_page_product_list_container_item devices"
                                 style="white-space:nowrap; text-overflow: ellipsis;">
                                {{implode(', ', $productKey->product->platforms->pluck('name')->toArray())}}
                            </div>
                            <div class="main_page_product_list_container_item validity">
                                {{$productKey->created_at->format('d.m.Y')}}
                                - {{$productKey->created_at->addYear()->format('d.m.Y')}}
                            </div>
                        </div>
                        <div class="key_container collapse" id="key-collapse-{{$productKey->id}}">
                            <span
                                class="key_title">Your license key for {{$productKey->product->name}}: </span>{{$productKey->key}}
                        </div>
                    @endforeach
                </div>
                <div class="main_page_product_list_footer">
                    <button type="button" class="main_page_product_list_footer_btn disabled" disabled>download</button>
                </div>
            </div>
        @endif
    </main>
@endsection
