@extends('layouts.default')
@section('content')
    <main class="article_main">
        <div class="article_breadcrumbs">
            <ul class="article_breadcrumbs_items">
                <li class="article_breadcrumbs_item">
                    <a href="{{ route('article.index') }}">Vim Blog</a>
                </li>
                <li class="article_breadcrumbs_item">
                    <span>{{ $article->title }}</span>
                </li>
            </ul>
        </div>
        <div class="article_thumb">
            <h2 class="article_thumb_title">
                {{ $article->title }}
            </h2>
            <p class="article_thumb_text">
                {!! $article->short_text !!}
            </p>
            <a href="{{ route('article.index') }}" class="article_thumb_buy">
                Buy Vimguard
            </a>
        </div>
        <div class="article_main_image">
            <img src="{{ $article->image }}" alt="Main image">
        </div>
        <div class="article_main_section">
            <div class="article_main_section_left">
                {!! $article->text !!}
                <hr>
                <div class="article_main_section_author">
                    <div class="article_main_section_author_left">
                        <p>by <b>{{ $article->author }}</b> on {{ $article->created_at }}</p>
                    </div>
                    <div class="article_main_section_author_right">
                        <a href="#" target="_blank" rel="noopener noreferrer">
                            <img src="/images/entypo-social_facebook.png" alt="a">
                        </a>
                        <a href="#" target="_blank" rel="noopener noreferrer">
                            <img src="/images/fa-brands_twitter-square.png" alt="a">
                        </a>
                        <a href="#" target="_blank" rel="noopener noreferrer">
                            <img src="/images/akar-icons_linkedin-fill.png" alt="a">
                        </a>
                        <a href="#" target="_blank" rel="noopener noreferrer">
                            <img src="/images/fa-solid_share-alt-square.png" alt="a">
                        </a>
                    </div>
                    <br>
                    <br>
                    <br>
                </div>
            </div>
        </div>
    </main>
@endsection
