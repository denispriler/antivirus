@extends('layouts.default')
@section('content')
<main class="main_blog">
    <div class="main_blog_banner">
        <h4 class="main_blog_banner_title">
            Vim blog
        </h4>
        <p class="main_blog_banner_text">
            VimGuard prevents 1.5 billion cyber attacks per month. And in our free time, we love teaching the world
            about digital security, online privacy, and device performance. See all of our guides, tips, and advice
            right here.
        </p>
    </div>
    <div class="blog_articles">
        <div class="blog_articles_items">
            @foreach($articles as $article)
                <div class="blog_articles_item vim_news">
                    <a href="{{ route('article.show', ['article' => $article->slug]) }}">
                        <img src="{{ $article->image }}" alt="a" class="blog_articles_item_image">
                        <h4 class="blog_articles_item_title">
                            {{ $article->name }}
                        </h4>
                        <p class="blog_articles_item_text">
                            {!! $article->short_text !!}
                        </p>
                        <div class="blog_articles_item_infoline">
                            <span class="blog_articles_item_date">{{ $article->created_at }}</span>
                            {{--<span class="blog_articles_item_views">1024 viewes</span>--}}
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</main>
@endsection
