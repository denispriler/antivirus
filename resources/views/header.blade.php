<header data-cmp-name="cmp-header"
        class="js-navigation-bootstrap header relative default web js-navigation-oo-19798 navigation-oo-19798">
    <div class="header-wrap">
        <div class="vim-logo">
            <a target="_parent" title="Home" href="{{ route('home') }}" data-role="Nav:TopLink" data-cta="logo">
                <img width="113" height="36" alt="VimGuard" src="{{asset('images/logo.png')}}">
            </a>
        </div>
        <button class="reader-only" href="#main-content">Skip to main content</button>
        <button class="js-toggle-menu hidden-desktop toggle-menu  bi-nav-menu" data-nav-mobile-toggle="open"
                aria-label="Open or close navigation menu"><span>>Close</span></button>
        <nav>
            <div class="first-menu">
            </div>
            <div class="second-menu for-home">
                <ul class="side" role="menubar">
                    <li role="menuitem">
                        <a href="{{ route('home') }}" data-role="Nav:MenuItem" data-cta="home" role="menuitem"><span
                                class="subcategory home"><img class="mobile"
                                                              src="{{asset('images/local/ico-mobile-home.svg')}}"
                                                              alt="">Home</span></a>
                    </li>

                    @foreach($categories as $category)
                        <li data-second-menu="{{ Str::snake($category->name, '-') }}" role="none">
              <span class="subcategory security arrow" tabindex="0" role="menuitem" aria-expanded="false"
                    aria-controls="security">{{ $category->name }}</span>
                            <div class="third-menu security" role="menu" id="navigation-security">
                                <ul class="block-products">
                                    @foreach($category->products as $product)
                                        <li>
                                            <a href="{{route('product.show', ['product' => $product->slug])}}"
                                               class="content-windows"
                                               data-role="Nav:MenuItem"
                                               data-cta="homeSecurity">
                                          <span class="name mobile-link">
                                            <div data-cmp-name="cmp-product-icon" class="product-icon box small">
                                              <img alt
                                                   src="/{{ $product->logo }}"
                                                   title="{{ $product->name }}">
                                            </div><img class="logo"
                                                       src="{{ $product->logo }}" alt=""><span
                                                  class="product-name">{{ $product->name }}</span>
                                          </span>

                                                <span
                                                    class="os @foreach ($product->platforms as $platform) {{ $platform->id }} @endforeach">
                                                                    <img src="{{asset('images/local/win.svg')}}"
                                                                         class="img-win"
                                                                         alt="Available for PC">
                                                                    <img src="{{asset('images/local/mac.svg')}}"
                                                                         class="img-mac"
                                                                         alt="Available for Mac">
                                                                    <img src="{{asset('images/local/android.svg')}}"
                                                                         class="img-android"
                                                                         alt="Available for Android">
                                                                    <img src="{{asset('images/local/ios.svg')}}"
                                                                         class="img-ios"
                                                                         alt="Available for iOS">
                                                                    <img src="{{asset('images/local/win-smb.svg')}}"
                                                                         class="img-win-smb" alt="Available for PC">
                                                                    <img src="{{asset('images/local/mac-smb.svg')}}"
                                                                         class="img-mac-smb" alt="Available for Mac">
                                                                    <img src="{{asset('images/local/servers-smb.svg')}}"
                                                                         class="img-servers-smb" alt="">
                                                                    <img src="{{asset('images/local/linux-smb.svg')}}"
                                                                         class="img-linux-smb" alt="">
                                                                    <img src="{{asset('images/local/android-smb.svg')}}"
                                                                         class="img-android-smb"
                                                                         alt="Available for Android">
                                                                    <img src="{{asset('images/local/ios-smb.svg')}}"
                                                                         class="img-ios-smb" alt="Available for iOS">
                                                                </span>
                                                <span class="description">{{ $product->details }}</span>
                                            </a>
                                            <a href="/product/{{ $product->slug }}" class="content-mac"
                                               data-role="Nav:MenuItem"
                                               data-cta="homeSecurity">
                                          <span class="name mobile-link">
                                            <div data-cmp-name="cmp-product-icon" class="product-icon box small">
                                              <img alt=""
                                                   src="/{{ $product->logo }}"
                                                   title="{{ $product->name }}">
                                            </div><img class="logo"
                                                       src="/{{ $product->logo }}" alt=""><span
                                                  class="product-name">{{ $product->name }}</span>
                                          </span>
                                                <span
                                                    class="os @foreach ($product->platforms as $platform) {{ $platform->id }} @endforeach">
                                            <img src="{{asset('images/local/win.svg')}}" class="img-win"
                                                 alt="Available for PC">
                                            <img src="{{asset('images/local/mac.svg')}}" class="img-mac"
                                                 alt="Available for Mac">
                                            <img src="{{asset('images/local/android.svg')}}"
                                                 class="img-android" alt="Available for Android">
                                            <img src="{{asset('images/local/ios.svg')}}" class="img-ios"
                                                 alt="Available for iOS">
                                            <img src="{{asset('images/local/win-smb.svg')}}"
                                                 class="img-win-smb" alt="Available for PC">
                                            <img src="{{asset('images/local/mac-smb.svg')}}"
                                                 class="img-mac-smb" alt="Available for Mac">
                                            <img src="{{asset('images/local/servers-smb.svg')}}"
                                                 class="img-servers-smb" alt="">
                                            <img src="{{asset('images/local/linux-smb.svg')}}"
                                                 class="img-linux-smb" alt="">
                                            <img src="{{asset('images/local/android-smb.svg')}}"
                                                 class="img-android-smb" alt="Available for Android">
                                            <img src="{{asset('images/local/ios-smb.svg')}}"
                                                 class="img-ios-smb" alt="Available for iOS">
                                          </span>
                                                <span class="description">{{ $product->details }}</span>
                                            </a>
                                            <a href="/product/{{ $product->slug }}" class="content-android"
                                               data-role="Nav:MenuItem"
                                               data-cta="homeSecurity">
                                          <span class="name mobile-link">
                                            <div data-cmp-name="cmp-product-icon" class="product-icon box small">
                                              <img alt=""
                                                   src="{{ asset($product->logo) }}"
                                                   title="{{ $product->name }}">
                                            </div><img class="logo"
                                                       src="{{ asset($product->logo) }}" alt=""><span
                                                  class="product-name">{{ $product->name }}</span>
                                          </span>
                                                <span
                                                    class="os @foreach ($product->platforms as $platform) {{ $platform->id }} @endforeach">
                                            <img src="{{asset('images/local/win.svg')}}" class="img-win"
                                                 alt="Available for PC">
                                            <img src="{{asset('images/local/mac.svg')}}" class="img-mac"
                                                 alt="Available for Mac">
                                            <img src="{{asset('images/local/android.svg')}}"
                                                 class="img-android" alt="Available for Android">
                                            <img src="{{asset('images/local/ios.svg')}}" class="img-ios"
                                                 alt="Available for iOS">
                                            <img src="{{asset('images/local/win-smb.svg')}}"
                                                 class="img-win-smb" alt="Available for PC">
                                            <img src="{{asset('images/local/mac-smb.svg')}}"
                                                 class="img-mac-smb" alt="Available for Mac">
                                            <img src="{{asset('images/local/servers-smb.svg')}}"
                                                 class="img-servers-smb" alt="">
                                            <img src="{{asset('images/local/linux-smb.svg')}}"
                                                 class="img-linux-smb" alt="">
                                            <img src="{{asset('images/local/android-smb.svg')}}"
                                                 class="img-android-smb" alt="Available for Android">
                                            <img src="{{asset('images/local/ios-smb.svg')}}"
                                                 class="img-ios-smb" alt="Available for iOS">
                                          </span>
                                                <span class="description">{{ $product->details }}</span>
                                            </a>
                                            <a href="/product/{{ $product->slug }}" class="content-ios"
                                               data-role="Nav:MenuItem"
                                               data-cta="homeSecurity">
                                          <span class="name mobile-link">
                                            <div data-cmp-name="cmp-product-icon" class="product-icon box small">
                                              <img alt
                                                   src="{{ asset($product->logo) }}"
                                                   title="{{ $product->name }}">
                                            </div><img class="logo"
                                                       src="{{ asset($product->logo) }}" alt=""><span
                                                  class="product-name">{{ $product->name }}</span>
                                          </span>
                                                <span
                                                    class="os @foreach ($product->platforms as $platform) {{ $platform->id }} @endforeach">
                                            <img src="{{asset('images/local/win.svg')}}" class="img-win"
                                                 alt="Available for PC">
                                            <img src="{{asset('images/local/mac.svg')}}" class="img-mac"
                                                 alt="Available for Mac">
                                            <img src="{{asset('images/local/android.svg')}}"
                                                 class="img-android" alt="Available for Android">
                                            <img src="{{asset('images/local/ios.svg')}}" class="img-ios"
                                                 alt="Available for iOS">
                                            <img src="{{asset('images/local/win-smb.svg')}}"
                                                 class="img-win-smb" alt="Available for PC">
                                            <img src="{{asset('images/local/mac-smb.svg')}}"
                                                 class="img-mac-smb" alt="Available for Mac">
                                            <img src="{{asset('images/local/servers-smb.svg')}}"
                                                 class="img-servers-smb" alt="">
                                            <img src="{{asset('images/local/linux-smb.svg')}}"
                                                 class="img-linux-smb" alt="">
                                            <img src="{{asset('images/local/android-smb.svg')}}"
                                                 class="img-android-smb" alt="Available for Android">
                                            <img src="{{asset('images/local/ios-smb.svg')}}"
                                                 class="img-ios-smb" alt="Available for iOS">
                                                                </span>
                                                <span class="description">{{ $product->details }}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                                <p class="hint hidden-mobile">{!! $category->description !!}</p>
                            </div>
                        </li>
                    @endforeach
                    <li role="none">
                        <a class="desktop" href="{{ route('product.index') }}" data-role="Nav:MenuItem"
                           data-cta="homeShop"
                           role="menuitem">
                            <span class="subcategory shop">
                                <img class="hidden-mobile" src="{{asset('images/local/cart.svg')}}" alt="">Buy product
                            </span>
                        </a>
                    </li>
                    <li role="menuitem">
                        <a href="{{ route('article.index') }}" data-role="Nav:MenuItem" data-cta="homeSupport"
                           role="menuitem"><span class="subcategory support"><img class="hidden-mobile"
                                                                                  src="{{asset('images/local/support.svg')}}"
                                                                                  alt=""><img class="mobile"
                                                                                              src="{{asset('images/local/ico-mobile-support.svg')}}"
                                                                                              alt="">Blogs</span></a>
                    </li>

                    <li role="menuitem">
                        <a href="{{route('dashboard')}}" data-role="Nav:MenuItem" data-cta="homeAccount"
                           role="menuitem">
                            <span class="subcategory account">
                                <img class="hidden-mobile" src="{{asset('images/local/account.svg')}}" alt>
                                <img class="mobile" src="{{asset('images/local/ico-mobile-account.svg')}}" alt="">
                                Account
                            </span>
                        </a>
                    </li>
                </ul>
                <ul class="side mobile-links-top" role="menubar">
                    <li class="mobile" role="menuitem">
                        <a href="/en-us/index" data-role="Nav:MenuItem" data-cta="home" role="menuitem"><span
                                class="subcategory home"><img class="mobile"
                                                              src="{{asset('images/local/ico-mobile-home.svg')}}"
                                                              alt="">Home</span></a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>
<section class="region-selector" role="dialog" aria-label="Region selector">
    <button class="region-selector-close">&nbsp;</button>
    <h1 class="reader-only">List of available regions</h1>
    <div class="custom-regions" id="lang-selector-navigation">
        <div class="main-regions">
            <h5 class="reader-only">Main regions</h5>
            <ul>
                <li class="region en-ww"><a lang="en" class="with-flag" href="/index">Worldwide (English)</a></li>
                <li class="region en-eu"><a lang="en" class="with-flag" href="/en-eu/index">Europe (English)</a></li>
                <li class="region es-ww"><a lang="es" class="with-flag" href="/es-ww/index">América Latina (español)</a>
                </li>
            </ul>
        </div>
        <div class="area regions-0">
            <h5 class="js-regions-toggle subcategory arrow">AMERICAS</h5>
            <div class="js-regions">
                <ul>
                    <li class="region es-ar"><a lang="es" class="with-flag" href="/es-ar/index">Argentina</a></li>
                    <li class="region pt-br"><a lang="pt" class="with-flag" href="/pt-br/index">Brasil</a></li>
                    <li class="region en-ca"><a lang="en" class="with-flag" href="/en-ca/index">Canada (English)</a>
                    </li>
                    <li class="region fr-ca"><a lang="fr" class="with-flag" href="/fr-ca/index">Canada (français)</a>
                    </li>
                    <li class="region es-cl"><a lang="es" class="with-flag" href="/es-cl/index">Chile</a></li>
                    <li class="region es-co"><a lang="es" class="with-flag" href="/es-co/index">Colombia</a></li>
                    <li class="region es-us"><a lang="es" class="with-flag" href="/es-us/index">EE.UU. (español)</a>
                    </li>
                    <li class="region es-mx"><a lang="es" class="with-flag" href="/es-mx/index">México</a></li>
                    <li class="region en-us"><a class="with-flag" href="/en-us/index">USA (English)</a></li>
                </ul>
                <ul class="other">
                    <li class="region es-ww"><a lang="es" class="with-flag" href="/es-ww/index">América Latina
                            (español)</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="area regions-1">
            <h5 class="js-regions-toggle subcategory arrow">EUROPE, MIDDLE EAST &amp; AFRICA</h5>
            <div class="js-regions">
                <ul>
                    <li class="region nl-be"><a lang="nl" class="with-flag" href="/nl-be/index">België (Nederlands)</a>
                    </li>
                    <li class="region fr-be"><a lang="fr" class="with-flag" href="/fr-be/index">Belgique (français)</a>
                    </li>
                    <li class="region cs-cz"><a lang="cs" class="with-flag" href="/cs-cz/index">Česká republika</a></li>
                    <li class="region da-dk"><a lang="da" class="with-flag" href="/da-dk/index">Danmark</a></li>
                    <li class="region de-de"><a lang="de" class="with-flag" href="/de-de/index">Deutschland </a></li>
                    <li class="region es-es"><a lang="es" class="with-flag" href="/es-es/index">España</a></li>
                    <li class="region fr-fr"><a lang="fr" class="with-flag" href="/fr-fr/index">France</a></li>
                    <li class="region it-it"><a lang="it" class="with-flag" href="/it-it/index">Italia </a></li>
                    <li class="region hu-hu"><a lang="hu" class="with-flag" href="/hu-hu/index">Magyarország</a></li>
                    <li class="region nl-nl"><a lang="nl" class="with-flag" href="/nl-nl/index">Nederland</a></li>
                    <li class="region no-no"><a lang="no" class="with-flag" href="/no-no/index">Norge</a></li>
                    <li class="region pl-pl"><a lang="pl" class="with-flag" href="/pl-pl/index">Polska</a></li>
                    <li class="region pt-pt"><a lang="pt" class="with-flag" href="/pt-pt/index">Portugal</a></li>
                    <li class="region de-ch"><a lang="de" class="with-flag" href="/de-ch/index">Schweiz (Deutsch)</a>
                    </li>
                    <li class="region cs-sk"><a lang="cs" class="with-flag" href="/cs-sk/index">Slovensko (česky)</a>
                    </li>
                    <li class="region en-za"><a lang="en" class="with-flag" href="/en-za/index">South Africa</a></li>
                </ul>
                <ul>
                    <li class="region fr-ch"><a lang="fr" class="with-flag" href="/fr-ch/index">Suisse (français)</a>
                    </li>
                    <li class="region fi-fi"><a lang="fi" class="with-flag" href="/fi-fi/index">Suomi</a></li>
                    <li class="region sv-se"><a lang="sv" class="with-flag" href="/sv-se/index">Sverige</a></li>
                    <li class="region tr-tr"><a lang="tr" class="with-flag" href="/tr-tr/index">Türkiye</a></li>
                    <li class="region en-ae"><a lang="en" class="with-flag" href="/en-ae/index">United Arab Emirates</a>
                    </li>
                    <li class="region en-gb"><a lang="en" class="with-flag" href="/en-gb/index">United Kingdom</a></li>
                    <li class="region el-gr"><a lang="el" class="with-flag" href="/el-gr/index">Ελλάδα</a></li>
                    <li class="region he-il"><a lang="he" class="with-flag" href="/he-il/index">ישראל</a></li>
                    <li class="region ru-kz"><a lang="ru" class="with-flag" href="/ru-kz/index">Казахстан</a></li>
                    <li class="region ro-ro"><a lang="ro" class="with-flag" href="/ro-ro/index">România</a></li>
                    <li class="region ru-ru"><a lang="ru" class="with-flag" href="/index">Россия</a></li>
                    <li class="region uk-ua"><a lang="uk" class="with-flag" href="/index">Україна (українська)</a>
                    </li>
                    <li class="region ru-ua"><a lang="ru" class="with-flag" href="/ru-ua/index">Украина
                            (русский)</a></li>
                    <li class="region ar-sa"><a lang="ar" class="with-flag" href="/ar-sa/index">المملكة العربية
                            السعودية</a>
                    </li>
                    <li class="region ar-ww"><a lang="ar" class="with-flag" href="/ar-ww/index">الدول العربية</a></li>
                </ul>
                <ul class="other">
                    <li class="region en-eu"><a lang="en" class="with-flag" href="/en-eu/index">Europe (English)</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="area regions-2">
            <h5 class="js-regions-toggle subcategory arrow">ASIA &amp; PACIFIC</h5>
            <div class="js-regions">
                <ul>
                    <li class="region en-au"><a lang="en" class="with-flag" href="/en-au/index">Australia</a></li>
                    <li class="region en-in"><a lang="en" class="with-flag" href="/en-in/index">India</a></li>
                    <li class="region hi-in"><a lang="hi" class="with-flag" href="/hi-in/index">इंडिया (हिंदी)</a></li>
                    <li class="region en-id"><a lang="en" class="with-flag" href="/en-id/index">Indonesia (English)</a>
                    </li>
                    <li class="region id-id"><a lang="id" class="with-flag" href="/id-id/index">Indonesia (Bahasa
                            Indonesia)</a>
                    </li>
                    <li class="region en-my"><a lang="en" class="with-flag" href="/en-my/index">Malaysia (English)</a>
                    </li>
                    <li class="region ms-my"><a lang="ms" class="with-flag" href="/ms-my/index">Malaysia (Bahasa
                            Melayu)</a>
                    </li>
                    <li class="region en-nz"><a lang="en" class="with-flag" href="/en-nz/index">New Zealand</a></li>
                    <li class="region en-ph"><a lang="en" class="with-flag" href="/en-ph/index">Philippines
                            (English)</a></li>
                    <li class="region tl-ph"><a lang="tl" class="with-flag" href="/tl-ph/index">Pilipinas (Filipino)</a>
                    </li>
                </ul>
                <ul>
                    <li class="region en-sg"><a lang="en" class="with-flag" href="/en-sg/index">Singapore</a></li>
                    <li class="region vi-vn"><a lang="vi" class="with-flag" href="/vi-vn/index">Việt Nam</a></li>
                    <li class="region ja-jp"><a lang="ja" class="with-flag" href="/index">日本語 </a></li>
                    <li class="region ko-kr"><a lang="ko" class="with-flag" href="/ko-kr/index">대한민국</a></li>
                    <li class="region zh-cn"><a lang="zh" class="with-flag" href="/zh-cn/index">简体中文</a></li>
                    <li class="region zh-tw"><a lang="zh" class="with-flag" href="/zh-tw/index">繁體中文</a></li>
                    <li class="region th-th"><a lang="th" class="with-flag" href="/th-th/index">ประเทศไทย</a></li>
                </ul>
                <ul class="other">
                    <li class="region en-ww"><a lang="en" class="with-flag" href="/index">Worldwide (English)</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
