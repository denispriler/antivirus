@extends('layouts.admin')
@section('content')
    <div class="main_page_first_line">
        <h2 class="main_page_first_line_title">
            Welcome to Vim
        </h2>
        <p class="main_page_first_line_text">
            You're logged in as {{Auth::user()->email}}
        </p>
    </div>
    <div class="main_page_menu">
        <ul class="main_page_menu_items">
            <li class="main_page_menu_item">
                <img src="{{asset('images/adminpanel/folder1212.png')}}" alt="Folder" class="main_page_menu_item_img">
                <div class="main_page_menu_item_container">
                    <a href="{{route('user.products')}}" class="main_page_menu_item_container_link">My products</a>
                    <p class="main_page_menu_item_container_info">
                        View and manage the subscriptions you've bought.
                    </p>
                </div>
            </li>
            <li class="main_page_menu_item">
                <img src="{{asset('images/adminpanel/monitor124.png')}}" alt="Monitor" class="main_page_menu_item_img">
                <div class="main_page_menu_item_container">
                    <a href="{{route('profile.show')}}" class="main_page_menu_item_container_link">Account settings</a>
                    <p class="main_page_menu_item_container_info">
                        View and manage your account details, change your password, and more.
                    </p>
                </div>
            </li>
            <li class="main_page_menu_item">
                <img src="{{asset('images/adminpanel/chat4550.png')}}" alt="Chat" class="main_page_menu_item_img">
                <div class="main_page_menu_item_container">
                    <a href="{{route('faq')}}" class="main_page_menu_item_container_link">How to install and
                        activate</a>
                    <p class="main_page_menu_item_container_info">
                        Download, install, and activate your product.
                    </p>
                </div>
            </li>
            {{--            <li class="main_page_menu_item">--}}
            {{--                <img src="{{asset('images/adminpanel/cards765.png')}}" alt="Cards" class="main_page_menu_item_img">--}}
            {{--                <div class="main_page_menu_item_container">--}}
            {{--                    <a href="#" class="main_page_menu_item_container_link">Get refund</a>--}}
            {{--                    <p class="main_page_menu_item_container_info">--}}
            {{--                        Request a refund during the 30-day refund period.--}}
            {{--                    </p>--}}
            {{--                </div>--}}
            {{--            </li>--}}
        </ul>
    </div>
@endsection
