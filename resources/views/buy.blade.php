@extends('layouts.default')
@section('content')
    <main class="buy_main">
        <div class="buy_main_choose">
            <div class="buy_main_choose_container">
                <h2 class="buy_main_choose_title">
                    Сhoose your level of protection
                </h2>
                <p class="buy_main_choose_available">
                    <span>Avaliable platforms: </span>
                    <img src="{{asset('images/wind.png')}}" alt="Windows">
                    <img src="{{asset('images/mac.png')}}" alt="MacOC">
                    <img src="{{asset('images/android.png')}}" alt="Android">
                    <img src="{{asset('images/apple.png')}}" alt="Apple">
                </p>
                <div class="buy_main_choose_items">

                    @foreach($products as $product)
                        <div class="buy_main_choose_item">
                            <h4 class="buy_main_choose_item_title">{{ $product->name }}</h4>
                            <p class="buy_main_choose_item_devices">one device</p>
                            <p class="buy_main_choose_item_price euro">{{ $product->price }}</p>
                            @if ($product->price == '0.00')
                                <a href="{{asset('test.exe')}}" class="buy_main_choose_item_buybutton">
                                    DOWNLOAD
                                </a>
                            @else
                                <button type="button" onclick="openBilling('{{ $product->slug }}', {{$product->price}})"
                                        class="buy_main_choose_item_buybutton">
                                    BUY NOW
                                </button>
                            @endif
                            <p class="buy_main_choose_item_undertex">
                                Price for the first year only
                            </p>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>


        <div class="confirm_popup" id="confirm-popup" style="display: none; opacity: 0;">
            <div class="confirm_popup_container">
                <ajax-form method="POST" action="{{route('api.product.store')}}" success="openSuccessful">
                    <div class="confirm_popup_container_firstline">
                        <span>
                            Credit or debit cards
                        </span>
                        <div class="confirm_popup_container_systems">
                            <img src="{{asset('images/mastercard.png')}}" alt="Master Cart">
                            <img src="{{asset('images/visa.png')}}" alt="Visa">
                        </div>
                    </div>
                    <div class="confirm_popup_container_billing">
                        <p class="confirm_popup_container_billing_title">
                            Please, tell us your billing details
                        </p>
                        <div class="confirm_popup_container_billing_inputs">
                            <input type="hidden" name="product.slug" id="product-slug">
                            <input type="text" name="user.name" id="billing-name" placeholder="First name(s):">
                            <input type="text" name="user.lastname" id="billing-lastname" placeholder="Last name:">
                            <input type="email" name="user.email" id="billing-email" placeholder="E-mail:">
                            <input type="tel" name="user.phone" id="billing-phone" placeholder="Phone number:">
                        </div>
                    </div>
                    <p class="confirm_popup_container_info">
                    </p>
                    <div class="confirm_popup_container_cart">
                        <input type="text" mask="0000 0000 0000 0000" mask-reverse="true" name="card.number"
                               id="cart-number" placeholder="Card number:">
                        <input type="text" mask="00/00" name="card.date" id="billing-date" placeholder="MM/YY">
                        <input type="text" mask="000" name="card.cvc" id="billing-cvc" placeholder="CVC">
                    </div>
                    <p data-error class="confirm_popup_container_error" style="text-align: center">
                    </p>
                    <p class="confirm_popup_container_price">
                <span class="confirm_popup_container_price_text">
                    Total price:
                </span>
                        <span class="confirm_popup_container_price_num">
                    <b id="total-sum"></b> <sub>€</sub>
                </span>
                    </p>
                    <button type="submit" style="margin-left: auto;margin-right: auto;"
                            class="confirm_popup_container_button">
                        Confirm
                    </button>
                </ajax-form>
            </div>
        </div>

        <div class="confirm_popup" id="saccefull-payment">
            <div class="saccefull_payment">
                <h3 class="saccefull_payment_title">
                    Payment Saccefull
                </h3>
                <div class="saccefull_payment_container">
                    <h4 class="saccefull_payment_container_ft">
                        Thank you for purchasing VimGuard!
                    </h4>
                    <p class="saccefull_payment_container_text">
                        To manage security across all your devices from anywhere via the Web,
                        you can create a Vim account. This allows you to check each device's security
                        &amp; license status, adjust key security settings - plus access special offers and
                        download free Vim products.
                        <br>
                        Vim also gives you easy access to technical support.
                        To avoid any conflicts between programs, you may need to close some
                        active applications and uninstall third-party solutions with antivirus
                        or similar functionality.
                    </p>
                    <h4 class="saccefull_payment_container_st">
                        Installing the program
                    </h4>
                    <p class="saccefull_payment_container_text">
                        To install VimGuard, download the installation file and run it on your pc.
                    </p>
                    <a href="#" class="saccefull_payment_container_link">Download VimGuard</a>
                    <h4 class="saccefull_payment_container_st">
                        Activate the program
                    </h4>
                    <p class="saccefull_payment_container_text">
                        To activate it, open VimGuard on your PC. Go to the Settings menu,
                        then go to the License tab, click the Activate product button.
                        In the field that appears, paste the key that you can copy in your
                        personal account
                    </p>
                    <a href="{{route('user.products')}}" class="saccefull_payment_container_link">{{route('user.products')}}</a>
                    <a href="{{route('user.products')}}" class="saccefull_payment_container_button">
                        go to activate
                    </a>
                </div>
            </div>
        </div>

        @include('components.module.blog')
    </main>
@endsection
