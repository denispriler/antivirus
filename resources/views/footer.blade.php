<div class="chrome-banner inverse" style="display:none" id="offer-chrome" data-cmp-name="cmp-chrome-banner">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-xl-10">
                <div class="chrome-banner__content">
                    <div class="chrome-banner__info">
                        <div class="chrome-banner__ico">
                            <img src="{{asset('images/local/chrome.svg')}}" alt="">
                        </div>
                        <h4 class="chrome-banner__title">
                            VimGuard recommends using <br> the <b>FREE</b> Chrome&trade; internet browser.
                        </h4>
                    </div>
                    <div class="chrome-banner__cta">
                        <div class="d-none d-md-block">
                            <div data-cmp-name="cmp-button" class="btn-wrapper ">
                                <a href="https://www.google.com/chrome/"
                                   data-cms-component="button--btn-lg btn-outline-secondary"
                                   data-role="cta-link" class="btn btn-lg btn-outline-secondary">
                                    <span>Download Chrome</span>
                                </a>
                            </div>
                        </div>
                        <a href="https://play.google.com/store/apps/details/Google_Chrome_Fast_Secure?id=com.android.chrome"
                           class="d-block d-md-none chrome-banner__play" target="_blank" rel="noopener noreferrer"
                           title="Download Chrome">
                            <img
                                src="{{asset('images/local/google-play.svg')}}"
                                alt="" data-cmp-name="cmp-store-badge-img" class="storebadge-google ">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="bottom-links inverse" data-cmp-name="cmp-footer-links">
    <div class="container">
        <div class="row internal">
            <div class="col-md-12 col-lg-4 select-region">
                <img class="logo-vim" src="{{asset('images/logo-white.png')}}"
                     alt="VimGuard">
                <p class="region en-us">
                    <a class="select-region-button with-flag js-language-selector-trigger"
                       aria-label="United States (English) opens dialog" tabindex="0">
                        <span>United States (English)</span><img
                            src="{{asset('images/local/arrow-s-down-white.svg')}}" alt="">
                    </a>
                </p>
            </div>
            <div class="col-md-3 col-lg-2 links sec1">
                <p class="tagline-medium">For home</p>
                <ul class="list-unstyled">
                    <li><a href="#" target="_blank" rel="noopener noreferrer">Support</a></li>
                    <li><a href="#">Security</a></li>
                    <li><a href="#" target="_blank" rel="noopener noreferrer">Privacy</a></li>
                    <li><a href="#">Performance</a></li>
                    <li><a href="#" target="_blank" rel="noopener noreferrer">Blog</a></li>
                    <li><a href="#" target="_blank" rel="noopener noreferrer">Forum</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-lg-2 links sec2">
                <p class="tagline-medium">For business</p>
                <ul class="list-unstyled">
                    <li><a href="/en-us/business/support">Business support</a></li>
                    <li><a href="/en-us/business">Business products</a></li>
                    <li><a href="/en-us/business/become-a-partner">Business partners</a></li>
                    <li><a href="#">Business blog</a></li>
                    <li><a href="/en-us/affiliates">Affiliates</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-lg-2 links sec3">
                <p class="tagline-medium">For partners</p>
                <ul class="list-unstyled">
                    <li><a href="/en-us/mno">Mobile Carriers</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-lg-2 links sec4">
                <p class="tagline-medium">Company</p>
                <ul class="list-unstyled">
                    <li><a href="/en-us/contacts">Contact Us</a></li>
                    <li><a href="#" target="_blank" rel="noopener noreferrer">Investors</a></li>
                    <li><a href="/en-us/careers">Careers</a></li>
                    <li><a href="#" target="_blank" rel="noopener noreferrer">Press Center</a></li>
                    <li><a href="/en-us/responsibility">Responsibility</a></li>
                    <li><a href="/en-us/technology">Technology</a></li>
                    <li><a href="/en-us/online-research">Research Participation</a></li>
                </ul>
            </div>
            <div class="social">
                <div class="social-icons" data-cmp-name="cmp-social-icons">
                    <a href="#" title="Facebook" aria-label="Facebook"
                       class="social-icons__item medium">
                        <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                            <path fill="#A8A4C3" fill-rule="evenodd"
                                  d="M20.575 16.347H17.86V25h-3.992v-8.653H12v-3.3h1.868v-2.151c-.072-1.064.338-2.106 1.121-2.852.784-.746 1.863-1.122 2.955-1.03h2.972v3.22h-2.123c-.244-.01-.48.086-.643.262-.164.175-.24.413-.206.648v1.9H21l-.425 3.303z">
                            </path>
                        </svg>
                    </a>
                    <a href="#" title="Instagram" aria-label="Instagram"
                       class="social-icons__item medium">
                        <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                            <path fill="#A8A4C3" fill-rule="evenodd"
                                  d="M27 11.492c.001-.917-.165-1.826-.49-2.683-.242-.74-.661-1.41-1.221-1.952-.54-.561-1.21-.981-1.952-1.224-.858-.326-1.768-.492-2.686-.49-1.543-.134-3.092-.173-4.64-.12-1.505-.05-3.012-.01-4.513.121-.918-.001-1.828.165-2.686.49-.742.241-1.414.66-1.957 1.219-.56.541-.98 1.211-1.222 1.952-.325.858-.49 1.77-.488 2.687-.135 1.5-.176 3.005-.122 4.51-.05 1.504-.01 3.01.122 4.508-.002.917.164 1.827.49 2.684.242.74.66 1.41 1.22 1.952.543.56 1.213.978 1.954 1.22.858.325 1.769.49 2.686.49 1.5.131 3.008.172 4.514.121 1.505.054 3.012.013 4.513-.121.917 0 1.828-.165 2.686-.49 1.49-.514 2.661-1.684 3.175-3.173.326-.857.492-1.766.49-2.683 0-1.22.122-1.586.122-4.51L27 11.492zm-2.076 8.9c.009.71-.115 1.413-.366 2.075-.425.913-1.158 1.647-2.071 2.073-.663.25-1.367.375-2.076.366-1.1 0-1.465.121-4.396.121-1.466.056-2.934.015-4.395-.121-.711.01-1.418-.114-2.084-.366-.912-.425-1.645-1.158-2.071-2.069-.25-.662-.375-1.366-.366-2.074 0-1.098-.122-1.464-.122-4.392-.055-1.465-.014-2.931.122-4.391-.01-.71.114-1.417.366-2.082.213-.45.502-.862.854-1.216.286-.426.72-.73 1.217-.853.664-.251 1.37-.375 2.08-.366 1.099 0 1.465-.122 4.395-.122 1.467-.055 2.935-.014 4.396.122.709-.009 1.413.115 2.076.366.913.424 1.648 1.157 2.075 2.069.25.662.375 1.366.366 2.074 0 1.098.122 1.464.122 4.392.055 1.466.014 2.934-.122 4.395zM16.01 10.392c-3.101 0-5.615 2.512-5.615 5.61 0 3.1 2.514 5.611 5.615 5.611 3.102 0 5.616-2.512 5.616-5.61.025-1.496-.559-2.938-1.617-3.995-1.059-1.058-2.502-1.641-3.999-1.616zm0 9.27c-.976.018-1.918-.362-2.61-1.052-.69-.69-1.07-1.632-1.052-2.608-.018-.975.362-1.916 1.053-2.607.69-.69 1.633-1.07 2.61-1.052.976-.017 1.918.362 2.609 1.052.69.69 1.07 1.632 1.053 2.607.018.976-.362 1.917-1.053 2.607-.69.69-1.633 1.07-2.61 1.053zm5.86-10.856c-.741 0-1.342.6-1.342 1.342 0 .74.6 1.341 1.343 1.341.741 0 1.343-.6 1.343-1.341-.007-.739-.604-1.336-1.343-1.342z">
                            </path>
                        </svg>
                    </a>
                    <a href="#" title="Twitter" aria-label="Twitter"
                       class="social-icons__item medium">
                        <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                            <path fill="#A8A4C3" fill-rule="evenodd"
                                  d="M24.762 11.5v.587c.012 4.72-2.547 9.07-6.668 11.334-4.122 2.265-9.146 2.082-13.094-.475.389 0 .681.097 1.071.097 2.051.027 4.047-.665 5.646-1.956-1.916-.042-3.595-1.298-4.186-3.13.285.08.581.113.876.097.396-.018.789-.083 1.169-.196-2.089-.421-3.594-2.26-3.603-4.401v-.098c.595.334 1.267.503 1.947.489-1.251-.823-2.017-2.214-2.044-3.717-.015-.79.187-1.569.584-2.25 2.285 2.86 5.698 4.575 9.345 4.695 0-.293-.097-.684-.097-1.076.029-2.473 2.017-4.472 4.478-4.5 1.262-.01 2.467.524 3.31 1.468.995-.196 1.95-.56 2.823-1.077-.327 1.054-1.018 1.956-1.947 2.544.897-.136 1.778-.365 2.628-.684-.646.844-1.398 1.6-2.238 2.249z">
                            </path>
                        </svg>
                    </a>
                    <a href="#" title="YouTube" aria-label="Youtube"
                       class="social-icons__item medium">
                        <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                            <path fill="#A8A4C3" fill-rule="evenodd"
                                  d="M16 7c2.51 0 5.022.112 7.533.336C26.628 7.612 29 10.205 29 13.312v6.376c0 3.107-2.372 5.7-5.467 5.976C21.022 25.888 18.51 26 16 26c-2.51 0-5.022-.112-7.533-.336C5.372 25.388 3 22.795 3 19.688v-6.376c0-3.107 2.372-5.7 5.467-5.976C10.978 7.112 13.49 7 16 7zm-3 6v7l7-3.5-7-3.5z">
                            </path>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer inverse web" data-cmp-name="cmp-footer">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-12  copyright">
                <p>1988-{{now()->year}} Copyright Vim Software s.r.o.</p>
            </div>
            <div class="col-12 middle">
                <a href="/en-us/privacy-policy">Privacy policy</a>
                <a href="/en-us/legal">Legal</a>
                <a href="/coordinated-vulnerability-disclosure">Report vulnerability</a>
                <a href="/bug-bounty">Contact security</a>
                <a href="#"
                   target="_blank" rel="noopener">Modern Slavery Statement</a>
                <a href="/en-us/do-not-sell">Do not sell my info</a>
                <button id="ot-sdk-btn" class="ot-sdk-show-settings">Cookie Preferences</button>
            </div>
            <div class="col-12  right">
            </div>
        </div>
    </div>
</div>
<div class="js-popup-dtp popup-dtp" id="downloadPopup" data-behavior="downloadPopup" data-role="popup"
     style="display: none">
    <div data-browser="chrome">
        <div class="dtp-arrow">
            <div class="dtp-arrow-header"></div>
            <div class="dtp-arrow-body">
                <p>Click this file to start installing VimGuard</p>
            </div>
            <div class="dtp-arrow-footer"></div>
        </div>
        <div class="js-dtp-close dtp-close">
            <span class="text">Close</span>
            <span class="cross"></span>
        </div>
        <div class="content">
            <div id="dwnTy-download-popup" class="dwnTy">
                <div class="AVsizer">
                    <div class="center">
                        <h1>Follow these steps to complete your VimGuard installation:</h1>
                        <p class="noteProgress" style="display: none;"><span></span></p>
                        <p class="note noteHidden" style="display: inline-block;"><strong>Note</strong>: If your
                            download did not
                            start automatically, please <a
                                href="/en-us/download-thank-you.php?product=FAV-ONLINE&locale=en-us"
                                data-bi-download-name="FAV-ONLINE" class="bi-download-link" data-role="download-link"
                                data-download-name="FAV-ONLINE">click here</a>.</p>
                    </div>
                    <div class="boxes">

                        <div class="row">
                            <div class="span4">
                                <div class="box">
                                    <span class="step"><span>1</span></span>
                                    <div class="box-in">
                                        <div class="same-height">
                                            <h2>Run the VimGuard installer</h2>
                                            <p>Click the downloaded file at the bottom left corner of your screen.</p>
                                        </div>
                                        <!-- need exe image -->
                                        <img src="/" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="box">
                                    <span class="step"><span>2</span></span>
                                    <div class="box-in">
                                        <div class="same-height">
                                            <h2>Confirm the installation</h2>
                                            <p>Click "Yes" on the system dialog window to approve the start of your
                                                VimGuard installation.</p>
                                        </div>
                                        <img src="{{asset('images/local/ch-step2.png')}}" alt="">
                                    </div>

                                </div>
                            </div>
                            <div class="span4">
                                <div class="box">
                                    <span class="step"><span>3</span></span>
                                    <div class="box-in">
                                        <div class="same-height">
                                            <h2>Follow setup instructions</h2>
                                            <p>Click the button in the installer window to begin installation.</p>
                                        </div>
                                        <img src="{{asset('images/local/express-install.png')}}" alt="">
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="clear"></div>
                    </div>
                    <p class="offlinep" style="display:none">
                        If you need to install VimGuard on a PC without an internet connection,<br> you can download the
                        offline
                        installer <a href="/en-us/download-thank-you.php?product=FAV-ONLINE-DIRECT&locale=en-us"
                                     data-bi-download-name="FAV-ONLINE-DIRECT" class="bi-download-link"
                                     data-role="download-link"
                                     data-download-name="FAV-ONLINE">here</a>.
                    </p>
                    <div class="techSupport">
                        <img src="{{asset('images/local/icon-phone.svg')}}" alt="">
                        <p><b>Need help?</b> Please call <span>855-745-3255</span></p>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <div data-browser="firefox" id="popup-dtp-firefox" style="display: none">
        <div class="dtp-arrow-title">
            <p>Click this file to start <br>installing VimGuard</p>
        </div>
        <div class="dtp-arrow firefox">
            <div class="dtp-arrow-body">


                <div id="animation_container" style="background-color:rgba(36, 44, 65, 1.00); width:56px; height:56px">
                    <canvas id="canvas" width="56" height="56"></canvas>
                    <div id="dom_overlay_container"
                         style="pointer-events:none; overflow:hidden; width:56px; height:56px; position: absolute; left: 0px; top: 0px; display: block;">
                    </div>
                </div>


                <img class="arrow-top"
                     src="{{asset('images/local/dtp-ff-arrow-website-698.svg')}}" alt="">
            </div>
        </div>


        <div class="js-dtp-close dtp-close">
            <span class="text">CLOSE</span>
            <span class="cross"></span>
        </div>

        <div class="content">

            <div id="dwnTy-download-popup-firefox" class="dwnTy">
                <div class="AVsizer">

                    <div class="center">
                        <h1>Almost done!</h1>
                        <p class="info">Follow these 3 easy steps to complete your VimGuard installation</p>


                        <p class="noteProgress" style="display: none;"><span></span></p>
                        <p class="note noteHidden firefox" style="display: inline-block;"><strong>Note</strong>: If your
                            download
                            did not start automatically, please <a
                                href="/en-us/download-thank-you.php?product=FAV-ONLINE&locale=en-us"
                                data-bi-download-name="FAV-ONLINE" class="bi-download-link" data-role="download-link"
                                data-download-name="FAV-ONLINE">click here</a>.</p>
                    </div>
                    <div class="boxes">

                        <div class="row">
                            <div class="span4">
                                <div class="box firefox">
                                    <span class="step"><span>Step 1</span></span>
                                    <div class="box-in">
                                        <div class="same-height">
                                            <h2>Run the VimGuard installer</h2>
                                            <p>Click the downloaded file at the <br />top right corner of your screen.
                                            </p>
                                        </div>
                                    </div>
                                    <!-- need exe image -->
                                    <img src="/" alt="">
                                </div>
                            </div>
                            <div class="span4">
                                <div class="box firefox">
                                    <span class="step"><span>Step 2</span></span>
                                    <div class="box-in">
                                        <div class="same-height">
                                            <h2>Confirm the installation</h2>
                                            <p>Click "Yes" on the system dialog window to approve the start of your
                                                VimGuard installation.</p>
                                        </div>
                                    </div>
                                    <img src="{{asset('images/local/dtp-ff-step2-website-698.png')}}"
                                         alt>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="box firefox">
                                    <span class="step"><span>Step 3</span></span>
                                    <div class="box-in">
                                        <div class="same-height">
                                            <h2>Follow setup instructions</h2>
                                            <p>Click the button in the installer <br />window to begin installation.</p>
                                        </div>
                                    </div>
                                    <img src="{{asset('images/local/dtp-ff-step3-website-698.png')}}"
                                         alt>
                                </div>
                            </div>
                        </div>

                        <div class="clear"></div>
                    </div>

                    <p class="offlinep" style="display:none">
                        Follow setup instructions
                    </p>

                    <div class="techSupport">
                        <img src="{{asset('images/local/icon-phone.svg')}}" alt="">
                        <p><b>Need help?</b> Please call <span>855-745-3255</span></p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/components.js')}}"></script>
<script src="{{asset('js/custom.js')}}"></script>
<script src="{{asset('js/static/10001792/web/j/v2/vendor/cash.js')}}"></script>
<script src="{{asset('js/static/10001792/web/j/v2/vimguard.js')}}"></script>
<script src="{{asset('js/static/10001792/web/j/v2/vendor/bootstrap-native.js')}}"></script>


<script src="{{asset('js/static/10001792/web/j/lib/createjs/bundle.js')}}"></script>
<script src="{{asset('js/static/10001792/web/j/dtp-overlay-firefox.js')}}"></script>

<script src="{{asset('js/aos.js')}}"></script>

<svg width="0" height="0" class="hidden" style="display: none;">

    <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" id="icon-arrow">
        <path fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.2"
              d="M3 12.5h18m-6-5l6 5-6 5" />
    </symbol>

    <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" id="next-arrow">
        <path fill="none" fill-rule="evenodd" stroke="#FFF" stroke-linecap="round" stroke-linejoin="round"
              stroke-width="1.2" d="M9 4l8 8-8 8" />
    </symbol>

    <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" id="back-arrow">
        <path fill="none" fill-rule="evenodd" stroke="#4E22D0" stroke-linecap="round" stroke-linejoin="round"
              stroke-width="1.2" d="M15 4l-8 8 8 8" />
    </symbol>

    <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" id="back-arrow-white">
        <path fill="none" fill-rule="evenodd" stroke="#ffffff" stroke-linecap="round" stroke-linejoin="round"
              stroke-width="1.2" d="M15 4l-8 8 8 8" />
    </symbol>

    <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" id="icon-premium-security">
        <path fill="none" fill-rule="evenodd" stroke="#FF7800" stroke-linecap="round" stroke-linejoin="round"
              stroke-width="1.2"
              d="M20.819 13.268c.028.069.014.123-.043.165l-2.368 1.644c-.144.095-.23.185-.258.267-.029.095-.014.226.043.39l.904 2.651c.028.069.014.123-.043.165-.072.04-.13.04-.172 0l-2.39-1.644c-.143-.096-.265-.144-.366-.144h-.064c-.086 0-.208.048-.366.144l-2.368 1.644a.136.136 0 0 1-.172 0 .15.15 0 0 1-.065-.165l.904-2.65c.072-.165.086-.296.043-.391-.028-.082-.115-.172-.258-.267l-2.346-1.644a.15.15 0 0 1-.065-.165c.029-.055.079-.082.15-.082h2.907c.186 0 .315-.028.387-.082.072-.055.144-.171.216-.35l.904-2.65c.014-.055.057-.083.129-.083h.064c.072 0 .122.028.151.082l.904 2.651c.058.179.122.295.194.35.086.054.222.082.409.082h2.906c.057 0 .1.027.129.082zM27 13.6c0 1.766-.261 3.735-.646 4.862-1.205 3.572-4.405 6.696-9.6 9.372-.23.111-.481.166-.754.166-.272 0-.524-.055-.753-.166-5.195-2.676-8.513-5.753-9.58-9.372a17.094 17.094 0 0 1-.666-4.862c.014-1.78 0-4.3 0-4.32 0-1.49.954-2.618 2.346-2.963.614-.152 2.982-.814 7.103-1.986C15.15 4.111 15.667 4 16 4c.334 0 .879.11 1.636.331 3.77 1.076 6.123 1.738 7.06 1.986C26.104 6.69 27 7.77 27 9.28v4.32z" />
    </symbol>

    <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" id="icon-privacy">
        <path fill="none" fill-rule="evenodd" stroke="#FF7800" stroke-linecap="round" stroke-linejoin="round"
              stroke-width="1.5"
              d="M16 22v-3.5V22zm-9.5 2.16v-7.391c0-2.179 1.052-3.269 3.155-3.269h12.69c2.103 0 3.155 1.09 3.155 3.269v7.39c0 2.228-1.052 3.341-3.155 3.341H9.655c-2.103 0-3.155-1.113-3.155-3.34zm4-10.66V9.841c.047-1.463.6-2.707 1.654-3.731 1.054-1.073 2.325-1.61 3.81-1.61 1.534 0 2.851.537 3.954 1.61 1.007 1.024 1.535 2.268 1.582 3.731V13.5">
        </path>
    </symbol>
    <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 31" id="icon-security">
        <path fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"
              d="M12.38 14.5l2.72 2.92c.2.12.394.12.58 0l4.92-5.1m-15.1 1c0 1.653.2 3.2.6 4.64 1.147 3.56 4.194 6.686 9.14 9.38.2.106.454.16.76.16.24 0 .46-.054.66-.16 4.96-2.694 8.04-5.82 9.24-9.38.4-1.44.6-2.987.6-4.64V8.6c0-.654-.2-1.207-.6-1.66-.36-.494-.913-.867-1.66-1.12L17.5 3.86c-.76-.24-1.26-.36-1.5-.36-.306 0-.833.12-1.58.36L7.74 5.82c-.746.253-1.32.626-1.72 1.12-.347.453-.52 1.006-.52 1.66v4.72z">
        </path>
    </symbol>

    <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" id="icon-vpn">
        <path fill="none" fill-rule="evenodd" stroke="#FF7800" stroke-linecap="round" stroke-linejoin="round"
              stroke-width="1.2"
              d="M10.135 16.5h12c1.033 0 1.652.8 1.652 1.8s-.135 6.6-.135 7.4c0 1-.62 1.8-1.652 1.8H10c-1.032 0-1.652-.8-1.652-1.8 0-.8.136-6.4.136-7.4s.62-1.8 1.651-1.8zm1.365 0v-2c0-2.456 2.106-4 4.5-4s4.5 1.544 4.5 4v2m-4.5 7v-3M10.89 8.6c1.533-1.334 3.03-2.1 5.11-2.1 2.102 0 4.218.766 5.729 2.1M8 6c2.239-2 4.916-3.5 8-3.5 3.13 0 5.785 1.5 8 3.5" />
    </symbol>

    <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" id="icon-vpn-purple">
        <path fill="none" fill-rule="evenodd" stroke="#4e22d0" stroke-linecap="round" stroke-linejoin="round"
              stroke-width="1.2"
              d="M10.135 16.5h12c1.033 0 1.652.8 1.652 1.8s-.135 6.6-.135 7.4c0 1-.62 1.8-1.652 1.8H10c-1.032 0-1.652-.8-1.652-1.8 0-.8.136-6.4.136-7.4s.62-1.8 1.651-1.8zm1.365 0v-2c0-2.456 2.106-4 4.5-4s4.5 1.544 4.5 4v2m-4.5 7v-3M10.89 8.6c1.533-1.334 3.03-2.1 5.11-2.1 2.102 0 4.218.766 5.729 2.1M8 6c2.239-2 4.916-3.5 8-3.5 3.13 0 5.785 1.5 8 3.5" />
    </symbol>

    <symbol id="icon-ios" viewBox="0 0 33 32">
        <title>icon-ios</title>
        <path
            d="M21.918 31.951h-9.55c-7.169 0-11.201-4.218-11.201-11.199v-9.55c0-7.355 3.938-11.2 11.201-11.2h9.55c7.26 0 11.199 3.845 11.199 11.2v9.55c0 7.168-3.846 11.199-11.199 11.199zM7.735 20.577h1.333v-6.001h-1.333v6.001zM8.401 11.089c-0.439 0-0.756 0.299-0.756 0.708 0 0.401 0.324 0.702 0.753 0.702h0.004c0.431 0 0.756-0.301 0.756-0.702 0-0.409-0.318-0.708-0.756-0.708zM14.84 11.234c-2.617 0-4.242 1.804-4.242 4.71 0 2.908 1.625 4.716 4.242 4.716 2.606 0 4.226-1.807 4.226-4.716 0-2.906-1.62-4.71-4.226-4.71zM23.733 12.149c1.107 0 1.896 0.529 2.093 1.388h1.126c-0.161-1.401-1.452-2.368-3.199-2.368-1.941 0-3.245 1.047-3.245 2.603 0 1.298 0.722 2.044 2.343 2.423l1.201 0.288c1.307 0.305 1.89 0.788 1.89 1.563 0 0.907-0.909 1.567-2.163 1.567-1.256 0-2.165-0.562-2.36-1.44l-1.152 0.001c0.175 1.478 1.49 2.425 3.399 2.425 2.125 0 3.444-1.049 3.444-2.738 0-1.326-0.749-2.041-2.586-2.463l-1.025-0.251c-1.293-0.303-1.82-0.728-1.82-1.467 0-0.916 0.825-1.53 2.054-1.53zM14.84 19.612c-1.88 0-3.046-1.406-3.046-3.668 0-2.223 1.196-3.662 3.046-3.662 1.869 0 3.031 1.403 3.031 3.662 0 2.227-1.189 3.668-3.031 3.668z">
        </path>
    </symbol>

    <symbol id="icon-mac" viewBox="0 0 32 32">
        <title>icon-mac</title>
        <path fill="#FFF" fill-rule="evenodd"
              d="M21.84 8.006c.508 0 1.292.17 2.354.511 1.06.34 2.053 1.124 2.976 2.35-.185.045-.727.521-1.627 1.43-.9.907-1.419 2.269-1.557 4.085.185 2.179.854 3.71 2.007 4.596 1.153.885 1.822 1.328 2.007 1.328-.046.09-.23.533-.554 1.327-.323.795-.83 1.714-1.522 2.758a13.7 13.7 0 0 1-1.973 2.485c-.715.704-1.545 1.056-2.491 1.056-.946 0-1.73-.17-2.353-.51-.623-.341-1.465-.511-2.526-.511-1.061 0-1.915.17-2.56.51-.647.34-1.408.534-2.284.579-.97 0-1.834-.374-2.596-1.124A14.089 14.089 0 0 1 7.1 26.323c-1.338-1.907-2.284-4.233-2.838-6.98-.553-2.746-.23-5.3.97-7.66A8.226 8.226 0 0 1 7.86 9.062a6.442 6.442 0 0 1 3.46-.987 7.56 7.56 0 0 1 2.7.612c.83.364 1.545.568 2.145.613.6-.045 1.395-.283 2.387-.715.992-.431 2.088-.624 3.288-.579zM20.974 0a6.874 6.874 0 0 1-.262 2.604 8.95 8.95 0 0 1-1.019 2.248 5.053 5.053 0 0 1-1.63 1.605c-.659.405-1.338.583-2.036.535a6.515 6.515 0 0 1 .29-2.604c.272-.832.602-1.534.99-2.105A5.53 5.53 0 0 1 19.023.678c.679-.404 1.329-.63 1.95-.678z" />
    </symbol>


    <symbol id="icon-pc" viewBox="0 0 33 32">
        <title>icon-pc</title>
        <path fill="#FFF" fill-rule="evenodd"
              d="M29.5 17v12l-14-2.2V17h14zm-16 0v9.5l-10-1.58V17h10zm0-11.5V15h-10V7.08l10-1.58zm16-2.5v12h-14V5.2l14-2.2z" />
    </symbol>


    <symbol id="icon-android" viewBox="0 0 26 32">
        <title>icon-android</title>
        <path
            d="M24.704 22.225c-1.014 0-1.543-0.499-1.543-1.542v-7.713c0-1.042 0.529-1.544 1.543-1.544 1.017 0 1.543 0.502 1.543 1.544v7.713c0 1.042-0.526 1.542-1.543 1.542zM20.077 25.312h-1.712v4.629c0 1.030-1.151 1.536-2.158 1.536s-2.472-0.506-2.472-1.536v-4.629h-1.179v4.629c0 1.030-1.457 1.536-2.464 1.536-1.006 0-2.166-0.506-2.166-1.536v-4.629h-1.735c-0.784 0-1.543-0.752-1.543-1.543v-12.343h16.971v12.343c0 0.791-0.76 1.543-1.543 1.543zM8.887 3.59l-1.541-2.282c-0.073-0.106-0.116-0.238-0.116-0.38 0-0.23 0.115-0.434 0.29-0.556l0.002-0.001c0.105-0.073 0.236-0.116 0.377-0.116 0.23 0 0.432 0.116 0.552 0.292l0.002 0.002 1.68 2.487c0.895-0.323 1.927-0.509 3.003-0.509 1.077 0 2.111 0.187 3.070 0.53l-0.064-0.020 1.68-2.488c0.121-0.179 0.323-0.295 0.552-0.295 0.141 0 0.271 0.044 0.378 0.118l-0.002-0.001c0.178 0.124 0.293 0.327 0.293 0.557 0 0.142-0.044 0.274-0.118 0.382l0.002-0.002-1.539 2.282c2.155 1.158 4.032 4.101 4.232 6.43h-16.971c0.199-2.328 2.083-5.271 4.239-6.43zM16.179 7.017c0.496-0.002 0.897-0.404 0.897-0.901 0-0.002 0-0.003 0-0.005v0c-0.018-0.481-0.412-0.865-0.897-0.865s-0.879 0.384-0.897 0.864l-0 0.002c0 0.501 0.4 0.905 0.896 0.905zM10.013 7.017c0.495-0.003 0.896-0.405 0.896-0.901 0-0.002 0-0.003 0-0.005v0c0.001-0.017 0.002-0.036 0.002-0.055 0-0.496-0.402-0.898-0.898-0.898s-0.898 0.402-0.898 0.898c0 0.019 0.001 0.039 0.002 0.058l-0-0.003c0 0.001 0 0.002 0 0.002 0 0.497 0.401 0.9 0.897 0.903h0zM1.561 22.225c-1.016 0-1.543-0.502-1.543-1.542v-7.713c0-1.042 0.527-1.544 1.543-1.544s1.544 0.502 1.544 1.544v7.713c0 1.040-0.528 1.542-1.544 1.542z">
        </path>
    </symbol>
    <symbol id="icon-best-protection" viewBox="0 0 48 48">
        <title>best-protection</title>
        <path fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="3" stroke-width="1.5"
              d="M21.004 36.426c1.163.822 2.446 1.623 3.85 2.4.234.133.551.199.95.199.3 0 .583-.066.851-.199 6.199-3.367 10.066-7.301 11.6-11.801.5-1.768.75-3.7.75-5.8v-5.85c0-.834-.25-1.533-.75-2.101-.434-.633-1.134-1.1-2.1-1.399l-8.5-2.45c-.934-.3-1.551-.45-1.851-.45-.399 0-1.066.15-2 .45l-8.35 2.45c-.934.3-1.65.767-2.15 1.399-.433.567-.65 1.267-.65 2.101v1.85"
              clip-rule="evenodd" />
        <path fill="none" stroke-linecap="round" stroke-miterlimit="10" stroke-width="1.5"
              d="M13.767 21.009c2.201 0 4.087.785 5.659 2.354 1.572 1.57 2.358 3.455 2.358 5.652 0 2.199-.786 4.084-2.358 5.652-1.572 1.57-3.458 2.355-5.659 2.355-2.2 0-4.086-.785-5.659-2.355C6.537 33.1 5.75 31.215 5.75 29.016c0-2.197.786-4.082 2.358-5.652 1.573-1.57 3.459-2.355 5.659-2.355zM19.049 25.955l2.829-2.025M9.429 21.056l1.744 3.25m2.358-3.297v-1.695m-8.205 5.322l1.274.612m10.704-3.486l.519-.847m-2.547 13.046l1.462 3.676m-8.064-7.491l-3.678 1.035m16.693-.941l1.604.471m-8.724-2.26l.66.66m4.668 5.039l.66.66M9.476 35.893l-.613.99"
              clip-rule="evenodd" />
    </symbol>
    <symbol id="icon-lowest-impact" viewBox="0 0 48 48">
        <title>lowest-impact</title>
        <path fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1.5"
              d="M25.053 37.099l2.031-3.493c-3.925-2.654-4.061-6.771-.406-12.351 1.506-2.322 3.549-4.516 6.131-6.582l1.664 6.135 1.26-8.166c1.434-.813 2.855-1.327 4.262-1.543 1.381 3.304 1.137 7.583-.73 12.837-.352.975-.744 1.977-1.178 3.006-2.842 6.608-6.51 8.829-11.002 6.663l1.949-3.373m-2.356-8.977l2.355 8.978 2.436-4.184 3.004-5.241m-3.004 5.241l7.795-2.113m.73-12.837l-5.521 9.709"
              clip-rule="evenodd" />
        <path fill="none" stroke-linecap="round" stroke-miterlimit="10" stroke-width="1.5"
              d="M34.926 11.177c-.539-.184-1.172-.275-1.9-.275H14.875c-2.767 0-4.149 1.383-4.149 4.15v13.7m12.149 3.25h-2.149l-.95-.7h-14.6v.05c.033.8.333 1.5.899 2.1.533.533 1.25.834 2.15.9h16.149m9.95 0h5.451c.766-.033 1.467-.367 2.1-1 .566-.533.883-1.217.949-2.05h-4.898"
              clip-rule="evenodd" />
    </symbol>
    <symbol id="icon-most-downloaded" viewBox="0 0 48 48">
        <title>most-downloaded</title>
        <g fill="none" stroke-linecap="round" stroke-width="1.5" clip-rule="evenodd">
            <path stroke-linejoin="round" stroke-miterlimit="3"
                  d="M19.875 39.95l.75 2.05 2.899-6.8h.851l2.899 6.8.75-2.05m3.901-1.65l1.95.851-3.101-7.101m-13.649 0l-3.101 7.101 1.95-.851" />
            <path stroke-miterlimit="10"
                  d="M28.024 39.95c.7-1.834 2-2.383 3.9-1.65m-15.949 0c1.891-.731 3.19-.182 3.9 1.65" />
        </g>
        <path fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="3" stroke-width="1.5"
              d="M22.675 6.6c.5-.4.934-.6 1.3-.6.367 0 .8.2 1.3.6l1.7 1.2c.8.6 1.316.967 1.55 1.1.267.133.8.25 1.601.35l2.35.35c.666.1 1.117.25 1.35.45.234.233.4.7.5 1.4l.351 2.35c.134.833.267 1.367.399 1.6.101.267.467.833 1.101 1.7l1.149 1.55c.4.5.601.933.601 1.3s-.2.817-.601 1.35l-1.449 1.95c-.434.6-.7 1.05-.801 1.35-.133.267-.283.9-.449 1.9l-.301 2.05c-.1.634-.266 1.084-.5 1.351-.232.233-.684.399-1.35.5l-2.05.3c-1 .166-1.634.3-1.9.4-.233.133-.899.6-2 1.399-1.1.866-1.767 1.333-2 1.4-.3.066-.6.066-.899 0-.333-.067-.65-.25-.95-.55l-1.8-1.25c-.733-.533-1.217-.867-1.45-1-.267-.101-.917-.234-1.95-.4l-2.05-.25c-.6-.133-1.05-.316-1.35-.55-.2-.267-.367-.717-.5-1.351l-.301-2.05c-.133-1-.25-1.633-.35-1.9-.133-.3-.517-.883-1.15-1.75l-1.1-1.55c-.4-.533-.6-.983-.6-1.35s.199-.8.6-1.3l1.3-1.8c.5-.7.8-1.183.9-1.45.133-.233.25-.784.35-1.65l.351-2.3c.133-.7.3-1.167.5-1.4.3-.2.75-.35 1.35-.45l2.1-.35c1-.1 1.634-.216 1.9-.35.233-.167.7-.5 1.4-1L22.675 6.6z"
              clip-rule="evenodd" />
        <path fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="3" stroke-width="1.5"
              d="M23.575 13.85c.066-.1.149-.167.25-.2.1-.034.199-.034.3 0 .1.033.184.1.25.2l1.399 3.5 3.8.3c.134 0 .234.05.301.15.066.066.1.167.1.3 0 .1-.05.2-.15.3l-2.899 2.4.899 3.7c0 .1-.016.2-.05.3-.066.1-.149.167-.25.2-.134 0-.233-.017-.3-.05l-3.25-2-3.25 2c-.1.033-.2.05-.3.05-.133-.033-.217-.1-.25-.2-.066-.1-.083-.2-.05-.3l.899-3.7-2.899-2.4c-.101-.1-.15-.2-.15-.3 0-.133.033-.233.101-.3.066-.1.149-.15.25-.15l3.8-.3 1.449-3.5z"
              clip-rule="evenodd" />
    </symbol>
    <symbol id="svg-sprite"></symbol>
</svg>
<script>
    $(document).ready(function () {

        var win = window;
        var marker = document.querySelector('.globe-earth');
        if (marker) {
            //отслеживаем событие прокрутки страницы
            win.scroll(function () {
                //Складываем значение прокрутки страницы и высоту окна, этим мы получаем положение страницы относительно нижней границы окна, потом проверяем, если это значение больше, чем отступ нужного элемента от верха страницы, то значит элемент уже появился внизу окна, соответственно виден
                if (win.scrollTop() + win.height() >= marker.offset().top) {
                    $('.globe-earth-round').addClass('rotate'); //выполняем действия если элемент виден
                } else {
                    // console.log('не виден'); //выполняем действия если элемент не виден
                }
            });
        }
    });
</script>
<script src="{{asset('js/static/10001792/web/j/v2/components/cmp-modal.js')}}"></script>
<script src="{{asset('js/static/10001792/web/j/v2/vimguard.js')}}"></script>

<script src="{{asset('js/index.js')}}"></script>
<script src="{{asset('js/admin-scripts.js')}}"></script>
<script src="{{asset('js/tiny-slider.min.js')}}"></script>
<script src="{{asset('js/cmp-articles.js')}}"></script>


<script src="{{asset('js/intlTelInput.min.js')}}"></script>
<script>
    let phoneInput = document.querySelector("#billing-phone")
    if (phoneInput) {
        let iti = window.intlTelInput(phoneInput, {
            allowDropdown: true,
            customContainer: 'billing-phone-container'
        })
        const code = iti.getSelectedCountryData().dialCode
        phoneInput.setAttribute('value', `+${code}`)
        phoneInput.addEventListener("countrychange", function () {
            const code = iti.getSelectedCountryData().dialCode
            phoneInput.setAttribute('value', `+${code}`)
        });
    }
</script>
