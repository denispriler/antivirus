<?php

return [
    'authenticate' => [
        'credentials' => 'Megaplan login or password is invalid',
        'token' => 'Invalid API token'
    ],
    'extension' => [
        'override' => 'Method :method not overridden.'
    ]
];
