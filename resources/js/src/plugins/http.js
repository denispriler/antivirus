import axios from 'axios'
import {serialize} from 'object-to-formdata'

const options = {
    indices: false,
    nullsAsUndefineds: false,
    booleansAsIntegers: true,
    allowEmptyArrays: false,
}

class Api {

    constructor() {
        let service = axios.create()

        // service.defaults.timeout = 1000 * 60 * 3

        service.interceptors.response.use(Api.handleSuccess, Api.handleError)

        this.service = service
    }

    static handleSuccess(response) {
        return response.data || response
    }

    static handleError(error) {
        const err = error.response.data
        return Promise.reject(err.errors || err.message)
    }

    get(path, payload = {}) {
        return this.service.get(path, {
            params: payload
        })
    }

    patch(path, payload) {
        if (payload instanceof FormData) {
            payload.set('_method', 'PUT')
        } else {
            payload['_method'] = 'PUT'
        }
        return this.service.request({
            method: 'POST',
            url: path,
            responseType: 'json',
            data: payload instanceof FormData ? payload : serialize(payload, options)
        })
    }

    rawPatch(path, payload) {
        if (payload instanceof FormData) {
            payload.set('_method', 'PUT')
        } else {
            payload['_method'] = 'PUT';
        }
        return this.service.request({
            method: 'POST',
            url: path,
            responseType: 'json',
            data: payload
        });
    }

    post(path, payload) {
        return this.service.request({
            method: 'POST',
            url: path,
            responseType: 'json',
            data: payload instanceof FormData ? payload : serialize(payload, options)
        })
    }

    rawPost(path, payload) {
        return this.service.request({
            method: 'POST',
            url: path,
            responseType: 'json',
            data: payload
        })
    }

    delete(path, payload) {
        return this.service.request({
            method: 'DELETE',
            url: path,
            responseType: 'json',
            data: payload
        })
    }
}

export default new Api
