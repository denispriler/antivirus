let mix = require('laravel-mix');
const path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

const publicPath = path.resolve(__dirname, 'public');
const resourcePath = path.resolve(__dirname, 'resources');

const config = require('./webpack.config');

mix.webpackConfig(config);

mix
    .combine('resources/css/*.css', `${publicPath}/styles/styles.css`)
    .copy(`${resourcePath}/fonts`, `${publicPath}/styles/fonts`)
    .copy(`${resourcePath}/js/lib/*.js`, `${publicPath}/js`)
    .copy(`${resourcePath}/js/template`, `${publicPath}/js`)
    .copy(`${resourcePath}/images`, `${publicPath}/images`)
    .copy(`${resourcePath}/favicon.ico`, `${publicPath}/favicon.ico`);

mix.autoload({ 'jquery': ['window.$', 'window.jQuery'] })
    .js(`${resourcePath}/js/src/main.js`, `${publicPath}/js/components.js`)
    .vue()
    .sourceMaps();

mix.js('resources/js/jetstream/app.js', 'public/jetstream/js')
    .postCss('resources/css/jetstream/app.css', 'public/jetstream/css', [
        require('postcss-import'),
        require('tailwindcss'),
    ]);

if (mix.inProduction()) {
    mix.version();
}
