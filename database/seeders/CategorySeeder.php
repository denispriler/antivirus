<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::firstOrCreate([
            'name' => 'Security'
        ], [
            'description' => 'Looking for a product for your device? <a href="'
                . route('product.show', ['product' => Product::$FREE_SLUG])
                . '" data-role="Nav:MenuItem" data-cta="homeSecurityLink">Free&nbsp;Antivirus&nbsp;for&nbsp;PC</a>'
        ]);
    }
}
