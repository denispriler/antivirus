<?php

namespace Database\Seeders\Megaplan;

use App\Models\Product;
use App\Services\Megaplan\MegaplanApiService;
use Illuminate\Database\Seeder;

class OfferSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $megaplan = app(MegaplanApiService::class);

        Product::each(function (Product $product) use (&$megaplan) {
            $megaplan->Offer->CreateOrUpdate($product);
        });
    }
}
