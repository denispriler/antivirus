<?php

namespace Database\Seeders;

use App\Models\Platform;
use Illuminate\Database\Seeder;

class PlatformSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $platforms = [
            ["Windows", "win", "./images/local/win.svg", "Available for PC"],
            ["Mac", "mac", "./images/local/mac.svg", "Available for Mac"],
            ["Android", "android", "./images/local/android.svg", "Available for Android"],
            ["iOS", "ios", "./images/local/ios.svg", "Available for iOS"],
            ["Servers", "servers", "./images/local/servers-smb.svg", "Available for Servers"],
            ["Linux", "linux", "./images/local/linux-smb.svg", "Available for Linux"],
        ];

        foreach ($platforms as $platform) {
            $attributes = array_combine(["name", "slug", "icon", "description"], $platform);
            Platform::firstOrCreate([
                'slug' => $attributes['slug']
            ], $attributes);
        }
    }
}
