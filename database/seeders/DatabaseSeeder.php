<?php

namespace Database\Seeders;

use Database\Seeders\Megaplan\OfferSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CategorySeeder::class,
            PlatformSeeder::class,
            ProductSeeder::class,
            OfferSeeder::class,
            ArticleSeeder::class
        ]);
    }
}
