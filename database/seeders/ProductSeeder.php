<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Platform;
use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $platforms = Platform::all(['id'])->pluck('id');
        $products = [
            ["Home Security", "Basic protection for all your devices", "<p style=\"color: #fff;\"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc scelerisque accumsan est ut mattis. Sed in lorem sagittis, aliquam sem vitae, euismod nibh. Praesent et nisi id nisl vehicula sagittis. Nam ut lorem vitae elit accumsan pulvinar. Phasellus non dui eget nulla mattis aliquet convallis quis diam. Nam et sem mi. Pellentesque ultrices nulla vel pharetra ornare. </p> <p style=\"color: #fff;\"> Aenean consequat sodales velit, non commodo erat. Proin id dignissim odio, a convallis felis. Nulla et ante at nunc interdum tempor eu non neque. Quisque id nulla eu tortor fermentum facilisis. Vestibulum tempor luctus lacus, eu euismod dolor mattis a. Etiam euismod, quam at vestibulum convallis, metus nisi molestie leo, vel consectetur nunc arcu cursus diam. In id felis ullamcorper, consequat libero vel, porttitor nulla. Donec at lorem id nulla eleifend facilisis. Praesent id vestibulum mauris, accumsan hendrerit ligula. Sed enim tellus, porttitor ut neque a, feugiat finibus libero. </p>", "./images/local/free-antivirus.svg", "79.00", "0", "free-antivirus", "products.free"],
            ["Premium Security", "Complete protection against all internet threats", "<p style=\"color: #fff;\"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc scelerisque accumsan est ut mattis. Sed in lorem sagittis, aliquam sem vitae, euismod nibh. Praesent et nisi id nisl vehicula sagittis. Nam ut lorem vitae elit accumsan pulvinar. Phasellus non dui eget nulla mattis aliquet convallis quis diam. Nam et sem mi. Pellentesque ultrices nulla vel pharetra ornare. </p> <p style=\"color: #fff;\"> Aenean consequat sodales velit, non commodo erat. Proin id dignissim odio, a convallis felis. Nulla et ante at nunc interdum tempor eu non neque. Quisque id nulla eu tortor fermentum facilisis. Vestibulum tempor luctus lacus, eu euismod dolor mattis a. Etiam euismod, quam at vestibulum convallis, metus nisi molestie leo, vel consectetur nunc arcu cursus diam. In id felis ullamcorper, consequat libero vel, porttitor nulla. Donec at lorem id nulla eleifend facilisis. Praesent id vestibulum mauris, accumsan hendrerit ligula. Sed enim tellus, porttitor ut neque a, feugiat finibus libero. </p>", "./images/local/premium.svg", "129.00", "0", "premium-security", "products.premium"],
            ["Total Security", "Our best security, privacy, and performance apps in one package", "<p style=\"color: #fff;\"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc scelerisque accumsan est ut mattis. Sed in lorem sagittis, aliquam sem vitae, euismod nibh. Praesent et nisi id nisl vehicula sagittis. Nam ut lorem vitae elit accumsan pulvinar. Phasellus non dui eget nulla mattis aliquet convallis quis diam. Nam et sem mi. Pellentesque ultrices nulla vel pharetra ornare. </p> <p style=\"color: #fff;\"> Aenean consequat sodales velit, non commodo erat. Proin id dignissim odio, a convallis felis. Nulla et ante at nunc interdum tempor eu non neque. Quisque id nulla eu tortor fermentum facilisis. Vestibulum tempor luctus lacus, eu euismod dolor mattis a. Etiam euismod, quam at vestibulum convallis, metus nisi molestie leo, vel consectetur nunc arcu cursus diam. In id felis ullamcorper, consequat libero vel, porttitor nulla. Donec at lorem id nulla eleifend facilisis. Praesent id vestibulum mauris, accumsan hendrerit ligula. Sed enim tellus, porttitor ut neque a, feugiat finibus libero. </p>", "./images/local/free-antivirus.svg", "169.00", "1", "ultimate", "products.ultimate"]
        ];

        $category = optional(Category::first())->id;
        foreach ($products as $product) {
            $attributes = array_combine(["name", "details", "description", "logo", "price", "is_best", "slug", "tpl"], $product);
            $product = Product::updateOrCreate([
                'slug' => $attributes['slug']
            ], array_merge($attributes, ['category_id' => $category]));
            $product->platforms()->sync($platforms);
        }
    }
}
