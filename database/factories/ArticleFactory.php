<?php

namespace Database\Factories;

use App\Models\Article;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArticleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Article::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'title'      => $this->faker->sentence(),
            'slug'       => $this->faker->slug(),
            'short_text' => $this->faker->sentence(),
            'text'       => $this->faker->text(),
            'author'     => $this->faker->title(),
            'image'      => '/images/articles_menu/a.jpg'
        ];
    }
}
