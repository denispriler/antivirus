<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductHasPlatformTable extends Migration
{
    public function up()
    {
        Schema::create('product_has_platform', function (Blueprint $table) {
            $table->unsignedBigInteger('platform_id');
            $table->unsignedBigInteger('product_id');

            $table->foreign('platform_id')
                ->references('id')
                ->on('platforms')
                ->cascadeOnDelete();
            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::table('product_has_platform', function (Blueprint $table) {
            $table->dropForeign(['platform_id']);
            $table->dropForeign(['product_id']);
        });
        Schema::dropIfExists('product_has_platform');
    }
}
