<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('product', \App\Http\Controllers\ProductController::class)->only(['index', 'show']);
Route::resource('article', \App\Http\Controllers\ArticleController::class)->only(['index', 'show']);

Route::get('/', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index'])->name('logs');

Route::get('oauth/{provider}', [\App\Http\Controllers\HomeController::class, 'oauth'])->name('oauth.auth');

Route::get('faq', function () {
    return view('faq');
})->name('faq');

Route::get('/auth/redirect/{provider}', function ($provider) {
    return Socialite::driver($provider)->redirect();
})->name('oauth.redirect');

Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::post('user/delete', [\App\Actions\Fortify\UpdateUserProfileInformation::class, 'delete'])->name('user-profile-information.delete');
    Route::post('user/update/email', [\App\Actions\Fortify\UpdateUserProfileInformation::class, 'updateEmail'])->name('user-profile-information-email.update');

    Route::get('my-products', [\App\Http\Controllers\ProductController::class, 'myProducts'])->name('user.products');

    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});
