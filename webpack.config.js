const path = require('path');

const resourcePath = path.resolve(__dirname, 'resources');

module.exports = {
    output: {chunkFilename: 'js/[name].js?id=[chunkhash]'},
    resolve: {
        alias: {
            '@': `${resourcePath}/js/src`,
            '@components': `${resourcePath}/js/src/components`,
            '@mixins': `${resourcePath}/js/src/mixins`,
            '@plugins': `${resourcePath}/js/src/plugins`,
            '@images': `${resourcePath}/images`,
        },
    }
}
